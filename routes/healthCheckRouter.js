const express = require('express');

const router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  // const result = await Action.create({resourceType: 'blackduck', createdBy: 'tuanna', resourceUrl: 'http://'});
  // res.json(result)
  res.json('OK health check, product is alive');
});

module.exports = router;
