/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-await-in-loop */
require('express-async-errors');
const express = require('express');

const router = express.Router();
const Project = require('../models/project.model');
const Action = require('../models/action.model');
const User = require('../models/user.model');

const { saveData } = require('../services/save-data');
const {
  collectAllData,
} = require('../helper/index.js');
const { verifyRequestAuthentication, isRoleSubAdmin, isRoleProjectUser } = require('../middlewares/auth.middleware');
/* GET home page. */
router.get('/', verifyRequestAuthentication, async (req, res) => {
  const { page, limit } = req.query;
  const limitValue = limit ? parseInt(limit, 10) : 10;
  const pageValue = page ? parseInt(page, 10) : 0;

  const total = await Action.find().countDocuments();
  const data = await Action.find().sort(
    {
      createdAt: -1,
    },
  ).skip(pageValue * limitValue).limit(limitValue);

  res.json(
    {
      total,
      data,
      limit: limitValue,
      page: pageValue,
    },
  );
});

const sortDataByResource = (array) => {
  const listHasResource = array.filter((item) => (
    item.resourceData.jenkins.length > 0
    || item.resourceData.sonar.length > 0
    || item.resourceData.coverity.length > 0
    || item.resourceData.blackduck.length > 0
  ));
  const listNoResource = array.filter((item) => (
    item.resourceData.jenkins.length === 0
    && item.resourceData.sonar.length === 0
    && item.resourceData.coverity.length === 0
    && item.resourceData.blackduck.length === 0
  ));
  return [...listHasResource, ...listNoResource];
};

router.get('/summary', verifyRequestAuthentication, isRoleProjectUser, async (req, res) => {
  const {
    page, limit, query, sort, sortOrder,
  } = req.query;
  const { userName, role } = req;
  const limitValue = limit ? parseInt(limit, 10) : 10;
  const pageValue = page ? parseInt(page, 10) : 0;
  const regex = new RegExp(`.*${query}.*`, 'i');
  let data = [];
  let total = 0;
  // const option = [
  //   { 'resourceData.jenkins': { $exists: true, $ne: [] } },
  //   { 'resourceData.sonar': { $exists: true, $ne: [] } },
  //   { 'resourceData.coverity': { $exists: true, $ne: [] } },
  //   { 'resourceData.blackduck': { $exists: true, $ne: [] } },
  // ];
  const sortObject = {};

  if (sort && sortOrder && (!['blackduck', 'coverity', 'sonar', 'jenkins'].includes(sort))) {
    if (sort === 'department1' || sort === 'department2') {
      sortObject[`department.${sort}`] = sortOrder === 'ascend' ? 1 : -1;
    } else {
      sortObject[sort] = sortOrder === 'ascend' ? 1 : -1;
    }
  } else {
    sortObject.timestamp = -1;
    sortObject.startDate = -1;
  }

  const projects = Project.find({
    projectCode: { $regex: regex },
    // $or: option
  }).sort(sortObject);
  let listProject = await projects;
  listProject = sortDataByResource(listProject);
  const start = pageValue * limitValue;
  const end = (pageValue + 1) * limitValue;
  if (role === 'user') {
    data = [];
    total = 0;
  } else if (sort && sortOrder && ['blackduck', 'coverity', 'sonar', 'jenkins'].includes(sort)) {
    // sort order resource
    const listHasResource = listProject.filter((item) => item.resourceData[`${sort}`].length > 0);
    const listNoResource = listProject.filter((item) => item.resourceData[`${sort}`].length === 0);

    let listProjectFm = [];

    if (sortOrder === 'ascend') {
      listProjectFm = [...listHasResource, ...listNoResource];
    } else {
      listProjectFm = [...listNoResource, ...listHasResource];
    }

    if (['project_admin', 'project_user'].includes(role)) {
      const user = await User.findOne({ name: userName.toLowerCase() });
      if (user) {
        const lisProjectFilter = listProjectFm.reduce((acc, current) => {
          let checkUser = null;
          if (Array.isArray(current.projectPermission)) {
            checkUser = current.projectPermission.find((item) => (
              item.userName === userName
            ));
          }

          if ((current.pmAccount === userName) || checkUser) {
            acc.push(current);
          }
          return acc;
        }, []);
        data = lisProjectFilter.slice(start, end);
        total = lisProjectFilter.length;
      }
    } else {
      data = listProjectFm.slice(start, end);
      total = listProjectFm.length;
    }
  } else if (['project_admin', 'project_user'].includes(role)) {
    // not sort order
    const user = await User.findOne({ name: userName.toLowerCase() });
    if (user) {
      const lisProjectFilter = listProject.reduce((acc, current) => {
        let checkUser = null;
        if (Array.isArray(current.projectPermission)) {
          checkUser = current.projectPermission.find((item) => (
            item.userName.toLowerCase() === userName.toLowerCase()
          ));
        }
        if ((current.pmAccount === userName) || checkUser) {
          acc.push(current);
        }
        return acc;
      }, []);

      data = lisProjectFilter.slice(start, end);
      total = lisProjectFilter.length;
    }
  } else {
    // data = await projects.skip(pageValue * limitValue).limit(limitValue);
    data = listProject.slice(pageValue * limitValue, limitValue * (pageValue + 1));
    total = await Project.find({
      projectCode: { $regex: regex },
      // $or: option
    }).countDocuments();
  }

  res.json(
    {
      total,
      data,
      limit: limitValue,
      page: pageValue,
    },
  );
});

router.get('/getAllProject', verifyRequestAuthentication, isRoleProjectUser,
  async (req, res) => {
    const data = await Project.find().sort({ timestamp: -1, startDate: -1 });
    res.json({ data });
  });

router.post('/save', verifyRequestAuthentication, isRoleSubAdmin, async (req, res) => {
  const {
    department,
    projectCode,
    projectName,
    pmAccount,
    akaworkId,
    projectLink,
    projectRank,
    status,
    startDate,
    endDate,
    description,
    useDevOpsService,
    resourceCreatedBy,
    isResourceCreated,
    resourceData,
  } = req.body;

  const result = await saveData(
    {
      department,
      projectCode,
      projectName,
      pmAccount,
      akaworkId,
      projectLink,
      projectRank,
      status,
      startDate,
      endDate,
      description,
      useDevOpsService,
      resourceCreatedBy,
      isResourceCreated,
      resourceData,
    },
  );

  res.json(
    result,
  );
});

router.post('/collect', verifyRequestAuthentication, isRoleProjectUser, async (req, res) => {
  const data = await collectAllData();

  res.json(data);
});

module.exports = router;
