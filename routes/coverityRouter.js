require('express-async-errors');
const express = require('express');

const router = express.Router();
const {
  // createMultiProject,
  createProject,
  createUser,
  summaryData,
  // createMultiStream,
  createStream,
  getProjects,
  deleteProject,
  updateUser,
  saveCoverityDeleteProjectLog,
  getUnuseResource,
  addResourceToProject,
  getListHostUnuseRs,
  getUser,
  getStream,
  getProject,
  deleteStream,
} = require('../middlewares/coverity.middleware');
const {
  verifyRequestAuthentication,
  isRoleSubAdmin,
  isRoleProjectUser,
  isProjectPermission,
} = require('../middlewares/auth.middleware');

router.post('/createUser', verifyRequestAuthentication, isRoleSubAdmin, createUser, summaryData);

router.post('/createProject', verifyRequestAuthentication, isRoleSubAdmin, createProject, summaryData);

router.post('/createStream', verifyRequestAuthentication, isProjectPermission('coverity'), createStream);

router.get('/projects', verifyRequestAuthentication, isRoleProjectUser, getProjects);

router.delete('/projects', verifyRequestAuthentication, isRoleSubAdmin, deleteProject, saveCoverityDeleteProjectLog);

router.post('/updateUser', verifyRequestAuthentication, isRoleSubAdmin, updateUser);

router.get('/listUnuseResource', verifyRequestAuthentication, isRoleProjectUser, getUnuseResource);

router.post('/addResourceToProject', verifyRequestAuthentication, isProjectPermission('coverity'), addResourceToProject);

router.get('/getListHostUnuseRs', verifyRequestAuthentication, isRoleProjectUser, getListHostUnuseRs);

router.get('/getUser', verifyRequestAuthentication, isRoleProjectUser, getUser);

router.get('/getStream', verifyRequestAuthentication, isRoleProjectUser, getStream);

router.get('/getProject', verifyRequestAuthentication, isRoleProjectUser, getProject);

router.post('/deleteStream', verifyRequestAuthentication, isProjectPermission('coverity'), deleteStream);

module.exports = router;
