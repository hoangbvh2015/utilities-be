require('express-async-errors');
const express = require('express');
const { ldapAuthenticate, generateToken } = require('../helper/auth');
const User = require('../models/user.model');
const userRoles = require('../models/userRoles.model');

const router = express.Router();

/* GET home page. */
router.get('/', async (req, res) => {
  // const result = await Action.create({resourceType: 'blackduck', createdBy: 'tuanna', resourceUrl: 'http://'});
  // res.json(result)
  res.json('OK, service is alive');
});

router.post('/authentication', async (req, res) => {
  const { username, password } = req.body;
  try {
    const authRes = await ldapAuthenticate(username, password);
    const token = generateToken({ name: authRes.name, email: authRes.mail });
    const regex = new RegExp(`.*${authRes.mail}.*`, 'i');
    const record = await User.findOne({ mail: { $regex: regex } });
    let role = null;

    if (record) {
      await User.updateOne(
        { mail: { $regex: regex } },
        { $set: { token, mail: authRes.mail } },
      );
      role = await userRoles.findOne({ roleCode: record.roleCode });
    } else {
      const userModel = new User({
        name: authRes.name && authRes.name.toLowerCase(),
        mail: authRes.mail,
        token,
        roleName: 'User',
        roleCode: 'user',
      });
      await userModel.save();
      role = await userRoles.findOne({ roleCode: userModel.roleCode });
    }

    // await getLdapUsers(username, password);
    res.json({ token, role });
  } catch (error) {
    res.status(401).json('Invalid username or password');
    throw error;
  }
});

module.exports = router;
