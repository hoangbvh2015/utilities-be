/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-await-in-loop */
require('express-async-errors');
const express = require('express');
const { exportPipeline, createFolder } = require('../middlewares/export.middleware');

const router = express.Router();

/* GET home page. */

router.get('/exportAll', exportPipeline);
router.post('/createFolder', createFolder);

module.exports = router;
