require('express-async-errors');
const express = require('express');

const router = express.Router();
const {
  // userIsExist,
  // canAccessCredential,
  createAgent,
  addOwners,
  createFolder,
  summaryData,
  getListFolder,
  getAllJobInFolder,
  listAgent,
  getAgentUserPermission,
  agentInfo,
  getFolderInfo,
  saveOwner,
  saveJenkinsChangeOwnerLog,
  getListJobInfolder,
  deleteFolder,
  saveJenkinsDeleteFolderLog,
  getUnuseResource,
  addResourceToProject,
  getListHostUnuseRs,
  pushJenkinsFile,
  updateAgent,
  getJenkinsPipelineJson,
} = require('../middlewares/jenkins.middleware');
const {
  verifyRequestAuthentication,
  isRoleSubAdmin,
  isRoleProjectUser,
  isProjectPermission,
} = require('../middlewares/auth.middleware');

router.post('/createFolder', verifyRequestAuthentication, isRoleSubAdmin, createFolder, summaryData);

router.post('/addOwners', verifyRequestAuthentication, isRoleSubAdmin, addOwners);

router.post('/createAgent', verifyRequestAuthentication, isRoleSubAdmin, createAgent);

router.get('/server_info', verifyRequestAuthentication, isRoleProjectUser, getListFolder);

router.get('/getAllJob', verifyRequestAuthentication, isRoleProjectUser, getAllJobInFolder);

router.get('/listAgent', verifyRequestAuthentication, isRoleProjectUser, listAgent);

router.get('/agentPermission', verifyRequestAuthentication, isRoleProjectUser, getAgentUserPermission);

router.get('/agentInfo', verifyRequestAuthentication, isRoleProjectUser, agentInfo);

router.get('/folder_info', verifyRequestAuthentication, isRoleProjectUser, getFolderInfo);

router.post('/ownerSubmit', verifyRequestAuthentication, isProjectPermission('jenkins'), saveOwner, saveJenkinsChangeOwnerLog);

router.get('/getJobsInFolder', verifyRequestAuthentication, isRoleProjectUser, getListJobInfolder);

router.post('/submitDelete', verifyRequestAuthentication, isRoleSubAdmin, deleteFolder, saveJenkinsDeleteFolderLog);

router.get('/listUnuseResource', verifyRequestAuthentication, isRoleProjectUser, getUnuseResource);

router.post('/addResourceToProject', verifyRequestAuthentication, isProjectPermission('jenkins'), addResourceToProject);

router.get('/getListHostUnuseRs', verifyRequestAuthentication, isRoleProjectUser, getListHostUnuseRs);

router.post('/push/jenkinsFile', verifyRequestAuthentication, pushJenkinsFile);

router.put('/updateAgent', verifyRequestAuthentication, isRoleProjectUser, updateAgent);

router.get('/pipelineJson', getJenkinsPipelineJson);

module.exports = router;
