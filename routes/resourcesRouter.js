require('express-async-errors');
const express = require('express');

const router = express.Router();
const {
  getAkaAuthenToken,
  getAkaDepartment,
  getAkaProjects,
  getAkaProjectsWithMapComponent,
  getExternalProjects,
  getExternalComponents,
} = require('../helper/akawork');
const { verifyRequestAuthentication, isRoleProjectUser } = require('../middlewares/auth.middleware');
const ProjectCode = require('../models/projectCode.model');
const { servers } = require('../enviroment');

const akaworkApiHost = servers.akaworkApi;

/* GET home page. */
router.post('/departments', verifyRequestAuthentication, isRoleProjectUser, async (_req, res) => {
  const token = await getAkaAuthenToken(akaworkApiHost);
  const data = await getAkaDepartment(akaworkApiHost, token);

  res.json(data);
});

router.get('/projects', verifyRequestAuthentication, isRoleProjectUser, async (req, res) => {
  const token = await getAkaAuthenToken(akaworkApiHost);
  // eslint-disable-next-line no-underscore-dangle
  const data = await getAkaProjects(akaworkApiHost, token, req._parsedUrl.query);

  res.json(data);
});

router.get('/projects-with-component', verifyRequestAuthentication, isRoleProjectUser, async (req, res) => {
  const token = await getAkaAuthenToken(akaworkApiHost);
  // eslint-disable-next-line no-underscore-dangle
  const data = await getAkaProjectsWithMapComponent(akaworkApiHost, token, req._parsedUrl.query);

  res.json(data);
});

router.get('/external-projects', verifyRequestAuthentication, isRoleProjectUser, async (req, res) => {
  const token = await getAkaAuthenToken(akaworkApiHost);
  // eslint-disable-next-line no-underscore-dangle
  const data = await getExternalProjects(akaworkApiHost, token, req._parsedUrl.query);

  res.json(data);
});

router.get('/external-components', verifyRequestAuthentication, isRoleProjectUser, async (req, res) => {
  const token = await getAkaAuthenToken(akaworkApiHost);
  // eslint-disable-next-line no-underscore-dangle
  const data = await getExternalComponents(akaworkApiHost, token, req._parsedUrl.query);

  res.json(data);
});

router.get('/allProjectCode', verifyRequestAuthentication, isRoleProjectUser, async (req, res) => {
  const startTime = (new Date()).getTime();
  const listProjectCode = await ProjectCode.find();
  const endTime = (new Date()).getTime();

  res.json({
    data: listProjectCode,
    total: listProjectCode.length,
    time: endTime - startTime,
  });
});

module.exports = router;
