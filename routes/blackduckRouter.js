require('express-async-errors');

const express = require('express');

const router = express.Router();
const {
  addUsertoMultiProject,
  createMultiProject,
  createUser,
  summaryData,
  listProject,
  getUsers,
  getUserProjects,
  getUnuseResource,
  addResourceToProject,
  getListHostUnuseRs,
  getAccountInfo,
  updateAccount,
  apiCheckAccountExist,
  apiGetProject,
  apiResetpassword,
} = require('../middlewares/blackduck.middleware');
const {
  verifyRequestAuthentication,
  isRoleSubAdmin,
  isProjectPermission,
  isRoleProjectUser,
} = require('../middlewares/auth.middleware');

router.post('/createUser', verifyRequestAuthentication, isRoleSubAdmin, createUser);

router.post('/createProjects', verifyRequestAuthentication, isRoleSubAdmin, createMultiProject, summaryData);

router.post('/addUserToProject', verifyRequestAuthentication, isRoleSubAdmin, addUsertoMultiProject);

router.get('/listProject', verifyRequestAuthentication, isRoleProjectUser, listProject);

router.get('/projectUsers', verifyRequestAuthentication, isRoleProjectUser, getUserProjects);

router.get('/users', verifyRequestAuthentication, isRoleProjectUser, getUsers);

router.get('/listUnuseResource', verifyRequestAuthentication, isRoleProjectUser, getUnuseResource);

router.post('/addResourceToProject', verifyRequestAuthentication, isProjectPermission('blackduck'), addResourceToProject);

router.get('/getListHostUnuseRs', verifyRequestAuthentication, isRoleProjectUser, getListHostUnuseRs);

router.get('/getAccountInfo', verifyRequestAuthentication, isRoleProjectUser, getAccountInfo);

router.put('/updateAccount', verifyRequestAuthentication, isRoleSubAdmin, updateAccount);

router.get('/checkAccountExist', verifyRequestAuthentication, isRoleProjectUser, apiCheckAccountExist);

router.get('/getProject', verifyRequestAuthentication, isRoleProjectUser, apiGetProject);

router.put('/resetpassword', verifyRequestAuthentication, isProjectPermission('blackduck'), apiResetpassword);

module.exports = router;
