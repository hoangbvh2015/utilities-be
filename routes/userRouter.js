/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-await-in-loop */
require('express-async-errors');
const express = require('express');

const router = express.Router();

const { verifyRequestAuthentication, isRoleAdmin, isRoleProjectUser } = require('../middlewares/auth.middleware');
const {
  getListLdapUser,
  updateUser,
  getRoleUser,
} = require('../middlewares/user.middleware');
/* GET home page. */

router.get('/getLdapUser', verifyRequestAuthentication, isRoleAdmin, getListLdapUser);
router.put('/updateUser', verifyRequestAuthentication, isRoleAdmin, updateUser);
router.get('/getRoleUser', verifyRequestAuthentication, isRoleProjectUser, getRoleUser);
router.get('/getLdapUserInfor', verifyRequestAuthentication, isRoleProjectUser, getListLdapUser);

module.exports = router;
