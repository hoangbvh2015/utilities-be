/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-await-in-loop */
require('express-async-errors');
const express = require('express');

const router = express.Router();
const {
  verifyRequestAuthentication,
  isProjectPermission,
  isRoleProjectAdmin,
} = require('../middlewares/auth.middleware');
const {
  addProjectUserAll,
  changePermissionAll,
  changePermissionEditUser,
  removeProjectUserAll,
  changeRoleProject,
} = require('../middlewares/projectUser.middleware');
/* GET home page. */
router.post('/changePermissionAll', verifyRequestAuthentication, isProjectPermission('edit_user_project'), changePermissionAll);
router.post('/changePermissionEditUser', verifyRequestAuthentication, isRoleProjectAdmin, changePermissionEditUser);
router.post('/addProjectUserAll', verifyRequestAuthentication, isProjectPermission('edit_user_project'), addProjectUserAll);
router.post('/removeProjectUserAll', verifyRequestAuthentication, isProjectPermission('edit_user_project'), removeProjectUserAll);
router.post('/changeRoleProject', verifyRequestAuthentication, isProjectPermission('change_role_user_project'), changeRoleProject);

module.exports = router;
