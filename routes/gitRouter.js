require('express-async-errors');
const express = require('express');

const router = express.Router();

const {
  pushNewCommit,
} = require('../middlewares/git.middleware');
const {
  verifyRequestAuthentication,
} = require('../middlewares/auth.middleware');

router.post('/push', verifyRequestAuthentication, pushNewCommit);

module.exports = router;
