require('express-async-errors');
const express = require('express');

const router = express.Router();

const {
  getServer,
  createServer,
  updateServer,
  removeServer,
} = require('../middlewares/server.middleware');
const {
  verifyRequestAuthentication,
} = require('../middlewares/auth.middleware');

router.get('/', verifyRequestAuthentication, getServer);

router.post('/create', verifyRequestAuthentication, createServer);

router.post('/update', verifyRequestAuthentication, updateServer);

router.post('/remove', verifyRequestAuthentication, removeServer);

module.exports = router;
