const express = require('express');

const router = express.Router();
const {
  createResource,
  summaryData,
  listProject,
  deleteProjects,
  addUserProjectPermission,
  addGroupProjectPermission,
  deleteUserProjectPermission,
  deleteGroupProjectPermission,
  getProjectUsers,
  getProjectGroups,
  searchProject,
  searchUser,
  getUnuseResource,
  addResourceToProject,
  getListHostUnuseRs,
  createUser,
  updateUser,
  changeStatusUser,
} = require('../middlewares/sonar.middleware');
const {
  verifyRequestAuthentication,
  isRoleSubAdmin,
  isRoleProjectUser,
  isProjectPermission,
} = require('../middlewares/auth.middleware');

router.post('/createResource', verifyRequestAuthentication, isRoleSubAdmin, createResource, summaryData);

router.post('/deleteProjects', verifyRequestAuthentication, isRoleSubAdmin, deleteProjects);

router.post('/project/users/permission_add', verifyRequestAuthentication, isRoleSubAdmin, addUserProjectPermission);

router.post('/project/groups/permission_add', verifyRequestAuthentication, isRoleSubAdmin, addGroupProjectPermission);

router.post('/project/users/permission_delete', verifyRequestAuthentication, isRoleSubAdmin, deleteUserProjectPermission);

router.post('/project/groups/permission_delete', verifyRequestAuthentication, isRoleSubAdmin, deleteGroupProjectPermission);

router.get('/project/user', verifyRequestAuthentication, isRoleProjectUser, getProjectUsers);

router.get('/project/group', verifyRequestAuthentication, isRoleProjectUser, getProjectGroups);

router.get('/project/search', verifyRequestAuthentication, isRoleProjectUser, searchProject);

router.get('/user/search', verifyRequestAuthentication, isRoleProjectUser, searchUser);

router.get('/listProject', verifyRequestAuthentication, isRoleProjectUser, listProject);

router.get('/listUnuseResource', verifyRequestAuthentication, isRoleProjectUser, getUnuseResource);

router.post('/addResourceToProject', verifyRequestAuthentication, isProjectPermission('sonar'), addResourceToProject);

router.get('/getListHostUnuseRs', verifyRequestAuthentication, isRoleProjectUser, getListHostUnuseRs);

router.post('/createUser', verifyRequestAuthentication, isRoleSubAdmin, createUser);

router.put('/updateUser', verifyRequestAuthentication, isRoleSubAdmin, updateUser);

router.put('/changeStatusUser', verifyRequestAuthentication, isRoleSubAdmin, changeStatusUser);

module.exports = router;
