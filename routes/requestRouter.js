require('express-async-errors');
const express = require('express');

const router = express.Router();

const { verifyRequestAuthentication, isRoleSubAdmin } = require('../middlewares/auth.middleware');
const {
  getListRequest,
  updateRequest,
  addCommentTicket,
  reloadTable,
  autoCloseTickket,
} = require('../middlewares/request.middleware');
/* GET home page. */

router.get('/getListRequest', verifyRequestAuthentication, isRoleSubAdmin, getListRequest);
router.post('/updateRequest', verifyRequestAuthentication, isRoleSubAdmin, updateRequest);
router.post('/addCommentTicket', verifyRequestAuthentication, isRoleSubAdmin, addCommentTicket);
router.get('/reloadTable', verifyRequestAuthentication, isRoleSubAdmin, reloadTable);
router.post('/closeTicket', verifyRequestAuthentication, isRoleSubAdmin, autoCloseTickket);

module.exports = router;
