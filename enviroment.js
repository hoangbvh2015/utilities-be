require('dotenv').config();

let servers = {};
let blackduckKey = {};
let jenkinsKey = {};
let sonarKey = {};
let coverityAccount = {};
let ezServiceAccount = {};
let akaworkApi = {};

if (process.env.NODE_ENV === 'development') {
  servers = {
    akawork: 'https://devops-int.fsoft.com.vn',
    akaworkApi: 'https://devops-api-core-int-test.fsoft.com.vn',
    ezAuthen: 'https://devops.fsoft.com.vn',
    ez: 'https://test-ez-devops.fsoft.com.vn',
    blackduck: 'https://test-blackduck.fsoft.com.vn',
    jenkins: 'https://test-jenkins.fsoft.com.vn',
    sonar: 'https://test-sonar.fsoft.com.vn',
    coverity: 'https://test-coverity.fsoft.com.vn',
  };
  blackduckKey = {
    'https://test-blackduck.fsoft.com.vn': 'NGFiNGY2YWYtZGM3ZS00ZjQ0LWFiYWUtYThjOGQyMTA1YWNjOmRmNjkzNTY2LTg1MzAtNGQxYy05YTA0LTk4OTI3YTkzOTAzNQ==',
  };

  jenkinsKey = {
    'https://test-jenkins.fsoft.com.vn': { username: 'devopsgrafana', password: 'u9sH2KfqZM' },
  };

  sonarKey = {
    'https://test-sonar.fsoft.com.vn': { username: 'admin', password: 'DevOps@1234' },
  };

  coverityAccount = {
    'https://test-coverity.fsoft.com.vn': {
      username: 'resources-management-acc',
      password: 'Dev0ps',
    },
  };

  ezServiceAccount = {
    'https://test-ez-devops.fsoft.com.vn': {
      username: 'devopsgrafana',
      password: 'u9sH2KfqZM',
    },
  };

  akaworkApi = {
    'https://devops-api-core-int-test.fsoft.com.vn': {
      username: 'root',
      password: '123456',
    },
  };
}

if (process.env.NODE_ENV === 'production') {
  servers = {
    akawork: 'https://devops.fsoft.com.vn',
    akaworkApi: 'https://devops-api.fsoft.com.vn',
    ezAuthen: 'https://devops.fsoft.com.vn',
    ez: 'https://devops.fsoft.com.vn',
    blackduck: 'https://test-blackduck.fsoft.com.vn',
    jenkins: 'https://test-jenkins.fsoft.com.vn',
    sonar: 'https://test-sonar.fsoft.com.vn',
    coverity: 'https://test-coverity.fsoft.com.vn',
  };

  blackduckKey = {
    'https://blackduck.fsoft.com.vn': 'MzYxNDU0MjgtODc1Zi00MGM2LWE4MjktMjAxN2I3OGI1ODM2OjNmNGZkZjE3LTcyZmItNDBlMS1iMmU0LTM4YWExYzRiM2FkNQ==',
  };

  jenkinsKey = {
    'https://jenkins-dn.fsoft.com.vn': { username: 'devopsgrafana', password: 'u9sH2KfqZM' },
    'https://jenkins.fsoft.com.vn': { username: 'devopsgrafana', password: 'u9sH2KfqZM' },
    'https://stg-jenkins.fsoft.com.vn': { username: 'devopsgrafana', password: 'u9sH2KfqZM' },
  };

  sonarKey = {
    'https://sonar.fsoft.com.vn': { username: 'rm_acc', password: 'DevOps@1234' },
    'https://sonar1.fsoft.com.vn': { username: 'rm_acc', password: 'DevOps@1234' },
    'https://sonar-dn.fsoft.com.vn': { username: 'rm_acc', password: 'DevOps@1234' },
  };

  coverityAccount = {
    'https://coverity.fsoft.com.vn': {
      username: 'resources-management-acc',
      password: 'Dev0ps',
    },
    'https://coverity1.fsoft.com.vn': {
      username: 'resources-management-acc',
      password: 'Dev0ps',
    },
    'https://coverity-dn.fsoft.com.vn': {
      username: 'resources-management-acc',
      password: 'Dev0ps',
    },
    'https://coverity-hcm.fsoft.com.vn': {
      username: 'resources-management-acc',
      password: 'Dev0ps',
    },
  };

  ezServiceAccount = {
    'https://devops.fsoft.com.vn': {
      username: 'Devops.service',
      password: 'Akaworknumber1!@',
    },
  };

  akaworkApi = {
    'https://devops-api.fsoft.com.vn': {
      username: 'resource_management',
      password: 'Aa@123456',
    },
  };
}

module.exports = {
  blackduckKey,
  coverityAccount,
  jenkinsKey,
  sonarKey,
  ezServiceAccount,
  servers,
  akaworkApi,
};
