/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
const {
  isEmpty,
  includes,
  isString,
} = require('lodash');
const axios = require('axios');
const btoa = require('btoa');
const FormData = require('form-data');
const { jenkinsKey } = require('../enviroment.js');
const { saveLog, saveUserActionLog } = require('../services/saveLog.js');
const { removeJobUrlJenkins } = require('../services/save-data');
const jenkinsXMLFolderTemplate = require('../template/jenkins-folder-template');
const jenkinsJSONNodeTemplate = require('../template/jenkins-node-template');
const {
  pipelineTemplate,
} = require('../template/jenkins-pipeline-template');
const {
  getListFolderInRoot,
  getJobInFolder,
  getFolderUsersPermission,
  getAgentInfo,
  getAgentUsersPermission,
  getAllTreeItem,
  getJobsInfolder,
  getAgentEdit,
} = require('../helper/jenkins.js');
const JenkinsAgent = require('../models/jenkinsAgent.model');
const Project = require('../models/project.model');
const UnUseResource = require('../models/unUseResource.model');

const createFolder = async (req, res, next) => {
  const {
    resourceUrl,
    note,
  } = req.body;
  try {
    const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[resourceUrl].username}:${jenkinsKey[resourceUrl].password}`)}`;
    const { data: { crumb }, headers } = await axios.get(`${resourceUrl}/crumbIssuer/api/json`, {
      headers: {
        Authorization: AUTH_BASIC,
      },
    });
    const url = `${resourceUrl}/createItem?name=${note.folderName}&mode=com.cloudbees.hudson.plugins.folder.Folder`;
    const header = {
      'Content-Type': 'application/xml',
      'Jenkins-Crumb': `${crumb}`,
      Authorization: `${AUTH_BASIC}`,
      Cookie: `${headers['set-cookie'] ? headers['set-cookie'][0] : undefined}`,
    };
    await axios.post(url, jenkinsXMLFolderTemplate, { headers: header });
    req.jenkinsFolderLink = `${resourceUrl}/job/${note.folderName}`;
    next();
  } catch (error) {
    res.status(error.response.status);
    if (isString(error.response.data) && error.response.data.includes('A job already exists with the name')) {
      res.json({ error: { message: `${resourceUrl}/job/${note.folderName} was existed` } });
    } else {
      res.json({ error: error.response.data });
    }
  }
};

const addOwners = async (req, res) => {
  const {
    resourceUrl,
    note,
  } = req.body;
  try {
    const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[resourceUrl].username}:${jenkinsKey[resourceUrl].password}`)}`;
    const { data: { crumb }, headers } = await axios.get(`${resourceUrl}/crumbIssuer/api/json`, {
      headers: {
        Authorization: AUTH_BASIC,
      },
    });
    const tmpArr = note.folderName.split('/');
    const job = tmpArr.map((x) => `job/${x}/`).join('');
    const bodyFormData = new FormData();
    const converter = {
      owners: {
        primaryOwner: note.owners.primary,
        coOwners: note.owners.secondary.map((user) => ({ coOwner: user })),
      },
    };

    bodyFormData.append('json', JSON.stringify(converter));
    const header = {
      Authorization: AUTH_BASIC,
      'Content-Type': bodyFormData.getHeaders()['content-type'],
      'Jenkins-Crumb': crumb,
      Cookie: `${headers['set-cookie'] ? headers['set-cookie'][0] : undefined}`,
    };

    await axios.post(`${resourceUrl}/${job}ownership/ownersSubmit`, bodyFormData, { headers: header });

    res.json({
      jenkinsUserData: {
        primaryUser: note.owners.primary,
        secondaryUsers: note.owners.secondary,
      },
    });
  } catch (error) {
    if (error && error.response) {
      res.status(error.response.status);
      res.json({ isSuccess: false, error: error.response.data });
    }
  }
};

const createAgent = async (req, res) => {
  const {
    resourceUrl,
    note,
  } = req.body;
  const listAgents = note.agents.filter((agent) => !isEmpty(agent.ip) && includes(['windows', 'linux', 'macos'], agent.os));
  const resultCreateNode = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const agent of listAgents) {
    try {
      const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[resourceUrl].username}:${jenkinsKey[resourceUrl].password}`)}`;
      const { data: { crumb }, headers } = await axios.get(`${resourceUrl}/crumbIssuer/api/json`, {
        headers: {
          Authorization: AUTH_BASIC,
        },
      });
      const bodyFormData = new FormData();
      bodyFormData.append('name', `${agent.ip}`);
      bodyFormData.append('json', JSON.stringify(jenkinsJSONNodeTemplate[agent.os]));
      const url = `${resourceUrl}/computer/doCreateItem?name=${note.folderName}&type=hudson.slaves.DumbSlave`;
      const header = {
        Authorization: AUTH_BASIC,
        'Content-Type': bodyFormData.getHeaders()['content-type'],
        'Jenkins-Crumb': crumb,
        Cookie: `${headers['set-cookie'] ? headers['set-cookie'][0] : undefined}`,
      };

      await axios.post(url, bodyFormData, { headers: header });
      resultCreateNode.push({ ip: agent.ip, link: `${resourceUrl}/computer/${agent.ip}` });
    } catch (error) {
      if (error.response.headers['x-error'].includes('already exists')) {
        resultCreateNode.push({ ip: agent.ip, error: error.response.headers['x-error'], status: error.response.status });
      } else {
        resultCreateNode.push({
          ip: agent.ip,
          error: error.message,
          status: error.response.status,
        });
      }
    }
  }
  res.json({ resultCreateNode });
};

const updateAgent = async (req, res) => {
  const {
    host,
    nodeName,
    nodeUpdate,
  } = req.body;
  // eslint-disable-next-line no-restricted-syntax
  try {
    const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;
    const { data: { crumb }, headers } = await axios.get(`${host}/crumbIssuer/api/json`, {
      headers: {
        Authorization: AUTH_BASIC,
      },
    });
    const { agentInfo, status } = await getAgentEdit(host, nodeName);

    if (status === 404) {
      res.status(404).json({ message: 'Node is not exist!' });
      return;
    }

    const bodyUpdate = {
      name: nodeUpdate.nodeName || agentInfo.name,
      nodeDescription: nodeUpdate.description || agentInfo.description,
      numExecutors: nodeUpdate.numExecutors || agentInfo.numExecutors,
      remoteFS: nodeUpdate.remoteFS || agentInfo.remoteFS,
      labelString: nodeUpdate.label || agentInfo.label,
      mode: agentInfo.mode,
      launcher: agentInfo.launcher,
      retentionStrategy: agentInfo.retentionStrategy,
      nodeProperties: agentInfo.nodeProperties,
    };

    // console.log('body upda ==== ', agentInfo);
    const bodyFormData = new FormData();
    bodyFormData.append('json', JSON.stringify(bodyUpdate));
    const url = `${host}/computer/${nodeName}/configSubmit`;
    const header = {
      Authorization: AUTH_BASIC,
      'Content-Type': bodyFormData.getHeaders()['content-type'],
      'Jenkins-Crumb': crumb,
      Cookie: `${headers['set-cookie'] ? headers['set-cookie'][0] : undefined}`,
    };

    await axios.post(url, bodyFormData, { headers: header });
    res.json({ message: `Edit node ${nodeName} success!` });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getListFolder = async (req, res) => {
  const params = req.query;
  const folders = await getListFolderInRoot(params.host);

  if (typeof folders === 'object' && (folders.status < 200 || folders.status >= 300)) {
    res.status(folders.status);
    res.json({ errors: folders.errors });
  } else {
    const {
      listUser: users,
      status,
      errors,
    } = await getFolderUsersPermission(params.host, folders[0].jobName);

    if (status < 200 || status >= 300) {
      res.status(status);
      res.json({ errors });
    } else {
      res.json({ jobs: folders, users });
    }
  }
};

const getAllJobInFolder = async (req, res) => {
  const params = req.query;
  const jobs = await getJobInFolder(params.host, params.folderName);
  if (typeof jobs === 'object' && (jobs.status < 200 || jobs.status >= 300)) {
    res.status(jobs.status);
    res.json({ errors: jobs.errors });
  } else {
    res.json({ jobs });
  }
};

const listAgent = async (req, res) => {
  const params = req.query;
  try {
    const listJenkinsAgent = await JenkinsAgent.find({ host: { $eq: params.host } });
    res.status(200).json({ listJenkinsAgent });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
  // const agents = await getListAgent(params.host);
  // if(typeof agents === 'object' && (agents.status < 200 || agents.status >= 300)) {
  //   res.status(agents.status);
  //   res.json({ errors: agents.errors })
  // } else {
  //   res.json({ agents });
  // }
};

const getAgentUserPermission = async (req, res) => {
  const params = req.query;
  const info = await getAgentUsersPermission(params.host, params.agentName);

  if (typeof info === 'object' && (info.status < 200 || info.status >= 300)) {
    res.status(info.status);
    res.json({ errors: info.errors });
  } else {
    res.json({
      name: info.name,
      primaryOwner: info.primaryOwner,
      secondaryOwners: info.secondaryOwners,
    });
  }
};

const agentInfo = async (req, res) => {
  const params = req.query;
  const info = await getAgentInfo(params.host, params.agentName);

  if (typeof info === 'object' && (info.status < 200 || info.status >= 300)) {
    res.status(info.status);
    res.json({ errors: info.errors });
  } else {
    res.json({
      ...info.agentInfo,
    });
  }
};

const getFolderInfo = async (req, res) => {
  const params = req.query;
  let folderTree;
  let errors = {};

  if (params.withTree === 'true') {
    folderTree = await getAllTreeItem(params.host, params.folderName);
    if (typeof folderTree === 'object' && (folderTree.status < 200 || folderTree.status >= 300)) {
      errors = folderTree;
    }
  }
  const info = await getFolderUsersPermission(params.host, params.folderName);
  if (typeof info === 'object' && (info.status < 200 || info.status >= 300)) {
    errors = info;
    res.status(errors.status);
    res.json(errors.errors);
  } else {
    res.json({
      // listUser: info.listUser,
      tree: folderTree,
      primaryOwner: info.primaryOwner,
      secondaryOwner: info.secondaryOwner,
    });
  }
};

const getListJobInfolder = async (req, res) => {
  const params = req.query;
  const listJob = await getJobsInfolder(params.host);
  res.json({ listJob });
};

const summaryData = async (req, res) => {
  const {
    fsu,
    bu,
    resourceUrl,
    projectCode,
    projectName,
    createdBy,
    pmAccount,
  } = req.body;
  const { jenkinsFolderLink } = req;
  await saveLog(
    'jenkins',
    {
      fsu,
      bu,
      projectCode,
      projectName,
      createdBy,
      pmAccount,
      resources:
      {
        resourceUrl,
        jenkinsFolderLink,
      },
    },
  );
  res.json({
    jenkinsFolderLink,
  });
};

const saveOwner = async (req, res, next) => {
  const {
    jenkinsServer,
    primaryOwner,
    secondaryOwner,
  } = req.body;
  try {
    const domain = jenkinsServer.split('/job/')[0];
    const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[domain].username}:${jenkinsKey[domain].password}`)}`;
    const { data: { crumb }, headers } = await axios.get(`${domain}/crumbIssuer/api/json`, {
      headers: {
        Authorization: AUTH_BASIC,
      },
    });
    const bodyFormData = new FormData();
    const coOwners = secondaryOwner.map((user) => ({ coOwner: user }));
    const converter = { owners: { primaryOwner, coOwners } };
    const Cookie = headers['set-cookie'] ? headers['set-cookie'][0] : '';
    bodyFormData.append('json', JSON.stringify(converter));
    const header = {
      Authorization: AUTH_BASIC,
      'Content-Type': bodyFormData.getHeaders()['content-type'],
      'Jenkins-Crumb': crumb,
      Cookie: `${Cookie}`,
    };
    await axios.post(`${jenkinsServer}/ownership/ownersSubmit`, bodyFormData, { headers: header });
    next();
  } catch (error) {
    res.status(error.response.status);
    res.json({ isSuccess: false, error: error.response.data });
    throw error;
  }
};

const deleteFolder = async (req, res, next) => {
  const {
    jobURL,
    projectCode,
  } = req.body;
  try {
    const domain = jobURL.split('/job/')[0];
    const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[domain].username}:${jenkinsKey[domain].password}`)}`;
    const { data: { crumb }, headers } = await axios.get(`${domain}/crumbIssuer/api/json`, {
      headers: {
        Authorization: AUTH_BASIC,
      },
    });
    const header = {
      'Content-Type': 'application/xml',
      'Jenkins-Crumb': `${crumb}`,
      Authorization: `${AUTH_BASIC}`,
      Cookie: `${headers['set-cookie'] ? headers['set-cookie'][0] : undefined}`,
    };
    const bodyFormData = new FormData();
    bodyFormData.append('Submit', 'Yes');

    await axios.post(`${jobURL}/doDelete`, bodyFormData, { headers: header });
    const response = await removeJobUrlJenkins(projectCode, jobURL);
    // res.status(response.status).json(response.statusText)
    res.locals = response;
    next();
  } catch (error) {
    if (error.response.status === 404) {
      const response = await removeJobUrlJenkins(projectCode, jobURL);
      res.status(error.response.status).json({
        message: error.response.statusText,
        project: response,
      });
    } else {
      res.status(error.response.status).json(error.response.statusText);
    }
  }
};

const saveJenkinsChangeOwnerLog = async (req, res) => {
  const {
    createdBy, jenkinsServer, primaryOwner, secondaryOwner,
  } = req.body;
  await saveUserActionLog(
    'jenkins_change_owner',
    { createdBy, resources: { jenkinsServer, primaryOwner, secondaryOwner } },
  );
  res.json({
    isSuccess: true, primaryOwner, secondaryOwner, jenkinsServer,
  });
};

const saveJenkinsDeleteFolderLog = async (req, res) => {
  const { createdBy, jobURL, projectCode } = req.body;
  await saveUserActionLog(
    'jenkins_delete_folder',
    { createdBy, resources: { jobURL, projectCode } },
  );
  res.json({
    isSuccess: true, jobURL, projectCode, project: res.locals,
  });
};

const getListHostUnuseRs = async (req, res) => {
  const record = await UnUseResource.find({ resourceType: { $eq: 'jenkins' } });

  if (record) {
    const listHost = record.reduce((acc, current) => {
      const urlSplit = current.jobURL.split('/');
      const host = [urlSplit[0], urlSplit[1], urlSplit[2]].join('/');
      const checkExist = acc.find((item) => item.value === host);
      if (!checkExist) {
        acc.push({
          label: current.hostName,
          value: host,
        });
      }
      return acc;
    }, []);
    res.json({ success: true, listHost });
  } else {
    res.status(400).json({ success: false, message: 'Connect DB failed!' });
  }
};

const getUnuseResource = async (req, res) => {
  const {
    query = '',
    page = 1,
    pageSize = 100,
    host = '',
    type,
  } = req.query;

  const record = await UnUseResource.find({ resourceType: { $eq: 'jenkins' } });
  if (record) {
    const start = (page - 1) * pageSize;
    const end = page * pageSize;
    let listItem = [];
    if (type === 'Agent') {
      listItem = record.filter(
        (item) => (item.displayName.includes(query) && item.jobURL.includes('/DEPLOY/') && item.jobURL.includes(host)),
      );
    }

    if (type === 'Jobs') {
      listItem = record.filter(
        (item) => (item.displayName.includes(query) && !item.jobURL.includes('/DEPLOY/') && item.jobURL.includes(host)),
      );
    }

    if (!type) {
      listItem = record.filter(
        (item) => (item.displayName.includes(query) && item.jobURL.includes(host)),
      );
    }
    const total = listItem.length;
    const result = listItem.slice(start, end);
    res.json({ success: true, result, total });
  } else {
    res.status(400).json({ success: false, message: 'Connect DB failed!' });
  }
};

const addResourceToProject = async (req, res) => {
  const { projectCode, listResourceChecked } = req.body;
  const record = await Project.findOne({ projectCode });

  if (record) {
    const { resourceData } = record;

    for (const value of listResourceChecked) {
      const checkResource = resourceData.jenkins.findIndex((item) => item.jobURL === value.jobURL);
      if (checkResource !== -1) {
        resourceData.jenkins[checkResource] = value;
      } else {
        resourceData.jenkins.push(value);
      }
      await UnUseResource.updateOne({ jobURL: value.jobURL }, {
        $set: {
          proejctAssigned: [projectCode],
        },
      });
    }

    await Project.updateOne({ projectCode }, {
      $set: {
        resourceData,
      },
    });
    res.status(200);
  } else {
    res.status(404);
  }
  res.json(listResourceChecked);
};

const pushJenkinsFile = async (req, res) => {
  const {
    userNameJenkins,
    passwordJenkins,
    jenkinsFolderName,
    jenkinsPipelineName,
    jenkinsUrlServer,
    codeJenkins,
  } = req.body;
  try {
    const AUTH_BASIC = `Basic ${btoa(`${userNameJenkins}:${passwordJenkins}`)}`;
    const { data: { crumb }, headers } = await axios.get(`${jenkinsUrlServer}/crumbIssuer/api/json`, {
      headers: {
        Authorization: AUTH_BASIC,
      },
    });
    const urlJenkins = `${jenkinsFolderName}/createItem?name=${jenkinsPipelineName}&mode=org.jenkinsci.plugins.workflow.job.WorkflowJob`;
    const header = {
      'Content-Type': 'application/xml',
      'Jenkins-Crumb': `${crumb}`,
      Authorization: `${AUTH_BASIC}`,
      Cookie: `${headers['set-cookie'] ? headers['set-cookie'][0] : undefined}`,
    };
    const jenkinsFile = pipelineTemplate(codeJenkins);
    await axios.post(urlJenkins, jenkinsFile, { headers: header }).then(
      (result) => {
        res.status(200).json(`Pipeline has create ${result}`);
      },
    );
  } catch (error) {
    if (isString(error.response.data) && error.response.data.includes('A job already exists with the name')) {
      res.status(400).json(`Name ${jenkinsPipelineName} was existed`);
    } else {
      res.status(500).json({ error: error.response.data });
    }
  }
};

const getJenkinsPipelineJson = async (req, res) => {
  const {
    jenkinsJob,
  } = req.query;
  try {
    const domain = jenkinsJob.split('/job/')[0];
    const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[domain].username}:${jenkinsKey[domain].password}`)}`;
    const { data: { crumb }, headers } = await axios.get(`${domain}/crumbIssuer/api/json`, {
      headers: {
        Authorization: AUTH_BASIC,
      },
    });
    const header = {
      'Content-Type': 'application/xml',
      'Jenkins-Crumb': `${crumb}`,
      Authorization: `${AUTH_BASIC}`,
      Cookie: `${headers['set-cookie'] ? headers['set-cookie'][0] : undefined}`,
    };
    const urlJenkins = `${domain}/pipeline-model-converter/toJson`;
    console.log(AUTH_BASIC)
    const response = await axios.post(urlJenkins, { headers: header });
    console.log(response)
  } catch (error) {
    res.status(error);
    res.json({ isSuccess: false, error: error });
    throw error;
  }
};

module.exports = {
  createAgent,
  addOwners,
  createFolder,
  summaryData,
  getListFolder,
  getAllJobInFolder,
  listAgent,
  getAgentUserPermission,
  agentInfo,
  getFolderInfo,
  saveOwner,
  saveJenkinsChangeOwnerLog,
  getListJobInfolder,
  deleteFolder,
  saveJenkinsDeleteFolderLog,
  getUnuseResource,
  addResourceToProject,
  getListHostUnuseRs,
  pushJenkinsFile,
  updateAgent,
  getJenkinsPipelineJson,
};
