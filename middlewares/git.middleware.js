require('express-async-errors');

const repoDir = process.cwd();
const simpleGit = require('simple-git/promise');
const url = require('url');
const {
  removeFolder,
  createFileContent,
} = require('../helper/file.js');
const {
  checkBranchExistOnRemote,
  getCurrentBranch,
} = require('../helper/git');

const folderRoot = 'repositories/git';

const pushNewCommit = async (req, res) => {
  const {
    username,
    password,
    gitURL,
    gitRepoFile,
    gitRepoCommit,
    gitRepoBranch,
    codePipe,
  } = req.body;
  const git = simpleGit(repoDir);
  const localURL = url.parse(gitURL);
  const localRepoName = (localURL.hostname + localURL.path).replace('.git', '');
  const remote = `https://${username}:${password}@${localRepoName}.git`;
  const localPath = `${folderRoot}/${localRepoName}`;

  await removeFolder(localPath);

  await git.clone(remote, localPath).then(
    (success) => {
      if (success) {
        res.status(200).json('OK');
      }
    },
    (err) => {
      res.status(401).json('Invalid username, password or gitURL incorrect, please check again');
      if (err) {
        throw err.message;
      }
    },
  );
  await git.cwd(localPath);
  await git.branch(['-a']).then(async (result) => {
    if (await checkBranchExistOnRemote(result.all, gitRepoBranch)) {
      await git.checkout(gitRepoBranch);
      const currentBranch = await getCurrentBranch(git);
      if (currentBranch !== gitRepoBranch) {
        res.status(500).json('Cannot found branch! Please contact Administrator to check issue!');
      }
      return;
    }
    res.status(500).json('Branch is not exist! Please create branch on remote repository first!');
  }, async (err) => {
    if (err) {
      throw err;
    }
  });

  await createFileContent(localPath, gitRepoFile, codePipe);

  await git.pull('origin', gitRepoBranch, { '--no-rebase': null }).then((result) => {
    if (!result) {
      res.status(500).json('Cannot access to repository!');
    }
  }, (err) => {
    if (err) {
      throw err.message;
    }
  });

  await git.add('.');
  await git.commit(gitRepoCommit);
  await git.push('origin', gitRepoBranch).then(
    async (result) => {
      await removeFolder(localPath);
      if (result) {
        res.status(200).json('OK');
      } else {
        res.status(500).json('Cannot access to repository!');
      }
    },
    async (err) => {
      await removeFolder(localPath);
      res.status(401).json('Error, please check the information of the git repo');
      if (err) {
        throw err.message;
      }
    },
  );
};

module.exports = {
  pushNewCommit,
};
