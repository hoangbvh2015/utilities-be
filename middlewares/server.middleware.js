const {
  getServerData,
  createServerData,
  updateServerData,
  removeServerData,
} = require('../services/save-data');

const getServer = async (req, res) => {
  const servers = await getServerData();
  const result = [];
  servers.forEach((server) => {
    const value = {
      serverName: server.serverName,
      serverUrl: server.serverUrl,
      application: server.application,
      authenType: server.authenType,
      token: server.token,
      username: server.username,
      password: server.password,
    };
    result.push(value);
  });
  res.json({ servers: result });
};

const createServer = async (req, res) => {
  const { server } = req.body;
  const result = await createServerData(
    server.serverName,
    server.serverUrl,
    server.application,
    server.authenType,
    server.token,
    server.username,
    server.password,
  );
  res.status(result.statusCode);
  res.json({ message: result.statusMessage });
};

const updateServer = async (req, res) => {
  const { oldServerName, newServerData } = req.body;
  const result = await updateServerData(
    oldServerName,
    newServerData.serverName,
    newServerData.serverUrl,
    newServerData.application,
    newServerData.authenType,
    newServerData.token,
    newServerData.username,
    newServerData.password,
  );
  res.status(result.statusCode);
  res.json({ message: result.statusMessage });
};

const removeServer = async (req, res) => {
  const { server } = req.body;
  const result = await removeServerData(
    server[0].serverName,
    server[0].serverUrl,
  );
  res.status(result.statusCode);
  res.json({ message: result.statusMessage });
};

module.exports = {
  getServer,
  createServer,
  updateServer,
  removeServer,
};
