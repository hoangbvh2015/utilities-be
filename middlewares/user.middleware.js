const User = require('../models/user.model');
const userRoles = require('../models/userRoles.model');
const Project = require('../models/project.model');

const getListLdapUser = async (req, res) => {
  const {
    page = 1, limit = 10, query = '', roleCode,
  } = req.query;
  const limitValue = limit ? parseInt(limit, 10) : 10;
  const pageValue = page ? parseInt(page, 10) : 1;
  const regex = new RegExp(`.*${query}.*`, 'i');
  const sortObject = { name: 1 };
  let total = 0;
  let users = [];

  if (!roleCode || roleCode.length === 0) {
    total = await User.find({ name: { $regex: regex } }).countDocuments();
    users = await User.find(
      {
        name: {
          $regex: regex,
        },
      },
    ).sort(sortObject).skip((pageValue - 1) * limitValue).limit(limitValue);
  } else {
    total = await User.find({ name: { $regex: regex }, roleCode: { $eq: `${roleCode}` } }).countDocuments();
    users = await User.find(
      {
        name: {
          $regex: regex,
        },
        roleCode: {
          $eq: `${roleCode}`,
        },
      },
    ).sort(sortObject).skip((pageValue - 1) * limitValue).limit(limitValue);
  }

  res.json(
    {
      total,
      users,
      limit: limitValue,
      page: pageValue,
    },
  );
};

const updateUser = async (req, res) => {
  const { name, roleName, roleCode } = req.body;

  await User.updateOne(
    { name },
    { roleName, roleCode },
  );

  const listProject = await Project.find();
  if (listProject) {
    // eslint-disable-next-line no-restricted-syntax
    for (const item of listProject) {
      let projectPermission = [];
      if (item.projectPermission) {
        projectPermission = Array.isArray(item.projectPermission) ? item.projectPermission : [];
      }
      const userIndex = projectPermission.findIndex((index) => (
        index.userName === name.toLowerCase()
      ));
      if (userIndex !== -1) {
        projectPermission[userIndex].roleName = roleName;
        projectPermission[userIndex].roleCode = roleCode;
        // eslint-disable-next-line no-await-in-loop
        await Project.updateOne({ projectCode: item.projectCode }, {
          $set: {
            projectPermission,
          },
        });
      }
    }
  }

  res.json('update success');
};

const getRoleUser = async (req, res) => {
  const roles = await userRoles.find();

  res.status(200).json({ roles });
};

module.exports = {
  getListLdapUser,
  updateUser,
  getRoleUser,
};
