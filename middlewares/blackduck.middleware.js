/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
const axios = require('../services/api');
const { blackduckKey } = require('../enviroment.js');
const { saveLog } = require('../services/saveLog');
const {
  getListBlackduckProject,
  getListBlackduckUser,
  getListUserBlackduckProject,
} = require('../helper/blackduck.js');
const Project = require('../models/project.model');
const UnUseResource = require('../models/unUseResource.model');

// getProjectInfor = async (req, res, next) => {
//     try {
//         const params = req.body;
//         const document = new Action();
//         document.fsu = params.fsu;
//         document.resourceType = params.resourceType;
//         document.resourceUrl = params.resourceUrl;
//         document.createdBy = params.createdBy;
//         document.projectCode = params.projectCode;
//         document.bu = params.bu;
//         document.projectName = params.projectName;
//         document.note = {};
//         document.note.projects = [];
//         document.note.account = {};
//         await document.save();
//         next();
//     } catch (error) {
//         next(error);
//     }
// }

const createProject = async (req, next, projectName) => {
  try {
    const params = req.body;
    const { data: { bearerToken } } = await axios.blackduck.post(
      `${params.resourceUrl}/api/tokens/authenticate`,
      {},
      {
        headers: {
          Authorization: `token ${blackduckKey[params.resourceUrl]}`,
        },
      },
    );
    const result = await axios.blackduck.post(`${params.resourceUrl}/api/projects`,
      {
        name: projectName,
        versionRequest: {
          versionName: '1',
          distribution: 'EXTERNAL',
          phase: 'PLANNING',
        },
      },
      {
        headers: { Authorization: `Bearer ${bearerToken}` },
      });
    return result;
  } catch (error) {
    throw error;
  }
};

const createMultiProject = async (req, res, next) => {
  const params = req.body;
  req.blackduckProjectData = [];
  for (const projectName of params.note.projects) {
    try {
      const { data, status } = await createProject(req, next, projectName);
      req.blackduckProjectData.push({ projectName });

      if (data.status < 200 || data.status >= 300) {
        res.status(status);
        res.json({ errors: data.errors });
      }
    } catch (error) {
      throw error.message;
    }
  }
  next();
};

const checkAccountExist = async (host, token, blackduckFirstName) => {
  const { data } = await axios.blackduck.get(`${host}/api/users?q=firstName:${blackduckFirstName}`, { headers: { Authorization: `Bearer ${token}` } });

  const account = data.items && data.items.find((user) => user.firstName === blackduckFirstName);
  if (account) {
    // eslint-disable-next-line no-underscore-dangle
    return { isExisted: true, id: account._meta.href.split('/api/users/')[1], account };
  }
  return { isExisted: false };
};

const createNewBlackduckAccount = async (host, token, blackduckAccount) => {
  const headers = { Authorization: `Bearer ${token}` };
  const response = await axios.blackduck.post(`${host}/api/users`, blackduckAccount, { headers });
  return response;
};

const updateBlackduckAccount = async (host, token, blackduckAccount, id) => {
  const headers = { Authorization: `Bearer ${token}` };
  // await axios.blackduck.put(
  //   `${host}/api/users/${id}/resetpassword`,
  //   { password: blackduckAccount.password },
  //   { headers }
  // );
  const response = await axios.blackduck.put(`${host}/api/users/${id}`, blackduckAccount, { headers });
  return response;
};

const getBlackduckBearToken = async (host) => {
  const { data: { bearerToken } } = await axios.blackduck.post(`${host}/api/tokens/authenticate`, {}, {
    headers: {
      Authorization: `token ${blackduckKey[host]}`,
      Accept: 'application/vnd.blackducksoftware.user-4+json',
    },
  });

  return bearerToken;
};

const createUser = async (req, res) => {
  try {
    const { note: { account }, resourceUrl } = req.body;
    const bearerToken = await getBlackduckBearToken(resourceUrl);
    const accountInfo = { ...account, ...{ active: true } };

    const blackduckAccount = await checkAccountExist(
      resourceUrl,
      bearerToken,
      accountInfo.firstName,
    );
    if (blackduckAccount.isExisted) {
      const response = await updateBlackduckAccount(
        resourceUrl,
        bearerToken,
        accountInfo,
        blackduckAccount.id,
      );
      res.status(response.status).json({ ...accountInfo, ...blackduckAccount });
    } else {
      const {
        data,
        status,
      } = await createNewBlackduckAccount(resourceUrl, bearerToken, accountInfo);
      res.status(status).json({ ...data, ...accountInfo });
    }
  } catch (error) {
    throw error.message;
  }
};

const findUserID = async (resourceUrl, firstName) => {
  try {
    // const params = req.body;
    const { data: { bearerToken } } = await axios.blackduck.post(`${resourceUrl}/api/tokens/authenticate`, {}, {
      headers: {
        Authorization: `token ${blackduckKey[resourceUrl]}`,
      },
    });
    const { data } = await axios.blackduck.get(
      `${resourceUrl}/api/users`,
      {
        headers: { Authorization: `Bearer ${bearerToken}` },
        params: {
          filter: 'userStatus:true',
          q: `firstName:${firstName}`,
        },
      },
    );
    const targetUser = data.items.find((item) => item.firstName === firstName);
    // totalCount
    // eslint-disable-next-line no-underscore-dangle
    const linkUser = targetUser._meta.href;
    const userID = linkUser.substring(linkUser.lastIndexOf('/') + 1);
    return userID;
  } catch (error) {
    throw error;
  }
};

const findProjectID = async (resourceUrl, projectName) => {
  try {
    // const params = req.body;
    const { data: { bearerToken } } = await axios.blackduck.post(`${resourceUrl}/api/tokens/authenticate`, {}, {
      headers: {
        Authorization: `token ${blackduckKey[resourceUrl]}`,
      },
    });
    const { data } = await axios.blackduck.get(`${resourceUrl}/api/risk-profile-dashboard`,
      {
        headers: { Authorization: `Bearer ${bearerToken}` },
        params: {
          q: `${projectName}`,
        },
      });
    return data.projectRiskProfilePageView.items[0].id;
  } catch (error) {
    throw error;
  }
};

const addUserToProject = async (resourceUrl, projectname, firstName) => {
  try {
    // const { resourceUrl, note } = req.body;
    const userID = await findUserID(resourceUrl, firstName);
    const projectID = await findProjectID(resourceUrl, projectname);
    const { data: { bearerToken } } = await axios.blackduck.post(`${resourceUrl}/api/tokens/authenticate`, {},
      { headers: { Authorization: `token ${blackduckKey[resourceUrl]}` } });
    const { data, status } = await axios.blackduck.post(
      `${resourceUrl}/api/users/${userID}/roles`,
      [
        {
          role: `${resourceUrl}/api/roles/00000001-0001-0001-0001-00000000000b`,
          scope: `${resourceUrl}/api/projects/${projectID}`,
        },
        {
          role: `${resourceUrl}/api/roles/00000001-0001-0001-0001-000000000015`,
          scope: `${resourceUrl}/api/projects/${projectID}`,
        },
        {
          role: `${resourceUrl}/api/roles/00000001-0001-0001-0001-000000000013`,
          scope: `${resourceUrl}/api/projects/${projectID}`,
        },
        {
          role: `${resourceUrl}/api/roles/00000001-0001-0001-0001-000000000012`,
          scope: `${resourceUrl}/api/projects/${projectID}`,
        },
        {
          role: `${resourceUrl}/api/roles/00000001-0001-0001-0001-000000000014`,
          scope: `${resourceUrl}/api/projects/${projectID}`,
        },
      ],
      {
        headers: {
          Authorization: `Bearer ${bearerToken}`,
          'Content-Type': 'application/vnd.blackducksoftware.internal-1+json',
        },
      },
    );

    return { data, status };
  } catch (error) {
    throw error;
  }
};

const addUsertoMultiProject = async (req, res) => {
  const { project, user, resourceUrl } = req.body;
  try {
    const { status, data } = await addUserToProject(resourceUrl, project, user);
    res.status(status).json(data);
  } catch (error) {
    throw error.message;
  }
};

// returnLog = (req, res, next) => {
//     try {
//         const params = req.body;
//         Action.find({
// projectCode: params.projectCode,
//   resourceType: params.resourceType
// }, function (err, docs) {
//             res.json({resources: docs[0]});
//         });
//     } catch (error) {
//         next(error);
//     }
// }

const summaryData = async (req, res) => {
  const {
    fsu,
    bu,
    resourceUrl,
    projectCode,
    projectName,
    createdBy,
    pmAccount,
  } = req.body;
  const { blackduckAccount, blackduckProjectData } = req;
  await saveLog(
    'blackduck',
    {
      fsu,
      bu,
      projectCode,
      projectName,
      createdBy,
      pmAccount,
      resources:
      {
        resourceUrl,
        blackduckAccount,
        blackduckProjectData,
      },
    },
  );
  res.json({
    blackduckAccount,
    blackduckProjectData,
  });
};

const listProject = async (req, res) => {
  const params = req.query;
  const { data, status } = await getListBlackduckProject(params.host, params.limit);

  if (status >= 200 && status < 300) {
    res.json({ items: data.items, totalCount: data.totalCount });
  } else {
    res.status(status);
    res.json({ errors: data.errors });
  }
};

const getUsers = async (req, res) => {
  const params = req.query;
  const { data, status } = await getListBlackduckUser(params.host, params.limit);

  if (status >= 200 && status < 300) {
    res.json({ users: data.items, totalCount: data.totalCount });
  } else {
    res.status(status);
    res.json({ errors: data.errors });
  }
};

const getUserProjects = async (req, res) => {
  const params = req.query;
  const { data, status } = await getListUserBlackduckProject(params.host, params.usersUrl);

  if (status >= 200 && status < 300) {
    res.json({ users: data.items, totalCount: data.totalCount });
  } else {
    res.status(status);
    res.json({ errors: data });
  }
};

const getListHostUnuseRs = async (req, res) => {
  const record = await UnUseResource.find({ resourceType: { $eq: 'blackduck' } });

  if (record) {
    const listHost = record.reduce((acc, current) => {
      const urlSplit = current.jobURL.split('/');
      const host = [urlSplit[0], urlSplit[1], urlSplit[2]].join('/');
      const checkExist = acc.find((item) => item.value === host);
      if (!checkExist) {
        acc.push({
          label: current.hostName,
          value: host,
        });
      }
      return acc;
    }, []);
    res.json({ success: true, listHost });
  } else {
    res.status(400).json({ success: false, message: 'Connect DB failed!' });
  }
};

const getUnuseResource = async (req, res) => {
  const {
    query = '',
    page = 1,
    pageSize = 100,
    host = '',
  } = req.query;

  const record = await UnUseResource.find({ resourceType: { $eq: 'blackduck' } });
  if (record) {
    const start = (page - 1) * pageSize;
    const end = page * pageSize;
    const listItem = record.filter(
      (item) => (item.displayName.includes(query) && item.jobURL.includes(host)),
    );
    const total = listItem.length;
    const result = listItem.slice(start, end);
    res.json({ success: true, result, total });
  } else {
    res.status(400).json({ success: false, message: 'Connect DB failed!' });
  }
};

const addResourceToProject = async (req, res) => {
  const { projectCode, listResourceChecked } = req.body;
  const record = await Project.findOne({ projectCode });

  if (record) {
    const { resourceData } = record;

    for (const value of listResourceChecked) {
      const checkResource = resourceData.blackduck.findIndex((item) => (
        item.jobURL === value.jobURL
      ));
      if (checkResource !== -1) {
        resourceData.blackduck[checkResource] = value;
      } else {
        resourceData.blackduck.push(value);
      }

      await UnUseResource.updateOne({ jobURL: value.jobURL }, {
        $set: {
          proejctAssigned: [projectCode],
        },
      });
    }

    await Project.updateOne({ projectCode }, {
      $set: {
        resourceData,
      },
    });
    res.status(200);
  } else {
    res.status(404);
  }
  res.json(listResourceChecked);
};

const getAccountInfo = async (req, res) => {
  const { host, projectName } = req.query;
  const bearerToken = await getBlackduckBearToken(host);
  const { data, status } = await axios.blackduck.get(
    `${host}/api/projects?q=name:${projectName}`,
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  );

  if (data.items) {
    const project = data.items.find((item) => item.name === projectName);
    if (project) {
      // eslint-disable-next-line no-underscore-dangle
      const projectId = project._meta.href.split('/projects/').pop();
      const resUserProject = await axios.blackduck.get(
        `${host}/api/projects/${projectId}/users`,
        {
          headers: {
            Authorization: `Bearer ${bearerToken}`,
          },
        },
      );

      if (resUserProject.data && resUserProject.data.items) {
        const listUser = resUserProject.data.items.filter((item) => item.firstName !== 'System');
        res.status(resUserProject.status).json({ listAccount: listUser });
      } else {
        res.status(resUserProject.status).json({ error: resUserProject.data.errorMessage });
      }
    }
  } else {
    res.status(status).json({ error: data.errorMessage });
  }
};

const updateAccount = async (req, res) => {
  const { host, accountId, account } = req.body;
  const bearerToken = await getBlackduckBearToken(host);

  try {
    const { status, data } = await axios.blackduck.put(
      `${host}/api/users/${accountId}`,
      account,
      {
        headers: {
          Authorization: `Bearer ${bearerToken}`,
        },
      },
    );
    res.status(status).json({ data });
  } catch (error) {
    throw error;
  }
};

const apiCheckAccountExist = async (req, res) => {
  const { host, firstName } = req.query;
  const bearerToken = await getBlackduckBearToken(host);
  const blackduckAccount = await checkAccountExist(host, bearerToken, firstName);
  res.json({ ...blackduckAccount });
};

const apiGetProject = async (req, res) => {
  const { projectCode } = req.query;
  const project = await Project.findOne({ projectCode: { $eq: projectCode } });

  res.json({ project });
};

const apiResetpassword = async (req, res) => {
  const { host, accountId, newPassword } = req.body;
  const bearerToken = await getBlackduckBearToken(host);

  try {
    await axios.blackduck.put(
      `${host}/api/users/${accountId}/resetpassword`,
      { password: newPassword },
      { headers: { Authorization: `Bearer ${bearerToken}` } },
    );
    res.status(200).json({ ...req.body });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  addUsertoMultiProject,
  createMultiProject,
  createUser,
  summaryData,
  listProject,
  getUsers,
  getUserProjects,
  getUnuseResource,
  addResourceToProject,
  getListHostUnuseRs,
  getAccountInfo,
  updateAccount,
  apiCheckAccountExist,
  apiGetProject,
  apiResetpassword,
};
