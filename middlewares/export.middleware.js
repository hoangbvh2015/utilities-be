const fs = require('fs');
const path = require('path');
const archiver = require('archiver');
const fsExtra = require('fs-extra');

const folderRoot = 'public/pipeline';

const createChildrenFolder = async (origin, pathFolder) => {
  if (origin.isLeaf) {
    // create file yaml
    if (fs.existsSync(pathFolder)) {
      fs.writeFile(path.join(pathFolder, `${origin.title}.yaml`), JSON.stringify(origin.jsonData), () => { });
    }
  } else {
    const pathFolderParent = path.join(pathFolder, origin.title);
    // create folder parent
    if (fs.existsSync(pathFolder) && !fs.existsSync(pathFolderParent)) {
      fs.mkdirSync(pathFolderParent);
    }

    if (origin.children.length > 0 && fs.existsSync(pathFolderParent)) {
      origin.children.forEach((item) => {
        createChildrenFolder(item, pathFolderParent);
      });
    }
  }
};

const createFolder = async (req, res) => {
  const { data } = req.body;
  if (await fs.existsSync(folderRoot)) {
    await fsExtra.remove(folderRoot);
  }

  await fs.mkdirSync(folderRoot);
  await fs.mkdirSync(path.join(folderRoot, 'folders'));
  await createChildrenFolder(data, path.join(folderRoot, 'folders'));
  const output = await fs.createWriteStream(`${folderRoot}/${data.title}.zip`);
  const archive = archiver('zip', {
    zlib: { level: 9 }, // Sets the compression level.
  });

  // pipe archive data to the file
  archive.pipe(output);

  // append files from a sub-directory and naming it `new-subdir` within the archive
  archive.directory(path.join(folderRoot, 'folders'), false);

  // finalize the archive (ie we are done appending files but streams have to finish yet)
  archive.finalize();

  // create folder done
  output.on('close', () => {
    res.json({ status: 'done' });
  });
};

const exportPipeline = async (req, res) => {
  try {
    if (fs.existsSync(folderRoot)) {
      const folders = await fs.readdirSync(folderRoot);
      const fileName = folders.filter((item) => item.includes('.zip'));
      res.writeHead(200, {
        'Content-Type': 'application/zip',
        'Content-Disposition': `attachment; filename=${fileName[0]}`,
      });

      const readStream = fs.createReadStream(path.join(folderRoot, fileName[0]));
      readStream.pipe(res);
      await fsExtra.remove(folderRoot);
    } else {
      res.send('export failed');
    }
  } catch (error) {
    res.send('export failed');
  }
};

module.exports = {
  createFolder,
  exportPipeline,
};
