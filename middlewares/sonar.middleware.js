/* eslint-disable guard-for-in */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
const { saveLog } = require('../services/saveLog');
const {
  getAllSonarProjects,
  deleteSonarProjects,
  createSonarProject,
  getSonarProjectUsers,
  getSonarProjectGroups,
  addSonarUserMultiplePermission,
  addSonarGroupMultiplePermission,
  deleteSonarUserMultiplePermission,
  deleteSonarGroupMultiplePermission,
  getListSonarProject,
  getListSonarUser,
  createSonarUser,
  updateSonarUser,
  deactivateUser,
} = require('../helper/sonar');
const Project = require('../models/project.model');
const UnUseResource = require('../models/unUseResource.model');

const deleteProjects = async (req, res) => {
  const params = req.body;
  const { status, errors } = await deleteSonarProjects(params.host, params.projectKeys.toString());

  if (params.projectCode) {
    const record = await Project.findOne({ projectCode: params.projectCode });

    record.resourceData.sonar = record.resourceData.sonar.filter((item) => (
      item.jobURL !== params.jobURL
    ));

    if (record) {
      await Project.updateOne({ projectCode: params.projectCode }, {
        $set: {
          resourceData: record.resourceData,
        },
      });

      await UnUseResource.updateOne({ jobURL: params.jobURL }, {
        $set: {
          proejctAssigned: [],
        },
      });
    }
  }

  if (status >= 200 && status < 300) {
    res.json({ status });
  } else {
    res.status(status);
    res.json({ errors });
  }
};

const getProjectUsers = async (req, res) => {
  const params = req.query;
  const result = await getSonarProjectUsers(params.host, params.projectKey, params.p, params.ps);

  if (result && (result.status < 200 || result.status >= 300)) {
    res.status(result.status);
    res.json({ errors: result.errors });
  } else {
    res.json({ users: result.users });
  }
};

const getProjectGroups = async (req, res) => {
  const params = req.query;
  const result = await getSonarProjectGroups(params.host, params.projectKey, params.p, params.ps);

  if (result && (result.status < 200 || result.status >= 300)) {
    res.status(result.status);
    res.json({ errors: result.errors });
  } else {
    res.json({ groups: result.groups });
  }
};

const createResource = async (req, _res, next) => {
  const {
    resourceUrl, project, admin, pmAccount,
  } = req.body;
  req.successProject = [];
  let errors = {};
  project.permissions = [];
  const dataCreateSonar = await createSonarProject(
    resourceUrl,
    project.projectName,
    project.projectKey,
  );
  if (dataCreateSonar && (dataCreateSonar.status < 200 || dataCreateSonar.status >= 300)) {
    errors = dataCreateSonar;
  }

  // SET ADMIN role for PM
  const setPermissionForPM = await addSonarUserMultiplePermission(
    resourceUrl,
    project.projectKey,
    pmAccount.toLowerCase(),
    true,
  );
  if (setPermissionForPM.errors.status < 200 || setPermissionForPM.errors.status >= 300) {
    errors = setPermissionForPM.errors;
  }

  // SET all roles except for admin role for another member in project
  for (const projectUser of project.projectUsers) {
    const createdPermissionList = await addSonarUserMultiplePermission(
      resourceUrl,
      project.projectKey,
      projectUser,
      admin,
    );
    project.permissions.push(
      {
        login: projectUser,
        permissions: createdPermissionList.permissionAdded,
      },
    );
    if (createdPermissionList.errors.status < 200 || createdPermissionList.errors.status >= 300) {
      errors = createdPermissionList.errors;
    }
  }

  req.successProject.push(project);
  if (errors.status < 200 || errors.status >= 300) {
    req.errors = errors;
  }
  next();
};

const addUserProjectPermission = async (req, res) => {
  const params = req.body;
  const { users } = params;
  let errors = {};
  for (let i = 0; i < users.length; i += 1) {
    const dataAddSonar = await addSonarUserMultiplePermission(
      params.host,
      params.projectKey,
      users[i].userName,
      params.admin,
    );
    errors = dataAddSonar.errors;
  }

  if (errors.status < 200 || errors.status >= 300) {
    res.status(errors.status);
    res.json({ errors: errors.errors });
  } else {
    res.json({
      status: 'OK',
    });
  }
};

const addGroupProjectPermission = async (req, res) => {
  const params = req.body;
  const { groups } = params;
  let errors = {};
  for (let i = 0; i < groups.length; i += 1) {
    const data = await addSonarGroupMultiplePermission(
      params.host,
      params.projectKey,
      groups[i],
      params.admin,
    );
    errors = data.errors;
  }

  if (errors.status < 200 || errors.status >= 300) {
    res.status(errors.status);
    res.json({ errors: errors.errors });
  } else {
    res.json({
      status: 'OK',
    });
  }
};

const deleteUserProjectPermission = async (req, res) => {
  const params = req.body;
  const { users } = params;
  let errors = {};
  for (let i = 0; i < users.length; i += 1) {
    const data = await deleteSonarUserMultiplePermission(
      params.host,
      params.projectKey,
      users[i],
      params.admin,
    );
    errors = data.errors;
  }

  if (errors.status < 200 || errors.status >= 300) {
    res.status(errors.status);
    res.json({ errors: errors.errors });
  } else {
    res.json({
      status: 'OK',
    });
  }
};

const deleteGroupProjectPermission = async (req, res) => {
  const params = req.body;
  const { groups } = params;
  let errors = {};
  for (let i = 0; i < groups.length; i += 1) {
    const data = await deleteSonarGroupMultiplePermission(
      params.host,
      params.projectKey,
      groups[i],
      params.admin,
    );
    errors = data.errors;
  }

  if (errors.status < 200 || errors.status >= 300) {
    res.status(errors.status);
    res.json({ errors: errors.errors });
  } else {
    res.json({
      status: 'OK',
    });
  }
};

const summaryData = async (req, res) => {
  const {
    fsu,
    bu,
    resourceUrl,
    projectCode,
    projectName,
    createdBy,
    pmAccount,
  } = req.body;
  const sonarProjectData = req.successProject.map((project) => ({
    link: `${resourceUrl}/dashboard?id=${project.projectKey}`,
    permissions: project.permissions,
  }));

  if (req.successProject.length > 0) {
    await saveLog(
      'sonars',
      {
        fsu,
        bu,
        projectCode,
        projectName,
        createdBy,
        pmAccount,
        resources:
        {
          sonarProjectData,
        },
      },
    );
  }

  if (req.errors) {
    res.status(req.errors.status).json(req.errors.errors);
  } else {
    res.json({ isSuccess: true, sonarProjectData });
  }
};

const listProject = async (req, res) => {
  const params = req.query;
  const projects = await getAllSonarProjects(params.host);

  if (projects.errors.status < 200 || projects.errors.status >= 300) {
    res.status(projects.errors.status);
    res.json({ errors: projects.errors.errors });
  } else {
    res.json(projects.sonarData);
  }
};

const searchProject = async (req, res) => {
  const params = req.query;
  const projects = await getListSonarProject(
    params.host,
    params.page,
    params.pageSize,
    params.projects,
  );
  res.json(projects);
};

const searchUser = async (req, res) => {
  const params = req.query;
  const projects = await getListSonarUser(
    params.host,
    params.q,
  );
  res.json(projects);
};

const getListHostUnuseRs = async (req, res) => {
  const record = await UnUseResource.find({ resourceType: { $eq: 'sonar' } });

  if (record) {
    const listHost = record.reduce((acc, current) => {
      const urlSplit = current.jobURL.split('/');
      const host = [urlSplit[0], urlSplit[1], urlSplit[2]].join('/');
      const checkExist = acc.find((item) => item.value === host);
      if (!checkExist) {
        acc.push({
          label: current.hostName,
          value: host,
        });
      }
      return acc;
    }, []);
    res.json({ success: true, listHost });
  } else {
    res.status(400).json({ success: false, message: 'Connect DB failed!' });
  }
};

const getUnuseResource = async (req, res) => {
  const {
    query = '',
    page = 1,
    pageSize = 100,
    host = '',
  } = req.query;

  const record = await UnUseResource.find({ resourceType: { $eq: 'sonar' } });
  if (record) {
    const start = (page - 1) * pageSize;
    const end = page * pageSize;
    const listItem = record.filter(
      (item) => (item.displayName.includes(query) && item.jobURL.includes(host)),
    );
    const total = listItem.length;
    const result = listItem.slice(start, end);
    res.json({ success: true, result, total });
  } else {
    res.status(400).json({ success: false, message: 'Connect DB failed!' });
  }
};

const addResourceToProject = async (req, res) => {
  const { projectCode, listResourceChecked } = req.body;
  const record = await Project.findOne({ projectCode });

  if (record) {
    const { resourceData } = record;

    for (const value of listResourceChecked) {
      const checkResource = resourceData.sonar.findIndex((item) => item.jobURL === value.jobURL);
      if (checkResource !== -1) {
        resourceData.sonar[checkResource] = value;
      } else {
        resourceData.sonar.push(value);
      }
      await UnUseResource.updateOne({ jobURL: value.jobURL }, {
        $set: {
          proejctAssigned: [projectCode],
        },
      });
    }

    await Project.updateOne({ projectCode }, {
      $set: {
        resourceData,
      },
    });
    res.status(200);
  } else {
    res.status(404);
  }
  res.json(listResourceChecked);
};

const createUser = async (req, res) => {
  const { status, message } = await createSonarUser(req.body);
  res.status(status).json(message);
};

const updateUser = async (req, res) => {
  const { status, message } = await updateSonarUser(req.body);
  res.status(status).json(message);
};

const changeStatusUser = async (req, res) => {
  const {
    host, login, active = false, name, newPassword,
  } = req.body;
  let status = 200;
  let message = '';
  if (active) {
    const reactiveRes = await createSonarUser({
      host,
      login,
      name,
      password: newPassword,
    });
    status = reactiveRes.status;
    message = reactiveRes.message;
  } else {
    const deactiveRes = await deactivateUser({ host, login });
    status = deactiveRes.status;
    message = deactiveRes.message;
  }
  res.status(status).json(message);
};

module.exports = {
  createResource,
  summaryData,
  listProject,
  deleteProjects,
  addUserProjectPermission,
  addGroupProjectPermission,
  deleteUserProjectPermission,
  deleteGroupProjectPermission,
  getProjectUsers,
  getProjectGroups,
  searchProject,
  searchUser,
  getUnuseResource,
  addResourceToProject,
  getListHostUnuseRs,
  createUser,
  updateUser,
  changeStatusUser,
};
