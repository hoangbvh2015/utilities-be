const { isValidToken } = require('../helper/auth');
const User = require('../models/user.model');
const Project = require('../models/project.model');

const verifyRequestAuthentication = async (req, res, next) => {
  const { authorization } = req.headers;
  const record = await User.findOne({ token: { $eq: authorization } });
  if (isValidToken(authorization) && record) {
    req.role = record.roleCode;
    req.userName = record.name;
    next();
  } else {
    res.status(401).send('Unauthorized');
  }
};

const isRoleAdmin = async (req, res, next) => {
  const { role } = req;
  if (role === 'admin') {
    next();
  } else {
    res.status(403).send('Require Admin Role!');
  }
};

const isRoleSubAdmin = async (req, res, next) => {
  const { role } = req;
  if (role === 'sub_admin' || role === 'admin') {
    next();
  } else {
    res.status(403).send('Require Sub Admin Role!');
  }
};

const isRoleProjectAdmin = async (req, res, next) => {
  const { role } = req;
  if (role === 'sub_admin' || role === 'admin' || role === 'project_admin') {
    next();
  } else {
    res.status(403).json({ message: 'Require Project Admin Role!' });
  }
};

const isRoleProjectUser = async (req, res, next) => {
  const { role } = req;
  if (role === 'sub_admin' || role === 'admin' || role === 'project_admin' || role === 'project_user') {
    next();
  } else {
    res.status(403).json({ message: 'Require Project Admin Role!' });
  }
};

const isProjectPermission = (resource) => async (req, res, next) => {
  const { userName } = req;
  let { role } = req;
  const { projectCode } = req.body;
  const record = await Project.findOne({ projectCode });

  if (!record && (role === 'sub_admin' || role === 'admin')) {
    next();
    return;
  }

  if (Array.isArray(record.projectPermission) && record.projectPermission.length > 0) {
    const user = record.projectPermission.find((item) => (
      (item.userName && item.userName.toLowerCase()) === (userName && userName.toLowerCase())
    ));
    if (user) {
      role = user.roleCode;
    }
  }

  if (role === 'project_user' || role === 'project_admin') {
    let userPermission = {};
    if (Array.isArray(record.projectPermission)) {
      userPermission = record.projectPermission.find((item) => (
        item.userName.toLowerCase() === userName.toLowerCase()
      ));
    }

    if (role === 'project_admin' && (record.pmAccount && record.pmAccount.toLowerCase()) === userName.toLowerCase()) {
      next();
    } else if (resource === 'edit_user_project') {
      if (userPermission.edit) {
        next();
      } else {
        res.status(400).json({ message: 'Project User not edit permission' });
      }
    } else if (resource === 'change_role_user_project') {
      if (['admin', 'sub_admin', 'project_admin'].includes(userPermission.roleCode)) {
        next();
      } else {
        res.status(400).json({ message: 'Project User not edit permission' });
      }
    } else if (resource && resource !== 'edit_user_project') {
      if (userPermission) {
        const permission = userPermission.permission[resource];
        if (permission.view && permission.edit) {
          next();
        } else {
          res.status(400).json({ message: 'Project User not edit permission' });
        }
      } else {
        res.status(400).json({ message: 'Project User not view permission' });
      }
    } else {
      res.status(400).json({ message: 'Project User not edit permission' });
    }
  } else if (role === 'sub_admin' || role === 'admin') {
    next();
  } else {
    res.status(403).send('Require Project User Role!');
  }
};

module.exports = {
  verifyRequestAuthentication,
  isRoleAdmin,
  isRoleSubAdmin,
  isRoleProjectAdmin,
  isProjectPermission,
  isRoleProjectUser,
};
