const axios = require('axios');
const Request = require('../models/request.model');
const { ezServiceAccount, servers } = require('../enviroment');
const collectRequestTicket = require('../scripts/collectRequestTicket');

// const ezServer = 'https://devops.fsoft.com.vn';
const ezServer = servers.ez;

const personAssignee = 'devopsgrafana@fsoft.com.vn';
let tokenGlobal = null;

const getListRequest = async (req, res) => {
  const { page, limit, query = '' } = req.query;

  const pageValue = page ? parseInt(page, 10) : 1;
  const limitValue = limit ? parseInt(limit, 10) : 10;
  const regexQuery = new RegExp(`.*${query}.*`, 'i');

  const projects = await Request.find({ projectCode: { $regex: regexQuery } });

  const listObjectNeedApproval = projects.filter((item) => item.requestStatus === 'open');
  const listObjectAproved = projects.filter((item) => item.requestStatus !== 'open');

  const start = (pageValue - 1) * limitValue;
  const end = pageValue * limitValue;
  const data = [...listObjectNeedApproval, ...listObjectAproved].slice(start, end);

  const total = await Request.find({ projectCode: { $regex: regexQuery } }).countDocuments();
  // await collectRequestTicket();

  res.json({
    data,
    total,
    limit: limitValue,
    page: pageValue,
  });
};

const reloadTable = async (req, res) => {
  const pageValue = 1;
  const limitValue = 10;

  const responseCollect = await collectRequestTicket();
  const projects = await Request.find();

  const listObjectNeedApproval = projects.filter((item) => item.requestStatus === 'open');
  const listObjectAproved = projects.filter((item) => item.requestStatus !== 'open');
  const start = (pageValue - 1) * limitValue;
  const end = pageValue * limitValue;
  const data = [...listObjectNeedApproval, ...listObjectAproved].slice(start, end);
  const total = await Request.find().countDocuments();

  res.status(responseCollect.status).json({
    data,
    total,
    limit: limitValue,
    page: pageValue,
    ...responseCollect,
  });
};

const updateRequest = async (req, res) => {
  const { userName } = req;
  const { ticketCode, data } = req.body;
  const record = await Request.findOne({ ticketCode });

  if (record) {
    await Request.updateOne({ ticketCode }, {
      $set: {
        ...data,
        approver: userName,
      },
    });
    res.json({ message: 'Update message success!' });
  } else {
    res.status(400).json({ message: 'TicketCode not exists' });
  }
};

const addCommentToServer = async (commentData) => {
  const { ticketCode, comment, token } = commentData;
  const commentObj = {
    objectTypeKey: 'otk_comment',
    actionKey: '',
    whereFields: [{ key: 'id', value: '' }],
    fields: [
      {
        key: 'fk_object_type_key',
        value: 'otk_dsc_services_01',
      },
      {
        key: 'fk_object_key',
        value: ticketCode,
      },
      {
        key: 'fk_otk_comment_comment',
        value: comment,
      },
      {
        key: 'fk_otk_comment_attached_file',
        value: '',
      },
    ],
  };

  try {
    const commentResponse = await axios.post(`${ezServer}/services/change/insertObject`, commentObj, {
      headers: {
        'access-token': token,
      },
    });

    return { success: true, message: 'Add comment ticket success', data: commentResponse.data };
  } catch (error) {
    return { success: false, message: 'Add comment ticket failed', data: error.response && error.response.data };
  }
};

const addCommentTicket = async (req, res) => {
  const { ticketCode, comment } = req.body;

  try {
    const { data: token } = await axios.post(`${servers.ezAuthen}/slo/authen/login`, {
      ...ezServiceAccount[ezServer],
    });

    tokenGlobal = token['access-token'];

    const commentResponse = await addCommentToServer({
      ticketCode, comment, token: token['access-token'],
    });

    if (!commentResponse.success) {
      res.status(400).json({ success: false, data: commentResponse.data });
    }
    res.status(200).json({ success: false, data: commentResponse.data });

    res.json({ message: 'Add comment ticket success', data: commentResponse.data });
  } catch (error) {
    if (error.response && error.response.status === 401) {
      const commentResponse = await addCommentToServer({
        ticketCode, comment, token: tokenGlobal,
      });

      if (!commentResponse.success) {
        res.status(400).json({ success: false, data: commentResponse.data });
      }
    } else {
      res.status(400).json({ success: false, data: error.response.data });
    }
  }
};

const filterOwner = async (token, ticketCode, stepService) => {
  try {
    const bodyFindOwner = {
      objectTypeKey: 'otk_ticket_task_v',
      actionKey: '',
      filter: `fk_otk_ticket_task_v_ticket_code like ${ticketCode} and if('super_admin' = 'admin', tbl_ticket_task_v.company in ('6600000000','All'),if('super_admin' = 'super_admin', 1 = 1, 1 = 0))`,
      orderBy: ' ORDER BY fk_otk_ticket_task_v_created_time ASC ',
      fields: 'id,fk_otk_ticket_task_v_owner,fk_otk_ticket_task_v_task_name,fk_otk_ticket_task_v_status',
      pageIndex: 1,
      pageSize: 20,
      locale: 'vn-VN',
      viewType: 'list',
    };

    const api = `${ezServer}/services//screen/listObjectHtml`;

    const { data } = await axios.post(api, {
      ...bodyFindOwner,
    }, { headers: { 'access-token': token } });

    const ticketManagementItem = data.find((item) => {
      const taskName = item.fields.find((elem) => elem.key === 'fk_otk_ticket_task_v_task_name');
      return taskName.value === stepService;
    });

    if (ticketManagementItem) {
      const ownerValue = ticketManagementItem.fields.find((item) => item.key === 'fk_otk_ticket_task_v_owner');
      const idTicketManagement = ticketManagementItem.fields.find((item) => item.key === 'id');
      const statusTicketManagement = ticketManagementItem.fields.find((item) => item.key === 'fk_otk_ticket_task_v_status');

      return {
        idTicketManagement: idTicketManagement.value,
        ownerValue: ownerValue.value,
        statusTicketManagement: statusTicketManagement.value,
      };
    }

    return {};
  } catch (error) {
    return {};
  }
};

const changeOwner = async (token, ticketCode, stepService) => {
  const {
    ownerValue, idTicketManagement, statusTicketManagement,
  } = await filterOwner(token, ticketCode, stepService);

  const userOwner = `${ezServiceAccount[ezServer].username}@fsoft.com.vn`;
  if (ownerValue && !ownerValue.split(',').includes(userOwner)) {
    const bodyChangeOwner = {
      objectTypeKey: 'otk_ticket_task',
      actionKey: 'ak_otk_ticket_task_edit',
      whereFields: [{ key: 'id', value: idTicketManagement }],
      updateFields: [{
        key: 'fk_otk_ticket_task_owner',
        value: `${ownerValue},${userOwner},${personAssignee}`,
      }],
    };

    const api = `${ezServer}/services//change/updateObject`;

    const { status } = await axios.post(api, {
      ...bodyChangeOwner,
    }, { headers: { 'access-token': token } });

    if (status === 200) {
      return {
        idTicketManagement,
        statusTicketManagement,
      };
    }
  } else {
    return {
      idTicketManagement,
      statusTicketManagement,
    };
  }
  return null;
};

const getTicketInfo = async (token, ticketCode) => {
  const inforRq = {
    webPanelKey: 'wpk_dsc_services_01_form_view_content',
    objectTypeKey: 'otk_dsc_services_01',
    objectKey: null,
    viewType: 'view',
    filter: ` and fk_ticket_code = ${ticketCode}`,
  };

  const { data: ticketInfo } = await axios.post(`${ezServer}/services/screen/objectHtml`, {
    ...inforRq,
  }, { headers: { 'access-token': token } });

  const ticketId = ticketInfo.fields.find((item) => item.key === 'id');
  const implementer = ticketInfo.fields.find((item) => item.key === 'fk_otk_dsc_services_01_assignee');

  return {
    ticketId: ticketId && ticketId.value,
    implementer: implementer && implementer.value,
  };
};

const updateHelpDeskConsultant = async ({
  description, resource = '', token, ticketId,
}) => {
  let serviceApplyValue = '';

  if (resource.includes('jenkins')) {
    serviceApplyValue += ',dsc_service_apply_Jenkins';
  }

  if (resource.includes('sonar')) {
    serviceApplyValue += ',dsc_service_apply_Sonar';
  }

  if (resource.includes('coverity')) {
    serviceApplyValue += ',dsc_service_apply_Coverity';
  }

  if (resource.includes('blackduck')) {
    serviceApplyValue += ',dsc_service_apply_Coverity';
  }

  serviceApplyValue = serviceApplyValue.slice(1);

  const bodyUpdate = {
    objectTypeKey: 'otk_dsc_services_01',
    actionKey: 'ak_otk_dsc_services_01_owner_update_1',
    whereFields: [
      { key: 'id', value: ticketId },
    ],
    updateFields: [
      {
        key: 'fk_otk_dsc_services_01_issue_type',
        value: 'dsc_issue_type_request_cicd',
      },
      {
        key: 'fk_otk_dsc_services_01_assignee',
        value: personAssignee,
      },
      {
        key: 'fk_otk_dsc_services_01_description',
        value: description,
      },
      {
        key: 'fk_otk_dsc_services_01_solution_tech_request',
        value: '',
      },
      {
        key: 'fk_otk_dsc_services_01_estimate_effort_tech_request',
        value: '',
      },
      {
        key: 'fk_otk_dsc_services_01_original_estimate_tech_request',
        value: '',
      }, {
        key: 'fk_otk_dsc_services_01_proiority',
        value: 'dsc_priority_Medium',
      },
      {
        key: 'fk_otk_dsc_services_01_severity',
        value: 'dsc_serverity_Minor',
      },
      {
        key: 'fk_otk_dsc_services_01_plan_start',
        value: '',
      },
      {
        key: 'fk_otk_dsc_services_01_due_date',
        value: '',
      },
      {
        key: 'fk_otk_dsc_services_01_service_apply',
        value: serviceApplyValue,
      },
      {
        key: 'fk_otk_dsc_services_01_attachment',
        value: '',
      },
      {
        key: 'fk_otk_dsc_services_01_technical_request',
        value: 'dsc_technical_yes',
      },
      {
        key: 'fk_otk_dsc_services_01_request_type_approve', value: '',
      },
    ],
  };

  const api = `${ezServer}/services//change/updateObject`;

  const { data } = await axios.post(api, {
    ...bodyUpdate,
  }, { headers: { 'access-token': token } });

  if (data && data.error.length === 0) {
    return {
      success: true,
    };
  }

  return {
    success: false,
    data,
  };
};

const finishTaskStep = async (dataFinish) => {
  const {
    accessToken,
    idTicketManagement,
    finishTime,
    // ticketCode,
    planEffort,
    actualEffort,
    taskNote,
    taskAttachment,
    taskResult,
    stepName,
  } = dataFinish;
  // const { implementer } = await getTicketInfo(accessToken, ticketCode);

  const bodyUpdate = {
    objectTypeKey: 'otk_ticket_task',
    actionKey: 'ak_otk_ticket_task_done',
    whereFields: [{ key: 'id', value: idTicketManagement }],
    updateFields: [
      { key: 'fk_otk_ticket_task_finish_time', value: finishTime || '' },
      { key: 'fk_otk_ticket_task_implementer', value: personAssignee },
      { key: 'fk_otk_ticket_task_plan_effort', value: planEffort || '' },
      { key: 'fk_otk_ticket_task_actual_effort', value: actualEffort || '' },
      { key: 'fk_otk_ticket_task_note', value: taskNote || '' },
      { key: 'fk_otk_ticket_task_attachment', value: taskAttachment || '' },
    ],
  };

  if (stepName === 'Helpdesk Consultant') {
    bodyUpdate.actionKey = 'ak_otk_ticket_task_done_update';
    bodyUpdate.updateFields.push({ key: 'fk_otk_ticket_task_result', value: taskResult });
  }

  const api = `${ezServer}/services//change/updateObject`;

  const { data } = await axios.post(api, {
    ...bodyUpdate,
  }, { headers: { 'access-token': accessToken } });

  if (data && data.error.length === 0) {
    return { success: true, message: `${stepName} Ticket Step Submit success!`, data };
  }
  return { success: false, message: `${stepName} Ticket Step Submit failed!`, data };
};

// const startTaskStep = async (dataStart) => {
//   const {
//     accessToken,
//     idTicketManagement,
//     planEffort,
//     actualEffort,
//     taskNote,
//     taskStart,
//     stepName,
//   } = dataStart;

//   const bodyUpdate = {
//     objectTypeKey: 'otk_ticket_task',
//     actionKey: 'ak_otk_ticket_task_start',
//     whereFields: [{ key: 'id', value: idTicketManagement }],
//     updateFields: [
//       { key: 'fk_otk_ticket_task_plan_effort', value: planEffort || '' },
//       { key: 'fk_otk_ticket_task_actual_effort', value: actualEffort || '' },
//       { key: 'fk_otk_ticket_task_note', value: taskNote || '' },
//       { key: 'fk_otk_ticket_task_start', value: taskStart || '' },
//     ],
//   };

//   const api = `${ezServer}/services//change/updateObject`;

//   const { data } = await axios.post(api, {
//     ...bodyUpdate,
//   }, { headers: { 'access-token': accessToken } });

//   if (data && data.error.length === 0) {
//     return { success: true, message: `${stepName} Ticket Step Submit success!`, data };
//   }
//   return { success: false, message: `${stepName} Ticket Step Submit failed!`, data };
// };

// const updateConfirmCss = async (dataStart) => {
//   const {
//     accessToken,
//     ticketId,
//     supportDetail,
//     timeSupport,
//     guideline,
//     supportAttitude,
//     acceptanceNoteRate,
//     stepName,
//   } = dataStart;

//   const bodyUpdate = {
//     objectTypeKey: 'otk_dsc_services_01',
//     actionKey: 'ak_otk_dsc_services_01_requester_update_1',
//     whereFields: [{ key: 'id', value: ticketId }],
//     updateFields: [
//       { key: 'fk_otk_dsc_services_01_support_detail', value: supportDetail || '5' },
//       { key: 'fk_otk_dsc_services_01_time_support', value: timeSupport || '5' },
//       { key: 'fk_otk_dsc_services_01_guideline', value: guideline || 'dsc_rate_guildeline_1' },
//       { key: 'fk_otk_dsc_services_01_support_attitude', value: supportAttitude || '5' },
// {
//   key: 'fk_otk_dsc_services_01_acceptance_note_rate',
//   value: acceptanceNoteRate || 'Done'
// },
//     ],
//   };

//   const api = `${ezServer}/services//change/updateObject`;

//   const { data } = await axios.post(api, {
//     ...bodyUpdate,
//   }, { headers: { 'access-token': accessToken } });

//   if (data && data.error.length === 0) {
//     return { success: true, message: `${stepName} Ticket Step Update success!`, data };
//   }
//   return { success: false, message: `${stepName} Ticket Step Update failed!`, data };
// };

const handleHelpdeskConsultantStep = async (dataObj) => {
  const {
    accessToken,
    ticketCode,
    description,
    resource,
    ticketId,
    stepName,
  } = dataObj;

  const { idTicketManagement } = await changeOwner(accessToken, ticketCode, 'Helpdesk Consultant');
  const { success } = await updateHelpDeskConsultant({
    ticketCode, description, resource, token: accessToken, ticketId,
  });

  if (success) {
    const finishHelpDeskResponse = await finishTaskStep({
      accessToken,
      ticketId,
      idTicketManagement,
      ticketCode,
      stepName,
      taskResult: 'update',
    });

    if (!finishHelpDeskResponse.success) {
      return {
        success: false,
        message: 'technicalConsultantStep Ticket Step Submit failed!',
        data: finishHelpDeskResponse.data,
      };
    }
  }
  return {
    success: true,
    message: 'technicalConsultantStep Ticket Step Submit success!',
  };
};

// const handleTechnicalConsultantStep = async (dataObj) => {
//   const {
//     accessToken,
//     ticketCode,
//     stepName,
//   } = dataObj;

// const changeOwnertechnicalConsultant = await changeOwner(
//   accessToken,
//   ticketCode,
//   'SV01_Technical Consultant',
// );
//   const idConsultantStep = changeOwnertechnicalConsultant.idTicketManagement;

//   // const updateConsultantStep = await technicalConsultantStep(
//   //   accessToken, IdStep,
//   // );

//   const updateConsultantStep = await startTaskStep({
//     accessToken,
//     idTicketManagement: idConsultantStep,
//     stepName,
//   });

//   const checkUpdate = updateConsultantStep.data.error.find(
// (item) => item.name === 'fk_otk_ticket_task_note'
//   );

//   if (
// updateConsultantStep.data.error.length === 0 ||
//   (
//     checkUpdate &&
//     checkUpdate.errorMessage === 'This task has been started.'
//   )
//     ) {
//     const finishConsultantStep = await finishTaskStep({
//       accessToken,
//       ticketCode,
//       idTicketManagement: idConsultantStep,
//       stepName,
//     });
//     if (!finishConsultantStep.success) {
//       return {
//         success: false,
//         message: 'technicalConsultantStep Ticket Step Submit failed!',
//         data: finishConsultantStep.data,
//       };
//     }
//   }
//   return {
//     success: true,
//     message: 'finishConsultantStep Ticket Step Submit success!',
//   };
// };

// const handlePrepareAgentStep = async (dataObj) => {
//   const {
//     accessToken,
//     ticketCode,
//     stepName,
//   } = dataObj;

//   const changeOwnerPrepareAgent = await changeOwner(accessToken, ticketCode, stepName);
//   const idStepPrepareAgent = changeOwnerPrepareAgent.idTicketManagement;

//   // const startPerpareAgent = await startPrepareAgentStep({
//   //   accessToken,
//   //   idTicketManagement: idStepPrepareAgent,
//   // });

//   const startPerpareAgent = await startTaskStep({
//     accessToken,
//     idTicketManagement: idStepPrepareAgent,
//     stepName,
//   });

//   const checkUpdatePrepareAgent = startPerpareAgent.data.error.find(
// (item) => item.name === 'fk_otk_ticket_task_note'
// );

//   if (
//     startPerpareAgent.data.error.length === 0||
// (
//   checkUpdatePrepareAgent &&
//   checkUpdatePrepareAgent.errorMessage === 'This task has been started.'
// )
//   ) {
//     const finishPerpareAgentStep = await finishTaskStep({
//       accessToken,
//       ticketCode,
//       idTicketManagement: idStepPrepareAgent,
//       stepName,
//     });
//     if (!finishPerpareAgentStep.success) {
//       return {
//         success: false,
//         message: 'technicalConsultantStep Ticket Step Submit failed!',
//         data: finishPerpareAgentStep.data,
//       };
//     }
//   }
//   return {
//     success: true,
//     message: 'technicalConsultantStep Ticket Step Submit success!',
//   };
// };

// const handleBuildSourceStep = async (dataObj) => {
//   const {
//     accessToken,
//     ticketCode,
//     stepName,
//   } = dataObj;

//   const changeOwnerBuildSource = await changeOwner(accessToken, ticketCode, stepName);
//   const idStepBuildSource = changeOwnerBuildSource.idTicketManagement;

//   const startPerpareAgent = await startTaskStep({
//     accessToken,
//     idTicketManagement: idStepBuildSource,
//     stepName,
//   });

//   const checkUpdateBuildSource = startPerpareAgent.data.error.find(
// (item) => item.name === 'fk_otk_ticket_task_note'
//   );

//   if (
//     startPerpareAgent.data.error.length === 0 ||
//  (
//    checkUpdateBuildSource &&
//     checkUpdateBuildSource.errorMessage === 'This task has been started.'
//     )
//   ) {
//     const finishConsultantStep = await finishTaskStep({
//       accessToken,
//       ticketCode,
//       idTicketManagement: idStepBuildSource,
//       stepName,
//     });
//     if (!finishConsultantStep.success) {
//       return {
//         success: false,
//         message: `${stepName} Ticket Step Submit failed!`,
//         data: finishConsultantStep.data,
//       };
//     }
//   }
//   return {
//     success: true,
//     message: `${stepName} Ticket Step Submit success!`,
//   };
// };

// const handleConfigPipelineStep = async (dataObj) => {
//   const {
//     accessToken,
//     ticketCode,
//     stepName,
//   } = dataObj;

//   const changeOwnerConfigPipeline = await changeOwner(accessToken, ticketCode, stepName);
//   const idStepConfigPipeline = changeOwnerConfigPipeline.idTicketManagement;

//   const startConfigPipeline = await startTaskStep({
//     accessToken,
//     idTicketManagement: idStepConfigPipeline,
//     stepName,
//   });

//   const checkUpdateConfigPipeline = startConfigPipeline.data.error.find(
// (item) => item.name === 'fk_otk_ticket_task_note'
// );

//   if (
//     startConfigPipeline.data.error.length === 0 || (
// checkUpdateConfigPipeline &&
//   checkUpdateConfigPipeline.errorMessage === 'This task has been started.'
//    )
//   ) {
//     const finishConfigPipelineStep = await finishTaskStep({
//       accessToken,
//       ticketCode,
//       idTicketManagement: idStepConfigPipeline,
//       stepName,
//     });
//     if (!finishConfigPipelineStep.success) {
//       return {
//         success: false,
//         message: `${stepName} Ticket Step Submit failed!`,
//         data: finishConfigPipelineStep.data,
//       };
//     }
//   }

//   return {
//     success: true,
//     message: `${stepName} Ticket Step Submit success!`,
//   };
// };

// const handleHelpdeskSupportStep = async (dataObj) => {
//   const {
//     accessToken,
//     ticketCode,
//     stepName,
//   } = dataObj;

//   const changeOwnerHelpdeskSupport = await changeOwner(accessToken, ticketCode, stepName);
//   const idStepHelpdeskSupport = changeOwnerHelpdeskSupport.idTicketManagement;

//   const startHelpdeskSupport = await startTaskStep({
//     accessToken,
//     idTicketManagement: idStepHelpdeskSupport,
//     stepName,
//   });

//   const checkUpdateHelpdeskSupport = startHelpdeskSupport.data.error.find(
// (item) => item.name === 'fk_otk_ticket_task_note'
//   );

//   if (
//     startHelpdeskSupport.data.error.length === 0 || (
// checkUpdateHelpdeskSupport &&
//   checkUpdateHelpdeskSupport.errorMessage === 'This task has been started.'
// )
//   ) {
//     const finishHelpdeskSupport = await finishTaskStep({
//       accessToken,
//       ticketCode,
//       idTicketManagement: idStepHelpdeskSupport,
//       stepName,
//     });
//     if (!finishHelpdeskSupport.success) {
//       return {
//         success: false,
//         message: `${stepName} Ticket Step Submit failed!`,
//         data: finishHelpdeskSupport.data,
//       };
//     }
//   }
//   return {
//     success: true,
//     message: `${stepName} Ticket Step Submit success!`,
//   };
// };

// const handleConfirmCssStep = async (dataObj) => {
//   const {
//     accessToken,
//     ticketCode,
//     stepName,
//     ticketId,
//   } = dataObj;

//   const changeOwnerConfirmCss = await changeOwner(accessToken, ticketCode, stepName);
//   const idStepConfirmCss = changeOwnerConfirmCss.idTicketManagement;

//   const startConfirmCss = await updateConfirmCss({
//     accessToken,
//     idTicketManagement: idStepConfirmCss,
//     ticketId,
//     stepName,
//   });

// const checkUpdateConfirmCss = startConfirmCss.data.error.find(
//   (item) => item.name === 'fk_otk_ticket_task_note'
// );

//   if (
//     startConfirmCss.data.error.length === 0 || (
// checkUpdateConfirmCss &&
// checkUpdateConfirmCss.errorMessage === 'This task has been started.'
// )
//   ) {
//     const finishConfirmCss = await finishTaskStep({
//       accessToken,
//       ticketCode,
//       idTicketManagement: idStepConfirmCss,
//       stepName,
//     });
//     if (!finishConfirmCss.success) {
//       return {
//         success: false,
//         message: `${stepName} Ticket Step Submit failed!`,
//         data: finishConfirmCss.data,
//       };
//     }
//   }

//   return {
//     success: true,
//     message: `${stepName} Ticket Step Submit success!`,
//   };
// };

const autoCloseTickket = async (req, res) => {
  const { ticketCode, description, resource = '' } = req.body;

  const { data: token, status } = await axios.post(`${servers.ezAuthen}/slo/authen/login`, {
    username: ezServiceAccount[ezServer].username,
    password: ezServiceAccount[ezServer].password,
  });

  let accessToken = null;
  if (status === 401) {
    accessToken = tokenGlobal;
  } else {
    accessToken = token['access-token'];
    tokenGlobal = token['access-token'];
  }

  const { ticketId } = await getTicketInfo(accessToken, ticketCode);

  // helpdesk consultant
  const filterHelpdeskConsultantStep = await filterOwner(accessToken, ticketCode, 'Helpdesk Consultant');
  if (filterHelpdeskConsultantStep.statusTicketManagement === 'waiting') {
    const handleHelpdeskConsultantResponse = await handleHelpdeskConsultantStep({
      accessToken,
      ticketCode,
      description,
      resource,
      ticketId,
      stepName: 'Helpdesk Consultant',
    });

    if (!handleHelpdeskConsultantResponse.success) {
      res.json(handleHelpdeskConsultantResponse);
      return;
    }
  }

  // // Technical Consultant
  // const filterOwnerTechnicalConsultant = await filterOwner(
  // accessToken,
  //   ticketCode,
  //   'SV01_Technical Consultant'
  //   );
  // if (filterOwnerTechnicalConsultant.statusTicketManagement === 'waiting') {
  //   const handleTechnicalConsultantResponse = await handleTechnicalConsultantStep({
  //     accessToken,
  //     ticketCode,
  //     stepName: 'SV01_Technical Consultant',
  //   });

  //   if (!handleTechnicalConsultantResponse.success) {
  //     res.json(handleTechnicalConsultantResponse);
  //     return;
  //   }
  // }

  // // PREPARE AGENT
  // const filterOwnerPrepareAgent = await filterOwner(
  // accessToken,
  //   ticketCode,
  //   'SV01_Prepare agent'
  //   );
  // if (filterOwnerPrepareAgent.statusTicketManagement === 'waiting') {
  //   const handlePrepareAgentStepResponse = await handlePrepareAgentStep({
  //     accessToken,
  //     ticketCode,
  //     stepName: 'SV01_Prepare agent',
  //   });

  //   if (!handlePrepareAgentStepResponse.success) {
  //     res.json(handlePrepareAgentStepResponse);
  //     return;
  //   }
  // }

  // // BUILD SOURCE
  // const filterOwnerBuildSource = await filterOwner(accessToken, ticketCode, 'SV01_Build source');
  // if (filterOwnerBuildSource.statusTicketManagement === 'waiting') {
  //   const handleBuildSourceResponse = await handleBuildSourceStep({
  //     accessToken,
  //     ticketCode,
  //     stepName: 'SV01_Build source',
  //   });

  //   if (!handleBuildSourceResponse.success) {
  //     res.json(handleBuildSourceResponse);
  //     return;
  //   }
  // }

  // // Config Pipeline
  // const filterOwnerConfigPipeline = await filterOwner(
  // accessToken,
  //  ticketCode,
  //   'SV01_Config Pipeline'
  //   );
  // if (filterOwnerConfigPipeline.statusTicketManagement === 'waiting') {
  //   const handleConfigPipelineResponse = await handleConfigPipelineStep({
  //     accessToken,
  //     ticketCode,
  //     stepName: 'SV01_Config Pipeline',
  //   });

  //   if (!handleConfigPipelineResponse.success) {
  //     res.json(handleConfigPipelineResponse);
  //     return;
  //   }
  // }

  // // HELPDESK SUPPORT
  // const filterOwnerHelpdeskSupport = await filterOwner(
  // accessToken,
  // ticketCode,
  // 'SV01_Helpdesk support'
  // );
  // if (filterOwnerHelpdeskSupport.statusTicketManagement === 'waiting') {
  //   const handleHelpdeskSupportResponse = await handleHelpdeskSupportStep({
  //     accessToken,
  //     ticketCode,
  //     stepName: 'SV01_Helpdesk support',
  //   });

  //   if (!handleHelpdeskSupportResponse.success) {
  //     res.json(handleHelpdeskSupportResponse);
  //     return;
  //   }
  // }

  // // CONFIRM CSS
  // const filterOwnerConfirmCss = await filterOwner(accessToken, ticketCode, 'SV01_Confirm CSS');
  // if (filterOwnerConfirmCss.statusTicketManagement === 'waiting') {
  //   const handleConfirmCssStepResponse = await handleConfirmCssStep({
  //     accessToken,
  //     ticketCode,
  //     ticketId,
  //     stepName: 'SV01_Confirm CSS',
  //   });

  //   if (!handleConfirmCssStepResponse.success) {
  //     res.json(handleConfirmCssStepResponse);
  //     return;
  //   }

  //   res.json({
  //     success: true,
  //     message: 'Close ticket success!',
  //   });
  //   return;
  // }

  res.json({
    success: true,
    message: 'Cannot change ticket status! Please contact with administrator',
  });
};

const cancelTicket = async (req, res) => {
  const { ticketCode, tokenTicket } = req.body;
  const infoRequest = {
    objectTypeKey: 'otk_devops_get_cicd_ticket',
    fields: 'id',
    filter: `t.ticket_code = "${ticketCode}"`,
    locale: 'vn-VN',
    pageIndex: 1,
    pageSize: 1,
  };

  try {
    const { data: token } = await axios.post(`${servers.ezAuthen}/slo/authen/login`, {
      ...ezServiceAccount[ezServer],
    });
    tokenGlobal = token['access-token'];

    const response = await axios.post(`${ezServer}/services/screen/virtualObjectHtml`, infoRequest, {
      headers: { 'access-token': token['access-token'] },
    });

    const ticketId = response.data.id;

    const cancelObj = {
      objectTypeKey: 'otk_dsc_services_01',
      actionKey: 'ak_otk_dsc_services_01_cancel_ticket',
      whereFields: [{ key: 'id', value: ticketId }],
      updateFields: [
        { key: 'fk_reason_change', value: 'cancel' },
        { key: 'fk_otk_dsc_services_01_acceptance_note_rate', value: 'Create resource failed!' },
      ],
    };

    await axios.post(`${ezServer}/services/change/updateObject`, cancelObj, {
      headers: {
        'access-token': tokenTicket,
      },
    });

    res.json({ message: 'Update ticket success!' });
  } catch (error) {
    res.json({ message: 'Cancel ticket success!' });
  }
};

module.exports = {
  getListRequest,
  updateRequest,
  addCommentTicket,
  cancelTicket,
  reloadTable,
  autoCloseTickket,
};
