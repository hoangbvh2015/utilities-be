const Project = require('../models/project.model');
const User = require('../models/user.model');
const userRoles = require('../models/userRoles.model');
// -----------------

const addProjectUserAll = async (req, res) => {
  const { projectCode, userName } = req.body;
  // eslint-disable-next-line no-useless-catch
  try {
    const record = await Project.findOne({ projectCode });
    const user = await User.findOne({ name: userName.toLowerCase() });
    const roles = await userRoles.find();
    if (record.pmAccount.toLowerCase() === userName) {
      res.status(400).json({ message: 'The User is PM Account' });
      return;
    }
    // add user to project
    const userPermission = {
      userName: userName.toLowerCase(),
      permission: {
        jenkins: { view: true, edit: false },
        blackduck: { view: true, edit: false },
        coverity: { view: true, edit: false },
        sonar: { view: true, edit: false },
      },
      edit: false,
    };
    let projectPermission = [];
    if (Array.isArray(record.projectPermission)) {
      projectPermission = record.projectPermission;
    }
    const userProject = projectPermission.find((item) => item.userName === userName);

    if (!userProject) {
      if (user && ['admin', 'sub_admin'].includes(user.roleCode)) {
        userPermission.roleName = user.roleName;
        userPermission.roleCode = user.roleCode;
      } else {
        const role = roles.find((item) => item.roleCode === 'project_user');
        userPermission.roleName = role.roleName;
        userPermission.roleCode = role.roleCode;
      }
      projectPermission.push(userPermission);
    }

    await Project.updateOne({ projectCode }, { $set: { projectPermission } });

    // add user table
    if (user) {
      if (!['admin', 'sub_admin', 'project_admin'].includes(user.roleCode)) {
        const dataUpdate = {
          roleCode: 'project_user',
          roleName: 'project user',
        };
        await User.updateOne({ name: user.name }, { $set: { ...dataUpdate } });
      }
    } else {
      const userModel = new User({
        name: userPermission.userName && userPermission.userName.toLowerCase(),
        mail: `${userName}@fsoft.com.vn`,
        token: '',
        roleName: 'project user',
        roleCode: 'project_user',
      });
      await userModel.save();
    }

    const resProject = await Project.findOne({ projectCode });

    res.status(200).json({ resProject });
  } catch (error) {
    throw (error);
  }
};

const removeProjectUserAll = async (req, res) => {
  const { projectCode, userName } = req.body;
  try {
    const record = await Project.findOne({ projectCode });

    if (record.pmAccount.toLowerCase() === userName) {
      res.status(400).json({ message: 'The User is PM Account' });
      return;
    }

    // remove user in resource
    if (record) {
      const projectPermission = record.projectPermission.filter(
        (item) => item.userName !== userName,
      );
      await Project.updateOne({ projectCode }, { $set: { projectPermission } });

      res.status(200).send('done');
    }
  } catch (error) {
    throw (error);
  }
};

const changePermissionAll = async (req, res) => {
  const { projectCode, userPermission } = req.body;
  try {
    const record = await Project.findOne({ projectCode });

    if (record) {
      const userIndex = record.projectPermission.findIndex(
        (item) => item.userName === userPermission.userName,
      );

      if (userIndex !== -1) {
        record.projectPermission[userIndex].permission = userPermission.permission;
        record.projectPermission[userIndex].edit = userPermission.edit;
        await Project.updateOne(
          {
            projectCode,
          },
          {
            $set: {
              projectPermission: record.projectPermission,
            },
          },
        );
      }

      // for (let item in record.resourceData) {
      //   record.resourceData[item].forEach((ele, index) => {
      //     if (ele) {
      //       let userSelectIndex = ele['users'].findIndex(
      // item => item.userName === userPermission.userName
      // );
      //       if (userSelectIndex !== -1) {
      //         let user = record.resourceData[item][index]['users'][userSelectIndex];
      //         record.resourceData[item][index]['users'][userSelectIndex] = {
      //           ...user,
      //           ...userPermission.permission[item]
      //         };
      //       }
      //     }
      //   });

      // }
      // await Project.updateOne({ projectCode }, { $set: { resourceData: record.resourceData } });

      res.status(200).send('done');
    }
  } catch (error) {
    throw (error);
  }
};

const changePermissionEditUser = async (req, res) => {
  const { userName, role } = req;
  const { projectCode, userPermission } = req.body;
  try {
    const record = await Project.findOne({ projectCode });
    if ((role === 'sub_admin'
      || role === 'admin'
      || role === 'project_admin')
      || (record && record.pmAccount === userName)) {
      const userIndex = record.projectPermission.findIndex(
        (item) => item.userName === userPermission.userName,
      );

      record.projectPermission[userIndex].edit = userPermission.edit;
      await Project.updateOne(
        {
          projectCode,
        },
        {
          $set: {
            projectPermission: record.projectPermission,
          },
        },
      );
      res.status(200).send('done');
    } else {
      res.status(400).json({ message: 'Project User not edit permission' });
    }
  } catch (error) {
    throw (error);
  }
};

const changeRoleProject = async (req, res) => {
  const {
    projectCode, roleCode, roleName, userName,
  } = req.body;

  const project = await Project.findOne({ projectCode });

  if (project && (!['admin', 'user'].includes(roleCode))) {
    const projectPermission = project.projectPermission ? project.projectPermission : [];
    const userIndex = projectPermission.findIndex((item) => (
      item.userName.toLowerCase() === userName.toLowerCase()
    ));
    projectPermission[userIndex].roleName = roleName;
    projectPermission[userIndex].roleCode = roleCode;
    await Project.updateOne({ projectCode }, { $set: { projectPermission } });
    res.json('update success');
  } else {
    res.json('update failed');
  }
};

module.exports = {
  addProjectUserAll,
  changePermissionAll,
  removeProjectUserAll,
  changePermissionEditUser,
  changeRoleProject,
};
