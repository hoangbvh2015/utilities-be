/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
const fs = require('fs');
const path = require('path');
const xml2js = require('xml2js');
const axios = require('../services/api');
const { coverityAccount } = require('../enviroment.js');
const { saveLog, saveUserActionLog } = require('../services/saveLog');
const {
  getListCovProject,
  authenXML,
  createStreamXML,
  createUserXML,
  createProjectXML,
  createRoleXML,
  deleteProjectXML,
  addUserToRoles,
  handleUpdateUser,
  getUserXML,
  getStreamsXML,
  getProjectsXML,
  deleteStreamXML,
} = require('../helper/coverity.js');
const {
  rolesCov,
  user,
  stream,
  userNoAccess,
  otherRoles,
} = require('../template/coverity-template');
const { removeProjectCoverity } = require('../services/save-data');
const Project = require('../models/project.model');
const UnUseResource = require('../models/unUseResource.model');

const getProjects = async (req, res) => {
  const params = req.query;
  const result = await getListCovProject(params.host);

  if (typeof result === 'object' && (result.status < 200 || result.status >= 300)) {
    res.status(result.status);
    res.json({ errors: result.errors });
  } else {
    res.json(result);
  }
};

const createProject = async (req, res, next) => {
  const {
    resourceUrl,
    fsu,
    bu,
    projectCode,
    username,
    project,
  } = req.body;

  const filePath = path.join(__dirname, '../assets/coverity/createProject.xml');
  const fileDataString = fs.readFileSync(filePath, { encoding: 'utf-8' });

  const fileDataObject = await xml2js.parseStringPromise(fileDataString);
  authenXML(
    fileDataObject,
    coverityAccount[resourceUrl].username,
    coverityAccount[resourceUrl].password,
  );
  createProjectXML(fileDataObject, project.name, `${fsu}.${bu}.${projectCode}`);
  if (resourceUrl === 'https://coverity.fsoft.com.vn') {
    addUserToRoles(rolesCov, username);
    userNoAccess.groupId[0].displayName = ['Users']; // group name
    userNoAccess.groupId[1].name = ['Users']; // group name
    rolesCov.push(userNoAccess);
    rolesCov.forEach((element) => {
      createRoleXML(fileDataObject, element);
    });
  } else {
    addUserToRoles(otherRoles, username);
    otherRoles.forEach((element) => {
      createRoleXML(fileDataObject, element);
    });
  }
  const xml = new xml2js.Builder().buildObject(fileDataObject);
  req.coverityProjectData = [{ projectName: project.name }];

  try {
    await axios.coverity.post(`${resourceUrl}:443/ws/v9/configurationservice`, xml, { headers: { 'Content-Type': 'text/xml;charset=UTF-8' } });
    // req.coverityProjectData.push({ projectName: project.name, projectStreams: [] });
  } catch (error) {
    if (error && error.response) {
      res.status(error.response.status);
      res.json({ errors: error.response.data });
    }
  }
  next();
};

const createStream = async (req, res) => { // create stream in project
  const { resourceUrl, streamName, projectName } = req.body;
  stream.name = [streamName];
  const filePath = path.join(__dirname, '../assets/coverity/createStream.xml');
  const fileDataString = fs.readFileSync(filePath, { encoding: 'utf-8' });
  const fileDataObject = await xml2js.parseStringPromise(fileDataString);

  authenXML(
    fileDataObject,
    coverityAccount[resourceUrl].username,
    coverityAccount[resourceUrl].password,
  );
  createStreamXML(fileDataObject, projectName, stream);
  const xml = new xml2js.Builder().buildObject(fileDataObject);

  try {
    const response = await axios.coverity.post(
      `${resourceUrl}:443/ws/v9/configurationservice`,
      xml,
      {
        headers: {
          'Content-Type': 'text/xml;charset=UTF-8',
        },
      },
    );
    res.status(response.status);

    if (response.status >= 200 && response.status < 300) {
      res.json({ data: response.data });
    } else {
      const messageRegex = response.data && response.data.match(/<message>.*<\/message>/g);

      const message = messageRegex && messageRegex[0].replace('<message>', '').replace('</message>', '');
      res.json({ data: response.data, message });
    }
    // const project = req.coverityProjectData.find((pj) => pj.projectName === projectName);
    // project.projectStreams.push(StreamName);
  } catch (error) {
    throw error;
    // if(error && error.response) {
    //   console.log(error)
    //   res.status(error.response.status);
    //   res.json({ errors: error.response.data })
    // }
  }
};

// const createMultiProject = async (req, res, next) => {
//   const params = req.body;
//   req.coverityProjectData = [];
//   for (const project of params.note.project) {
//     await createProject(req, res, project);
//     req.coverityProjectData.push(project.name);
//   }
//   next();
// };

// const createMultiStream = async (req, res, next) => {
//   const {note: {project: projects}} = req.body;
//   for (const project of projects) {
//     for (const stream of project.stream) {
//       await createStream(req, res, stream, project.name);
//     }
//   }
//   res.json(projects)
// };

const createUser = async (req, res, next) => { // param.note.user
  const { resourceUrl, note } = req.body;
  user.username = note.user.username;
  user.password = note.user.password;
  user.givenName = note.user.lastname;
  user.familyName = note.user.firstname;
  user.email = note.user.email;

  const filePath = path.join(__dirname, '../assets/coverity/createUser.xml');
  fs.readFile(filePath, { encoding: 'utf-8' }, async (err, data) => {
    let xml;
    xml2js.parseString(data, async (_err, result) => {
      authenXML(
        result,
        coverityAccount[resourceUrl].username,
        coverityAccount[resourceUrl].password,
      );
      createUserXML(result, user);
      xml = new xml2js.Builder().buildObject(result);
    });
    try {
      const response = await axios.coverity.post(
        `${resourceUrl}:443/ws/v9/configurationservice`,
        xml,
        {
          headers: {
            'Content-Type': 'text/xml;charset=UTF-8',
          },
        },
      );
      req.covAccount = note.user;

      if ((typeof response.data === 'string') && (response.data.includes(`User with user name ${note.user.username} already exists.`))) {
        const body = {
          host: resourceUrl,
          username: note.user.username,
          userSpec: note.user,
        };
        await handleUpdateUser(body);
      }
      next();
    } catch (error) {
      if (error && error.response) {
        res.status(error.response.status);
        res.json({ errors: error.response.data });
      }
    }
  });
};

const updateUser = async (req, res) => {
  try {
    await handleUpdateUser(req.body);
    res.status(200).json({});
  } catch (error) {
    throw error;
  }
};

const summaryData = async (req, res) => {
  const {
    fsu,
    bu,
    projectCode,
    projectName,
    resourceUrl,
    createdBy,
    pmAccount,
  } = req.body;
  const { covAccount, coverityProjectData } = req;
  await saveLog(
    'coverity',
    {
      fsu,
      bu,
      projectCode,
      projectName,
      createdBy,
      pmAccount,
      resources:
      {
        resourceUrl,
        covAccount,
        coverityProjectData,
      },
    },
  );

  res.json({
    covAccount,
    coverityProjectData,
  });
};

const deleteProject = async (req, res, next) => {
  const {
    host, projectName, projectCode, jobURL,
  } = req.body;
  const filePath = path.join(__dirname, '../assets/coverity/deleteProject.xml');
  const fileDataString = fs.readFileSync(filePath, { encoding: 'utf-8' });
  const fileDataObject = await xml2js.parseStringPromise(fileDataString);
  authenXML(
    fileDataObject,
    coverityAccount[host].username,
    coverityAccount[host].password,
  );
  deleteProjectXML(fileDataObject, projectName, stream);
  const xml = new xml2js.Builder().buildObject(fileDataObject);

  try {
    const responseDelete = await axios.coverity.post(
      `${host}:443/ws/v9/configurationservice`,
      xml,
      {
        headers: {
          'Content-Type': 'text/xml;charset=UTF-8',
        },
      },
    );

    if (responseDelete.status >= 200 && responseDelete.status < 300) {
      const { project } = await removeProjectCoverity(projectCode, projectName, host, jobURL);
      res.locals = project;
      next();
    } else {
      const response = await removeProjectCoverity(projectCode, projectName, host, jobURL);
      if (response.message) {
        res.status(200).json({
          message: response.message, project: response.project, isSuccess: true,
        });
        return;
      }
      res.status(responseDelete.status).json({
        message: responseDelete.data, project: response.project,
      });
    }
  } catch (error) {
    if (error && error.response) {
      res.status(error.response.status).json({
        message: error.message, error: error.response.data,
      });
    }
  }
};

const saveCoverityDeleteProjectLog = async (req, res) => {
  const {
    host,
    projectName,
    projectCode,
    createdBy,
  } = req.body;
  await saveUserActionLog(
    'coverity_delete_project',
    { createdBy, resources: { projectName, projectCode, host } },
  );
  res.json({ isSuccess: true, project: res.locals });
};

const getListHostUnuseRs = async (req, res) => {
  // const { type } = req.params;
  const record = await UnUseResource.find({ resourceType: { $eq: 'coverity' } });

  if (record) {
    const listHost = record.reduce((acc, current) => {
      const urlSplit = current.jobURL.split('/');
      const host = [urlSplit[0], urlSplit[1], urlSplit[2]].join('/');
      const checkExist = acc.find((item) => item.value === host);
      if (!checkExist) {
        acc.push({
          label: current.hostName,
          value: host,
        });
      }
      return acc;
    }, []);
    res.json({ success: true, listHost });
  } else {
    res.status(400).json({ success: false, message: 'Connect DB failed!' });
  }
};

const getUnuseResource = async (req, res) => {
  const {
    query = '',
    page = 1,
    pageSize = 100,
    host = '',
  } = req.query;

  // const { type } = req.params;

  const record = await UnUseResource.find({ resourceType: { $eq: 'coverity' }, proejctAssigned: { $exists: true, $eq: [] } });
  if (record) {
    const start = (page - 1) * pageSize;
    const end = page * pageSize;
    const listItem = record.filter(
      (item) => (item.displayName.includes(query) && item.jobURL.includes(host)),
    );
    const total = listItem.length;
    const result = listItem.slice(start, end);
    res.json({ success: true, result, total });
  } else {
    res.status(400).json({ success: false, message: 'Connect DB failed!' });
  }
};

const addResourceToProject = async (req, res) => {
  const { projectCode, listResourceChecked } = req.body;
  const record = await Project.findOne({ projectCode });

  if (record) {
    const { resourceData } = record;

    for (const value of listResourceChecked) {
      const checkResource = resourceData.coverity.findIndex(
        (item) => item.jobURL === value.jobURL,
      );
      if (checkResource !== -1) {
        resourceData.coverity[checkResource] = value;
      } else {
        resourceData.coverity.push(value);
      }
      await UnUseResource.updateOne({ jobURL: value.jobURL }, {
        $set: {
          proejctAssigned: [projectCode],
        },
      });
    }

    await Project.updateOne({ projectCode }, {
      $set: {
        resourceData,
      },
    });
    res.status(200);
  } else {
    res.status(404);
  }
  res.json(listResourceChecked);
};

const getUser = async (req, res) => { // getUser in project
  const { host, userName } = req.query;
  const filePath = path.join(__dirname, '../assets/coverity/getUser.xml');
  const fileDataString = fs.readFileSync(filePath, { encoding: 'utf-8' });
  const fileDataObject = await xml2js.parseStringPromise(fileDataString);

  authenXML(
    fileDataObject,
    coverityAccount[host].username,
    coverityAccount[host].password,
  );
  getUserXML(fileDataObject, userName);
  const xml = new xml2js.Builder().buildObject(fileDataObject);

  try {
    const response = await axios.coverity.post(
      `${host}:443/ws/v9/configurationservice`,
      xml,
      {
        headers: {
          'Content-Type': 'text/xml;charset=UTF-8',
        },
      },
    );
    const data = await xml2js.parseStringPromise(response.data);
    const userObject = data['S:Envelope']['S:Body'][0]['ns2:getUserResponse'][0].return[0];
    res.status(response.status).json({ data: userObject });
  } catch (error) {
    throw error;
  }
};

const getStream = async (req, res) => { // getUser in project
  const { host, namePattern } = req.query;
  const filePath = path.join(__dirname, '../assets/coverity/getStreams.xml');
  const fileDataString = fs.readFileSync(filePath, { encoding: 'utf-8' });
  const fileDataObject = await xml2js.parseStringPromise(fileDataString);

  authenXML(
    fileDataObject,
    coverityAccount[host].username,
    coverityAccount[host].password,
  );
  getStreamsXML(fileDataObject, namePattern);
  const xml = new xml2js.Builder().buildObject(fileDataObject);

  try {
    const response = await axios.coverity.post(
      `${host}:443/ws/v9/configurationservice`,
      xml,
      {
        headers: {
          'Content-Type': 'text/xml;charset=UTF-8',
        },
      },
    );
    const data = await xml2js.parseStringPromise(response.data);
    const userObject = data['S:Envelope']['S:Body'][0]['ns2:getStreamsResponse'][0].return[0];
    res.status(response.status).json({ data: userObject });
  } catch (error) {
    throw error;
  }
};

const getProject = async (req, res) => {
  const { host, namePattern } = req.query;
  const filePath = path.join(__dirname, '../assets/coverity/getProjects.xml');
  const fileDataString = fs.readFileSync(filePath, { encoding: 'utf-8' });
  const fileDataObject = await xml2js.parseStringPromise(fileDataString);

  authenXML(
    fileDataObject,
    coverityAccount[host].username,
    coverityAccount[host].password,
  );
  getProjectsXML(fileDataObject, namePattern);
  const xml = new xml2js.Builder().buildObject(fileDataObject);

  try {
    const response = await axios.coverity.post(`${host}:443/ws/v9/configurationservice`, xml, { headers: { 'Content-Type': 'text/xml;charset=UTF-8' } });
    const data = await xml2js.parseStringPromise(response.data);

    const dataInfor = data['S:Envelope']
      && data['S:Envelope']['S:Body']
      && data['S:Envelope']['S:Body'][0]['ns2:getProjectsResponse']
      && data['S:Envelope']['S:Body'][0]['ns2:getProjectsResponse'][0].return;

    const projectObject = dataInfor ? dataInfor[0] : { streams: null, roleAssignments: null };

    res.status(response.status).json({ data: projectObject });
  } catch (error) {
    if (error && error.response) {
      res.status(error.response.status);
      res.json({ errors: error.response.data });
    } else {
      throw error;
    }
  }
};

const deleteStream = async (req, res) => {
  const { host, streamName } = req.body;
  const filePath = path.join(__dirname, '../assets/coverity/deleteStream.xml');
  const fileDataString = fs.readFileSync(filePath, { encoding: 'utf-8' });
  const fileDataObject = await xml2js.parseStringPromise(fileDataString);

  authenXML(
    fileDataObject,
    coverityAccount[host].username,
    coverityAccount[host].password,
  );
  deleteStreamXML(fileDataObject, streamName);
  const xml = new xml2js.Builder().buildObject(fileDataObject);

  try {
    const response = await axios.coverity.post(
      `${host}:443/ws/v9/configurationservice`,
      xml,
      {
        headers: {
          'Content-Type': 'text/xml;charset=UTF-8',
        },
      },
    );
    res.status(response.status).json({});
  } catch (error) {
    throw error;
  }
};

module.exports = {
  // createMultiProject,
  createProject,
  createUser,
  // createMultiStream,
  createStream,
  summaryData,
  getProjects,
  deleteProject,
  updateUser,
  saveCoverityDeleteProjectLog,
  getUnuseResource,
  addResourceToProject,
  getListHostUnuseRs,
  getUser,
  getStream,
  getProject,
  deleteStream,
};
