const { authenticate } = require('ldap-authentication');
const jwt = require('jsonwebtoken');

const ldapConfig = {
  url: 'ldap://10.16.34.14:389/',
  bindDn: 'CN=devopsldap,OU=_FSO HO,OU=SHARE_Account,OU=FSOFT CO,DC=fsoft,DC=fpt,DC=vn',
  bindPassword: 'LHAU7&100yLsXo599SJnz',
  usernameAttribute: 'cn',
  userSearchBase: 'OU=FSOFT CO,DC=fsoft,DC=fpt,DC=vn',
  groupsSearchBase: 'OU=MAIL_LIST,OU=FSOFT CO,DC=fsoft,DC=fpt,DC=vn',
  groupClass: 'group',
};

const ldapAuthenticate = async (username, password) => {
  const authenticatedUser = await authenticate({
    ldapOpts: {
      url: ldapConfig.url,
    },
    adminDn: ldapConfig.bindDn,
    adminPassword: ldapConfig.bindPassword,
    userSearchBase: ldapConfig.userSearchBase,
    usernameAttribute: ldapConfig.usernameAttribute,
    groupsSearchBase: ldapConfig.groupsSearchBase,
    groupClass: ldapConfig.groupClass,
    username,
    userPassword: password,
  });

  return authenticatedUser;
};

const generateToken = (info) => jwt.sign(info, process.env.JWT_KEY || 'secret');

const isValidToken = (token) => {
  try {
    return jwt.verify(token, process.env.JWT_KEY || 'secret');
  } catch (error) {
    return false;
  }
};

// const getLdapUsers = async (userName, password) => {
//   const config = {
//     url: 'ldap://10.16.34.14:389/',
//     baseDN: 'OU=FSOFT CO,DC=fsoft,DC=fpt,DC=vn',
//     username: `${userName}@fsoft.com.vn`,
//     password: password,
//   }
//   const query = 'mail=*@fsoft.com.vn*';

//   const ad = new ActiveDirectory(config);
//   await ad.find(query, async (err, results) => {
//     if ((err) || (!results)) {
//       console.log('ERROR: ' + JSON.stringify(err));
//       return;
//     }

//     for(const item of results.users) {
//       const accountName = item.mail.split('@fsoft')[0];
//       const record = await User.findOne({name: accountName, mail: item.mail});

//       if(!record) {
//         const userModel = new User({
//           name: accountName,
//           mail: item.mail,
//           token: '',
//           roleName: 'user'
//         });
//         await userModel.save();
//       }
//     }
//   });
// }

module.exports = {
  ldapAuthenticate,
  generateToken,
  isValidToken,
  // getLdapUsers
};
