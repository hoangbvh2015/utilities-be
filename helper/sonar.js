const btoa = require('btoa');
const axios = require('axios');
const { sonarKey } = require('../enviroment.js');
const Project = require('../models/project.model');

const getSonarProjectUsers = async (host, projectKey, page, pageSize) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const url = `${host}/api/permissions/users?projectKey=${projectKey}&p=${page}&ps=${pageSize}`;
  try {
    const sonarProjectData = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    return sonarProjectData.data;
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error;
  }
};

const getSonarProjectGroups = async (host, projectKey, page, pageSize) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const url = `${host}/api/permissions/groups?projectKey=${projectKey}&p=${page}&ps=${pageSize}`;
  try {
    const sonarProjectData = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    return sonarProjectData.data;
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error;
  }
};

const getListProjectHaveSonar = async () => {
  const listProject = await Project.find();
  const listProjectHaveSonar = [];
  listProject.forEach((item) => {
    if (item.resourceData.sonar.length > 0) {
      listProjectHaveSonar.push(item);
    }
  });
  return listProjectHaveSonar;
};

const getListSonarProject = async (host, page, pageSize, projects) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const p = page ? `&p=${page}` : '';
  const ps = pageSize ? `&ps=${pageSize}` : '';
  const projectKeys = projects ? `&projects=${projects}` : '';
  const url = `${host}/api/projects/search?${p}${ps}${projectKeys}`;
  const projectSonar = await getListProjectHaveSonar();
  try {
    const sonarProjectData = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    sonarProjectData.data.components.forEach((item) => {
      if (!item.lastAnalysisDate) {
        projectSonar.forEach((project) => {
          project.resourceData.sonar.forEach((record) => {
            if (record.displayName === item.name) {
              // eslint-disable-next-line no-param-reassign
              item.lastAnalysisDate = project.createdAt;
            }
          });
        });
      }
    });
    return sonarProjectData.data;
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error;
  }
};

const getListSonarUser = async (host, query) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const url = `${host}/api/users/search?q=${query}`;
  try {
    const response = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });

    return response.data.users.find((user) => user.login.toLowerCase() === query.toLowerCase());
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error;
  }
};

const getAllSonarProjects = async (host) => {
  const sonarData = [];
  let errors = {};
  let page = 1;
  const pageSize = 500;
  let total = 0;
  let result = await getListSonarProject(host, page, pageSize);

  if (result && (result.status < 200 || result.status >= 300)) {
    errors = result;
  }

  let sonarProjectList = result.components;
  total = result.paging.total;
  const num = Math.ceil(total / pageSize);
  while (page <= num) {
    for (let i = 0; i < sonarProjectList.length; i += 1) {
      const lastAnalysisDate = new Date(sonarProjectList[i].lastAnalysisDate).toLocaleString();
      const sonarProject = {
        server: host,
        projectData: {
          projectName: sonarProjectList[i].name,
          projectLink: `${host}/dashboard?id=${sonarProjectList[i].key}`,
          projectKey: sonarProjectList[i].key,
          projectCode: sonarProjectList[i].name.split('.')[2] === undefined ? sonarProjectList[i].name.split('.')[0] : sonarProjectList[i].name.split('.')[2],
          department: {
            department1: sonarProjectList[i].name.split('.')[0],
            department2: sonarProjectList[i].name.split('.')[1] === undefined ? sonarProjectList[i].name.split('.')[0] : sonarProjectList[i].name.split('.')[1],
          },
          lastAnalysisDate,
        },
      };
      sonarData.push(sonarProject);
    }
    page += 1;
    // eslint-disable-next-line no-await-in-loop
    result = await getListSonarProject(host, page, pageSize);
    if (result && (result.status < 200 || result.status >= 300)) {
      errors = result;
    }
    sonarProjectList = result.components;
  }
  return { sonarData, errors };
};

const collectSonarProject = async (host) => {
  // get all project from sonar host and return with data type defined
  const { sonarData } = await getAllSonarProjects(host);

  const projects = [];
  const data = sonarData;
  while (data.length > 0) {
    const project = {
      projectName: data[0].projectData.projectName,
      projectCode: data[0].projectData.projectCode,
      department: data[0].projectData.department,
      projectLink: '',
      projectRank: '',
      status: '',
      startDate: '',
      endDate: '',
      description: '',
      useDevOpsService: false,
      pmAccount: '',
      resourceCreatedBy: '',
      isResourceCreated: true,
      resourceData: {
        jenkins: [],
        sonar: [],
        coverity: [],
        blackduck: [],
      },
    };
    for (let i = 0; i < sonarData.length; i += 1) {
      if (project.projectCode === sonarData[i].projectData.projectCode) {
        project.resourceData.sonar.push(sonarData[i]);
        project.isResourceCreated = true;
        data.splice(i, 1);
      }
    }
    projects.push(project);
    data.splice(0, 1);
  }
  return projects;
};

const createSonarProject = async (host, projectName, projectKey) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  try {
    const urlCreateProject = `${host}/api/projects/create?name=${projectName}&project=${projectKey}&visibility=private`;
    await axios.post(urlCreateProject, null, { headers: { Authorization: AUTH_BASIC } });
    return {
      status: 'OK',
      message: 'Create Success',
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const deleteSonarProjects = async (host, projectKeys) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const url = `${host}/api/projects/bulk_delete?projects=${projectKeys.toString()}`;
  try {
    const { status } = await axios.post(url, null, { headers: { Authorization: AUTH_BASIC } });
    return { status };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const deleteSonarUserPermission = async (host, projectKey, projectUser, permission) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const urlAddPermission = `${host}/api/permissions/remove_user?login=${projectUser}&permission=${permission}&projectKey=${projectKey}`;
  try {
    await axios.post(urlAddPermission, {}, { headers: { Authorization: AUTH_BASIC } });
    return {
      status: 'OK',
      message: 'Remove user permissions success',
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const addSonarUserPermission = async (host, projectKey, projectUser, permission) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const urlAddPermission = `${host}/api/permissions/add_user?login=${projectUser}&permission=${permission}&projectKey=${projectKey}`;
  try {
    await axios.post(urlAddPermission, {}, { headers: { Authorization: AUTH_BASIC } });
    return {
      status: 'OK',
      message: 'Add user permissions success',
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const deleteSonarGroupPermission = async (host, projectKey, groupName, permission) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const urlAddPermission = `${host}/api/permissions/remove_group?groupName=${groupName}&permission=${permission}&projectKey=${projectKey}`;
  try {
    await axios.post(urlAddPermission, {}, { headers: { Authorization: AUTH_BASIC } });
    return {
      status: 'OK',
      message: 'Remove group permissions success',
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const addSonarUserMultiplePermission = async (host, projectKey, projectUser, isAdmin) => {
  const permissionList = ['codeviewer', 'issueadmin', 'securityhotspotadmin', 'scan', 'user'];
  const permissionAdded = [];
  let errors = {};
  if (isAdmin) {
    permissionList.push('admin');
  }
  // else if (projectUser.permissions.indexOf('admin') > -1) {
  //   let data = await deleteSonarUserPermission(host, projectKey, projectUser.userName, 'admin');
  //   if(data && (data.status < 200 || data.status >= 300)) {
  //     errors = { ...data };
  //   }
  // }

  // eslint-disable-next-line no-restricted-syntax
  for (const permission of permissionList) {
    // if (!projectUser.permissions.indexOf(permission) > -1) {
    // eslint-disable-next-line no-await-in-loop
    const data = await addSonarUserPermission(host, projectKey, projectUser, permission);
    if (data && (data.status < 200 || data.status >= 300)) {
      errors = { ...data };
    }
    permissionAdded.push(permission);
    // }
  }
  return { permissionAdded, errors };
};

const addSonarGroupPermission = async (host, projectKey, groupName, permission) => {
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  const urlAddPermission = `${host}/api/permissions/add_group?groupName=${groupName}&permission=${permission}&projectKey=${projectKey}`;
  try {
    await axios.post(urlAddPermission, {}, { headers: { Authorization: AUTH_BASIC } });
    return {
      status: 'OK',
      message: 'Add group permissions success',
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const addSonarGroupMultiplePermission = async (host, projectKey, projectGroup, isAdmin) => {
  const permissionList = ['codeviewer', 'issueadmin', 'securityhotspotadmin', 'scan', 'user'];
  const permissionAdded = [];
  let errors = {};
  if (isAdmin) {
    permissionList.push('admin');
  }
  // eslint-disable-next-line no-restricted-syntax
  for (const permission of permissionList) {
    if (!projectGroup.permissions.indexOf(permission) > -1) {
      // eslint-disable-next-line no-await-in-loop
      const data = await addSonarGroupPermission(
        host,
        projectKey,
        projectGroup.groupName,
        permission,
      );
      if (data && (data.status < 200 || data.status >= 300)) {
        errors = { ...data };
      }
      permissionAdded.push(permission);
    } else {
      // eslint-disable-next-line no-await-in-loop
      const data = await deleteSonarGroupPermission(
        host,
        projectKey,
        projectGroup.groupName,
        permission,
      );
      if (data && (data.status < 200 || data.status >= 300)) {
        errors = { ...data };
      }
    }
  }
  return { permissionAdded, errors };
};

const deleteSonarUserMultiplePermission = async (host, projectKey, projectUser, removeAdmin) => {
  const permissionRemoved = [];
  let errors = {};
  if (removeAdmin) {
    const data = await deleteSonarUserPermission(host, projectKey, projectUser.userName, 'admin');
    if (data && (data.status < 200 || data.status >= 300)) {
      errors = { ...data };
    }
    permissionRemoved.push('admin');
  }
  // eslint-disable-next-line no-restricted-syntax
  for (const permission of projectUser.permissions) {
    // eslint-disable-next-line no-await-in-loop
    const data = await deleteSonarUserPermission(
      host,
      projectKey,
      projectUser.userName,
      permission,
    );
    if (data && (data.status < 200 || data.status >= 300)) {
      errors = { ...data };
    }
    permissionRemoved.push(permission);
  }
  return { permissionRemoved, errors };
};

const deleteSonarGroupMultiplePermission = async (host, projectKey, projectGroup, isAdmin) => {
  const permissionList = ['codeviewer', 'issueadmin', 'securityhotspotadmin', 'scan', 'user'];
  const permissionAdded = [];
  let errors = {};
  if (isAdmin) {
    permissionList.push('admin');
  }
  // eslint-disable-next-line no-restricted-syntax
  for (const permission of permissionList) {
    // eslint-disable-next-line no-await-in-loop
    const data = await deleteSonarGroupPermission(
      host,
      projectKey,
      projectGroup.groupName,
      permission,
    );
    permissionAdded.push(permission);
    if (data && (data.status < 200 || data.status >= 300)) {
      errors = { ...data };
    }
  }

  return { permissionAdded, errors };
};

const createSonarUser = async (body) => {
  const {
    host,
    email = '',
    local = true,
    login = '',
    name = '',
    password = '',
    scmAccount = '',
  } = body;
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  try {
    const urlCreateUser = `${host}/api/users/create?email=${email}&local=${local}&login=${login}&name=${name}&password=${password}&scmAccount=${scmAccount}`;
    await axios.post(urlCreateUser, null, { headers: { Authorization: AUTH_BASIC } });
    return {
      status: 200,
      message: 'Success',
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
      };
    }
    throw error.message;
  }
};

const updateSonarUser = async (body) => {
  const {
    host,
    email = '',
    login = '',
    name = '',
    scmAccount = '',
  } = body;
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  try {
    const urlUpdateUser = `${host}/api/users/update?email=${email}&login=${login}&name=${name}&scmAccount=${scmAccount}`;
    await axios.post(urlUpdateUser, null, { headers: { Authorization: AUTH_BASIC } });
    return {
      status: 200,
      message: 'Success',
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
      };
    }
    throw error.message;
  }
};

const deactivateUser = async (body) => {
  const { host, login = '' } = body;
  const AUTH_BASIC = `Basic ${btoa(`${sonarKey[host].username}:${sonarKey[host].password}`)}`;
  try {
    const deactivUser = `${host}/api/users/deactivate?&login=${login}`;
    await axios.post(deactivUser, null, { headers: { Authorization: AUTH_BASIC } });
    return {
      status: 200,
      message: 'Success',
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
      };
    }
    throw error.message;
  }
};

module.exports = {
  getListSonarProject,
  collectSonarProject,
  getAllSonarProjects,
  deleteSonarProjects,
  createSonarProject,
  addSonarUserPermission,
  addSonarUserMultiplePermission,
  addSonarGroupPermission,
  addSonarGroupMultiplePermission,
  deleteSonarUserPermission,
  deleteSonarUserMultiplePermission,
  deleteSonarGroupPermission,
  deleteSonarGroupMultiplePermission,
  getSonarProjectUsers,
  getSonarProjectGroups,
  getListSonarUser,
  createSonarUser,
  updateSonarUser,
  deactivateUser,
};
