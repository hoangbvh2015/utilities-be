/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
const {
  forEach,
  tail,
} = require('lodash');
const axios = require('axios');
const btoa = require('btoa');
const cheerio = require('cheerio');
const xml2js = require('xml2js');
const { jenkinsKey } = require('../enviroment.js');

const getListFolderInRoot = async (host) => {
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;
  try {
    const { data } = await axios.get(host, { headers: { Authorization: AUTH_BASIC } });
    const $ = cheerio.load(data);

    return $('#projectstatus tr').filter((index, value) => value.attribs.id).map((index, value) => {
      const jobName = value.attribs.id.replace('job_', '');
      const type = value.children[0].children[0].attribs.tooltip;
      return {
        jobName: value.attribs.id.replace('job_', ''),
        jobLink: `${host}/job/${jobName}`,
        jobType: type.toLowerCase().replace(' ', '-'),
        projectCode: jobName.split('.')[2] === undefined ? jobName.split('.')[0] : jobName.split('.')[2],
        department: {
          department2: jobName.split('.')[1] === undefined ? jobName.split('.')[0] : jobName.split('.')[1],
          department1: jobName.split('.')[0],
        },
      };
    }).toArray();
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const getJobInFolder = async (host, folderName) => {
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;
  const url = `${host}/job/${folderName}/`;
  try {
    const { data } = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    const $ = cheerio.load(data);

    return $('#projectstatus .model-link')
      .filter((index, value) => ($(value).text().indexOf('#') < 0))
      .map((index, value) => ({
        jobName: $(value).text(),
        jobLink: url + $(value).text(),
      }))
      .toArray();
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const getFolderUsersPermission = async (host, folderName) => {
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;
  const url = `${host}/job/${folderName}/ownership/manage-owners`;
  try {
    const { data } = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    const $ = cheerio.load(data);
    const pag = [];
    const primaryOwner = $('.setting-main option[selected="true"]').attr('value');
    const secondaryOwner = [];
    forEach($('.setting-main option'), (value, key) => {
      pag[key] = { key: value.attribs.value, value: $(value).text() };
    });

    forEach($('.setting-main input[name="_.coOwner"]'), (value) => {
      secondaryOwner.push($(value).attr('value'));
    });

    return {
      listUser: pag,
      primaryOwner,
      secondaryOwner: tail(secondaryOwner),
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const getFolderInfo = async (host, folder) => {
  // const jenkinsJobs = await getJobInFolder(
  //   host,
  //   folder.jobName,
  // );
  const jenkinsJobs = '';
  const jenkinsFolderPermission = await getFolderUsersPermission(
    host,
    folder.jobName,
  );

  const jenkinsFolder = {
    server: host,
    projectData: {
      department: folder.department,
      projectCode: folder.projectCode,
      folderName: folder.jobName,
      folderLink: folder.jobLink,
      jobType: folder.jobType,
      jenkinsJobs,
      primaryOwner: jenkinsFolderPermission.primaryOwner,
      secondaryOwners: jenkinsFolderPermission.secondaryOwner,
    },
  };

  return jenkinsFolder;
};

const getListAgent = async (host) => {
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;
  const url = `${host}/computer/`;
  try {
    const { data } = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    const $ = cheerio.load(data);
    const pag = [];
    forEach($('#computers tr[id*=node_]'), (value, key) => {
      const agent = {
        agentName: $(value.children[1]).text(),
        agentLink: `${host}${$(value).find('a').attr('href')}`,
        status: $(value.children[0]).attr('data') === 'computer.png',
      };
      pag[key] = agent;
    });

    return pag;
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const getAgentUsersPermission = async (host, agentName) => {
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;
  let url = `${host}/computer/${agentName}/ownership/manage-owners`;

  if (agentName === 'master') {
    url = `${host}/computer/(${agentName})/ownership/manage-owners`;
  }

  try {
    const { data } = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    const $ = cheerio.load(data);
    const name = agentName;
    const primaryOwner = $('.setting-main option[selected="true"]').attr('value');
    const secondaryOwners = [];

    forEach($('.setting-main input[name="_.coOwner"]'), (value) => {
      secondaryOwners.push($(value).attr('value'));
    });

    return {
      name,
      primaryOwner,
      secondaryOwners: tail(secondaryOwners),
    };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const getAgentInfo = async (host, agentName) => {
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;
  let url = `${host}/computer/${agentName}/configure`;

  if (agentName === 'master') {
    url = `${host}/computer/(${agentName})/configure`;
  }

  try {
    const { data } = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    const $ = cheerio.load(data);

    const name = agentName;
    const description = $('.setting-main input[name="_.nodeDescription"]').attr('value');
    const executors = $('.setting-main input[name="_.numExecutors"]').attr('value');
    const rootRemoteDir = $('.setting-main input[name="_.remoteFS"]').attr('value');
    const label = $('.setting-main input[name="_.labelString"]').attr('value');

    const mode = $('select[name="mode"] option[selected="true"]').attr('value');

    const agentInfo = {
      name,
      description,
      executors,
      rootRemoteDir,
      label,
      mode,
    };
    return { agentInfo };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    // throw error.message;
  }
  return {};
};

const getAgentEdit = async (host, agentName) => {
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;

  try {
    const historyConfigNodeResponse = await axios.get(`${host}/computer/${agentName}/jobConfigHistory/`, { headers: { Authorization: AUTH_BASIC } });
    const $history = cheerio.load(historyConfigNodeResponse.data);
    const historyRow1 = $history('table[id="confighistory"] tr[id="table-row-1"]').children().first();
    const urlGetNodeXml = `${host}/computer/${agentName}/jobConfigHistory/configOutput?type=xml&timestamp=${historyRow1.text()}`;
    const dataXml = await axios.get(urlGetNodeXml, { headers: { Authorization: AUTH_BASIC } });
    const nodeXmlparse = (await xml2js.parseStringPromise(dataXml.data)).slave;
    // get laucher
    // const tunnel = $('.setting-main input[name="_.tunnel"]').attr('value');
    // const vmargs = $('.setting-main input[name="_.vmargs"]').attr('value');

    // get WorkDir
    // eslint-disable-next-line max-len
    // const disableWorkDirCheckbox = $('table[name="slave.launcher"] input[name="_.disabled"]').attr('checked');

    // get tool location
    // eslint-disable-next-line max-len
    // const toolLocationNodePropertyCheckbox = $('.optional-block-start input[name="hudson-tools-ToolLocationNodeProperty"]').attr('checked');
    // const listToolLocation = [];
    // $('.repeated-container div:not(.to-be-removed)[name="locations"]').each((index, element) => {
    // eslint-disable-next-line max-len
    //   const toolKey = $(element).find('select[name="locations.key"] option[selected="true"]').attr('value');
    //   const toolHome = $(element).find("input[name='locations.home']").attr('value');
    //   listToolLocation.push({ key: toolKey, home: toolHome });
    // });

    const agentInfo = {
      name: agentName,
      description: nodeXmlparse.description && nodeXmlparse.description[0],
      numExecutors: nodeXmlparse.numExecutors && nodeXmlparse.numExecutors[0],
      remoteFS: nodeXmlparse.remoteFS && nodeXmlparse.remoteFS[0],
      label: nodeXmlparse.label && nodeXmlparse.label[0],
      mode: nodeXmlparse.mode && nodeXmlparse.mode[0],
      nodeXmlparse,
      nodeProperties: {
        'stapler-class-bag': true,
      },
    };

    if (nodeXmlparse.launcher) {
      const launcherStaplerClass = nodeXmlparse.launcher[0].$.class;
      const launcherClass = nodeXmlparse.launcher[0].$.class;
      agentInfo.launcher = {
        'stapler-class': launcherStaplerClass,
        $class: launcherClass,
      };
      if (launcherStaplerClass === 'hudson.slaves.JNLPLauncher') {
        const workSetting = nodeXmlparse.launcher[0].workDirSettings[0];
        agentInfo.launcher.workDirSettings = {
          disabled: workSetting.disabled[0],
          workDirPath: workSetting.workDirPath && workSetting.workDirPath[0],
          internalDir: workSetting.internalDir && workSetting.internalDir[0],
          failIfWorkDirIsMissing: workSetting.failIfWorkDirIsMissing[0],
        };

        const [tunnel] = nodeXmlparse.launcher[0].tunnel || [];
        const [vmargs] = nodeXmlparse.launcher[0].vmargs || [];
        const [webSocket] = nodeXmlparse.launcher[0].webSocket || [];
        agentInfo.launcher.webSocket = webSocket;
        agentInfo.launcher.tunnel = tunnel;
        agentInfo.launcher.vmargs = vmargs;
      }
    }

    // get Availability
    if (nodeXmlparse.retentionStrategy) {
      const retentionStrategyStaplerClass = nodeXmlparse.retentionStrategy[0].$.class;
      const retentionClass = nodeXmlparse.retentionStrategy[0].$.class;

      agentInfo.retentionStrategy = {
        'stapler-class': retentionStrategyStaplerClass,
        $class: retentionClass,
      };
    }

    return { agentInfo };
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const formatTreeValue = (list, url) => {
  const dataFormat = {};

  list.forEach((item) => {
    const folder = item.shift();
    if (dataFormat[folder] && Array.isArray(dataFormat[folder])) {
      dataFormat[folder] = item.length > 0 ? [...dataFormat[folder], item] : dataFormat[folder];
    } else {
      dataFormat[folder] = item.length > 0 ? [item] : [];
    }
  });

  const result = [];
  for (const [key, value] of Object.entries(dataFormat)) {
    const urlJob = key.split('+%+').length > 1 ? `${url}/job/${key.split('+%+')[1]}` : `${url}/job/${key}`;
    if (value.length > 0) {
      result.push({
        title: key.split('+%+')[0],
        key: urlJob,
        isleaf: false,
        children: formatTreeValue(value, urlJob),
        icon: 'folder',
      });
    } else {
      result.push({
        title: key.split('+%+')[0],
        key: urlJob,
        isleaf: false,
        children: [],
        icon: 'folder',
      });
    }
  }

  return result;
};

const recursiveTreeItem = async (url) => {
  const domain = url.split('/job/')[0];
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[domain].username}:${jenkinsKey[domain].password}`)}`;
  let data = {};
  let formatTree = [];

  try { // get other folder
    const resListMove = await axios.get(`${url}/move`, { headers: { Authorization: AUTH_BASIC } });
    data = resListMove.data;
  } catch (error) {
    const message = `${url}: ${error.message}`;
    throw message;
  }

  let $ = cheerio.load(data);

  const listFolderMove = $('#main-panel').find('> form  > select > option:nth-child(2)').not('.header');
  const otherFolder = $(listFolderMove).attr().value;

  try { // get current folder
    const response = await axios.get(`${domain}/job${otherFolder}/move/`, { headers: { Authorization: AUTH_BASIC } });
    data = response.data;
  } catch (error) {
    const message = `${url}: ${error.message}`;
    throw message;
  }
  $ = cheerio.load(data);

  const listFolder = $('#main-panel').find('> form  > select > option').not('.header').filter((index, value) => {
    const listFolderName = $(value).text().toLocaleLowerCase().includes(`${url.split('/job/')[1].toLocaleLowerCase()} » `);
    return listFolderName;
  })
    .toArray();

  if (listFolder.length > 0) {
    const formatListFolder = listFolder.map((item) => {
      const folders = $(item).text().split(' » ').slice(2);
      const attr = $(item).attr().value;
      if (folders[folders.length - 1] !== attr.split('/').pop()) {
        folders[folders.length - 1] += `+%+${attr.split('/').pop()}`;
      }
      return folders;
    });

    formatTree = formatTreeValue(formatListFolder, url);
  }

  return formatTree;
};

const getJobsInfolder = async (url) => {
  const domain = url.split('/job/')[0];
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[domain].username}:${jenkinsKey[domain].password}`)}`;
  let data = {};

  try {
    const response = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    // console.log('xxxxx =====', response);
    data = response.data;
  } catch (error) {
    return [];
  }
  const $ = cheerio.load(data);

  const children = $('#projectstatus').find('> tbody > tr').not('.header').filter((index, value) => {
    const isLeaf = $(value).find('> td:nth-child(1) > img').attr().tooltip !== 'Folder';
    return isLeaf;
  })
    .toArray();

  const listJob = children.map((item) => {
    const jobElement = $(item).find('> td:nth-child(3) > a');
    const key = `${url}/${jobElement.attr().href.slice(0, -1)}`;
    const status = $(item).find('> td:nth-child(1) > img').attr().tooltip;
    const title = jobElement.text();
    const isLeaf = true;
    const icon = 'project';

    return {
      title,
      key,
      status,
      isLeaf,
      icon,
    };
  });

  return listJob;
};

const getAllTreeItem = async (host, folderName) => {
  try {
    const tree = await recursiveTreeItem(`${host}/job/${folderName}`);

    return tree;
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const getFolderOwner = async (host, folderName) => {
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[host].username}:${jenkinsKey[host].password}`)}`;
  const url = `${host}/job/${folderName}/ownership/manage-owners`;
  try {
    const { data } = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });
    const $ = cheerio.load(data);
    const primaryOwner = $('.setting-main option[selected="true"]').attr('value');
    const secondaryOwner = [];
    forEach($('.setting-main input[name="_.coOwner"]'), (value) => {
      secondaryOwner.push($(value).attr('value'));
    });

    return {
      primaryOwner,
      secondaryOwner: tail(secondaryOwner),
    };
  } catch (error) {
    if (error && error.response) {
      return {
        primaryOwner: '',
        secondaryOwner: [],
      };
    }
    throw error.message;
  }
};

const getTree = async (url) => {
  const domain = url.split('/job/')[0];
  const AUTH_BASIC = `Basic ${btoa(`${jenkinsKey[domain].username}:${jenkinsKey[domain].password}`)}`;

  try {
    const { data } = await axios.get(url, { headers: { Authorization: AUTH_BASIC } });

    const $ = cheerio.load(data);

    const children = $('#projectstatus').find('> tbody > tr').not('.header').map((index, value) => {
      const jobElement = $(value).find('> td:nth-child(3) > a');
      const status = $(value).find('> td:nth-child(1) > img').attr().tooltip;
      const key = `${url}${jobElement.attr() ? jobElement.attr().href.slice(0, -1) : ''}`;
      const title = jobElement.text();
      const isLeaf = status !== 'Folder';
      const icon = isLeaf ? 'project' : 'folder';
      return {
        title,
        key,
        isLeaf,
        status,
        icon,
      };
    })
      .toArray();

    if (children.length > 0) {
      for (const child of children) {
        if (!child.isLeaf) {
          child.children = await getTree(child.key);
        }
      }
    }

    return children;
  } catch (error) {
    if (error && error.response) {
      return [];
    }
    throw error.message;
  }
};

const getAllFolderJenKinsInfo = async (url) => {
  const host = url.split('/job/')[0];
  const folderName = url.split('/job/').pop().replace('/', '');

  if (!jenkinsKey[host]) {
    return {
      listJob: [],
      primaryOwner: '',
      secondaryOwner: [],
    };
  }

  const listJob = await getTree(url);
  const { primaryOwner, secondaryOwner } = await getFolderOwner(host, folderName);

  return {
    listJob,
    primaryOwner,
    secondaryOwner,
  };
};

module.exports = {
  getListFolderInRoot,
  getJobInFolder,
  getFolderUsersPermission,
  getFolderInfo,
  getListAgent,
  getAgentInfo,
  getAgentUsersPermission,
  getAllTreeItem,
  getJobsInfolder,
  getAllFolderJenKinsInfo,
  getFolderOwner,
  getAgentEdit,
};
