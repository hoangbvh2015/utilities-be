/* eslint-disable no-await-in-loop */
require('express-async-errors');
const axios = require('axios');
const https = require('https');
const { akaworkApi } = require('../enviroment');

// At request level
const agent = new https.Agent({
  rejectUnauthorized: false,
});

const getAkaAuthenToken = async (host) => {
  const { data: { token } } = await axios.post(
    `${host}/api/v1/login`,
    {
      username: akaworkApi[host].username,
      password: akaworkApi[host].password,
    },
    { httpsAgent: agent },
  );

  return token;
};

const getAkaDepartment = async (host, token) => {
  const { data } = await axios.get(
    `${host}/dashboard-service/api/v1/departments`,
    {
      headers: { Authorization: `Bearer ${token}` },
    },
    { httpsAgent: agent },
  );
  return data;
};

const getAkaProjects = async (host, token, query) => {
  try {
    const { data } = await axios.post(
      `${host}/dashboard-service/api/v1/projects/search?${query}`,
      {},
      { headers: { Authorization: `Bearer ${token}` } },
      { httpsAgent: agent },
    );
    return data;
  } catch (error) {
    throw error.message;
  }
};

// const getAllProjectOnAkawork = async (host) => {
//   const projectData = [];
//   const akaworkHost = 'https://devops.fsoft.com.vn';
//   let page = 0;
//   const pageSize = 500;
//   const token = await getAkaAuthenToken(host);
//   let result = await getAkaProjects(host, token, `page=${page}&size=${pageSize}`);
//   const totalPage = result.totalPages;
//   let projectList = result.content;

//   while (page < totalPage) {
//     for (let i = 0; i < projectList.length; i += 1) {
//       const startDate = new Date(projectList[i].startDate).toLocaleDateString();
//       const endDate = new Date(projectList[i].endDate).toLocaleDateString();
//       const project = {
//         projectName: projectList[i].name,
//         projectLink: `${akaworkHost}/#/projects/${projectList[i].id}/about`,
//         projectCode: projectList[i].fiProjectKey,
//         projectRank: projectList[i].projectRank,
//         department: {
//           department1: projectList[i].departmentL1,
//           department2: projectList[i].departmentL2DisplayName,
//         },
//         status: projectList[i].status,
//         startDate,
//         endDate,
//         description: projectList[i].description,
//         useDevOpsService: projectList[i].useDevOpsService,
//       };
//       projectData.push(project);
//     }
//     page += 1;
//     result = await getAkaProjects(host, token, `page=${page}&size=${pageSize}`);
//     projectList = result.content;
//   }
//   return projectData;
// };

const getAkaProjectsWithMapComponent = async (host, token, query) => {
  try {
    const { data } = await axios.get(
      `${host}/dashboard-service/api/v1/projects/components?${query}`,
      { headers: { Authorization: `Bearer ${token}` } },
      { httpsAgent: agent },
    );
    return data;
  } catch (error) {
    throw error.message;
  }
};

const getExternalProjects = async (host, token, query) => {
  try {
    const { data } = await axios.get(
      `${host}/dashboard-service/api/v1/external/projects?${query}`,
      { headers: { Authorization: `Bearer ${token}` } },
      { httpsAgent: agent },
    );
    return data;
  } catch (error) {
    throw error.message;
  }
};

const getAllProjectsJira = async (host, token) => {
  try {
    const { data } = await axios.get(
      `${host}/dashboard-service/api/v1/external/components/jira`,
      { headers: { Authorization: `Bearer ${token}` } },
      { httpsAgent: agent },
    );
    return data;
  } catch (error) {
    throw error.message;
  }
};

const getExternalComponents = async (host, token, query) => {
  try {
    const { data } = await axios.get(
      `${host}/dashboard-service/api/v1/external/components?${query}`,
      { headers: { Authorization: `Bearer ${token}` } },
      { httpsAgent: agent },
    );
    return data;
  } catch (error) {
    throw error.message;
  }
};

module.exports = {
  getAkaAuthenToken,
  getAkaDepartment,
  getAkaProjects,
  // getAllProjectOnAkawork,
  getAkaProjectsWithMapComponent,
  getExternalProjects,
  getAllProjectsJira,
  getExternalComponents,
};
