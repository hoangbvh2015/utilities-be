const axios = require('../services/api');
const { blackduckKey } = require('../enviroment.js');

const getListBlackduckProject = async (host, limit) => {
  try {
    const { data: { bearerToken } } = await axios.blackduck.post(`${host}/api/tokens/authenticate`, {}, {
      headers: {
        Authorization: `token ${blackduckKey[host]}`,
      },
    });
    const { data, status } = await axios.blackduck.get(`${host}/api/projects?limit=${limit}&offset=0&sort=name`,
      {
        headers: { Authorization: `Bearer ${bearerToken}` },
      });
    return { data, status };
  } catch (error) {
    throw error;
  }
};

const getListBlackduckUser = async (host, limit = 10) => {
  try {
    const { data: { bearerToken } } = await axios.blackduck.post(
      `${host}/api/tokens/authenticate`,
      {},
      {
        headers: {
          Authorization: `token ${blackduckKey[host]}`,
        },
      },
    );
    const { data, status } = await axios.blackduck.get(
      `${host}/api/users?limit=${limit}&offset=0&sort=userName`,
      {
        headers: { Authorization: `Bearer ${bearerToken}` },
      },
    );

    return { data, status };
  } catch (error) {
    throw error.message;
  }
};

const getListUserBlackduckProject = async (host, usersUrl) => {
  try {
    const { data: { bearerToken } } = await axios.blackduck.post(`${host}/api/tokens/authenticate`, {}, {
      headers: {
        Authorization: `token ${blackduckKey[host]}`,
      },
    });
    const { data, status } = await axios.blackduck.get(
      usersUrl,
      {
        headers: { Authorization: `Bearer ${bearerToken}` },
      },
    );
    return { data, status };
  } catch (error) {
    throw error.message;
  }
};

const getAllBlackduckProject = async (host) => {
  const blackduckData = [];
  const limit = 100;
  const blackduckProjectList = await getListBlackduckProject(host, limit);
  for (let i = 0; i < blackduckProjectList.data.items.length; i += 1) {
    const accounts = [];
    let usersUrl = '';
    // eslint-disable-next-line no-underscore-dangle
    const { links } = blackduckProjectList.data.items[i]._meta;
    for (let j = 0; j < links.length; j += 1) {
      if (links[j].rel === 'users') {
        usersUrl = links[j].href;
        // eslint-disable-next-line no-await-in-loop
        const userData = await getListUserBlackduckProject(host, usersUrl);
        for (let u = 0; u < userData.data.items.length; u += 1) {
          accounts.push(userData.data.items[u].name);
        }
      }
    }
    blackduckProjectList.items[i].updatedAt = new Date().toLocaleString();
    const blackduckProject = {
      server: host,
      projectData: {
        projectName: blackduckProjectList.items[i].name,
        // eslint-disable-next-line no-underscore-dangle
        projectLink: blackduckProjectList.items[i]._meta.href,
        projectCode: blackduckProjectList.items[i].name.split('.')[2] === undefined ? blackduckProjectList.items[i].name.split('.')[0] : blackduckProjectList.items[i].name.split('.')[2],
        department: {
          department1: blackduckProjectList.items[i].name.split('.')[0],
          department2: blackduckProjectList.items[i].name.split('.')[1] === undefined ? blackduckProjectList.items[i].name.split('.')[0] : blackduckProjectList.items[i].name.split('.')[1],
        },
        lastUpdate: blackduckProjectList.items[i].updatedAt,
        accounts,
      },
    };
    blackduckData.push(blackduckProject);
  }

  return blackduckData;
};

const collectBlackduckProject = async (host) => {
  const projects = [];
  const blackduckData = await getAllBlackduckProject(host);
  const data = blackduckData;
  while (data.length > 0) {
    const project = {
      projectName: data[0].projectData.projectName,
      projectCode: data[0].projectData.projectCode,
      department: data[0].projectData.department,
      projectLink: '',
      projectRank: '',
      status: '',
      startDate: '',
      endDate: '',
      description: '',
      useDevOpsService: false,
      pmAccount: '',
      resourceCreatedBy: '',
      isResourceCreated: false,
      resourceData: {
        jenkins: [],
        sonar: [],
        coverity: [],
        blackduck: [],
      },
    };
    for (let i = 0; i < blackduckData.length; i += 1) {
      if (project.projectCode === blackduckData[i].projectData.projectCode) {
        project.resourceData.blackduck.push(blackduckData[i]);
        project.isResourceCreated = true;
        data.splice(i, 1);
      }
    }
    projects.push(project);
    data.splice(0, 1);
  }
  return projects;
};

module.exports = {
  getListBlackduckProject,
  getListBlackduckUser,
  getListUserBlackduckProject,
  getAllBlackduckProject,
  collectBlackduckProject,
};
