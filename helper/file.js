const fs = require('fs');

const isFileExist = async (path) => {
  if (fs.existsSync(path)) return true;
  return false;
};

const removeFolder = async (path) => {
  if (isFileExist(path)) {
    await fs.rmdir(path, { recursive: true }, (err) => {
      if (err) {
        throw err.message;
      }
    });
  }
};

const createFileContent = async (path, fileName, data) => {
  await fs.writeFile(`${path}/${fileName}`, data, (err) => {
    if (err) {
      throw err.message;
    }
  });
};

module.exports = {
  isFileExist,
  removeFolder,
  createFileContent,
};
