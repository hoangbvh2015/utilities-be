const checkBranchExistOnRemote = async (branches, branch) => {
  if (branches.indexOf(`remotes/origin/${branch}`) > 0) {
    return true;
  }
  return false;
};

const getCurrentBranch = async (git) => {
  let currentBranch = '';
  await git.branch(['-a']).then(async (result) => {
    currentBranch = result.current;
  }, async (err) => {
    if (err) {
      throw err;
    }
  });
  return currentBranch;
};

module.exports = {
  checkBranchExistOnRemote,
  getCurrentBranch,
};
