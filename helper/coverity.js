/* eslint-disable no-param-reassign */
const fs = require('fs');
const path = require('path');
const xml2js = require('xml2js');
const axios = require('../services/api');
const { coverityAccount } = require('../enviroment.js');

const authenXML = (result, username, password) => {
  result['soapenv:Envelope']['soapenv:Header'][0]['wsse:Security'][0]['wsse:UsernameToken'][0]['wsse:Username'][0] = username;
  result['soapenv:Envelope']['soapenv:Header'][0]['wsse:Security'][0]['wsse:UsernameToken'][0]['wsse:Password'][0]._ = password;
};

const deleteProjectXML = (result, projectName) => {
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:deleteProject'][0].projectId[0].name = projectName;
};

const createStreamXML = (result, projectName, streamCov) => {
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:createStreamInProject'][0].projectId[0].name = projectName;
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:createStreamInProject'][0].streamSpec = streamCov;
};

const createUserXML = (result, userCov) => {
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:createUser'][0].userSpec = [userCov];
};

const updateUserXML = (result, username, userSpec) => {
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:updateUser'][0].username = [username];
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:updateUser'][0].userSpec = [userSpec];
};

const createProjectXML = (result, name, description) => {
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:createProject'][0].projectSpec[0].name = [name];
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:createProject'][0].projectSpec[0].description = [description];
};

const createRoleXML = (result, role) => {
  // eslint-disable-next-line no-prototype-builtins
  if (result['soapenv:Envelope']['soapenv:Body'][0]['v9:createProject'][0].projectSpec[0].hasOwnProperty('roleAssignments')) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:createProject'][0].projectSpec[0].roleAssignments.push(role);
  } else {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:createProject'][0].projectSpec[0].roleAssignments = [role];
  }
};

const getUserXML = (result, username) => {
  result['soapenv:Envelope']['soapenv:Body'][0]['v9:getUser'][0].username = username;
};

const getStreamsXML = (result, namePattern, languageList, descriptionPattern) => {
  if (languageList) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:getStreams'][0].filterSpec[0].languageList = languageList;
  }

  if (descriptionPattern) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:getStreams'][0].filterSpec[0].descriptionPattern = descriptionPattern;
  }

  if (namePattern) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:getStreams'][0].filterSpec[0].namePattern = namePattern;
  }
};

const getProjectsXML = (
  result,
  namePattern,
  descriptionPattern,
  includeChildren,
  includeStreams,
) => {
  if (namePattern) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:getProjects'] = [{ filterSpec: [{ namePattern }] }];
  }

  if (descriptionPattern) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:getProjects'] = [{ filterSpec: [{ descriptionPattern }] }];
  }

  if (includeChildren !== undefined) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:getProjects'] = [{ filterSpec: [{ includeChildren }] }];
  }

  if (includeStreams !== undefined) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:getProjects'] = [{ filterSpec: [{ includeStreams }] }];
  }
};

const deleteStreamXML = (result, name) => {
  if (name) {
    result['soapenv:Envelope']['soapenv:Body'][0]['v9:deleteStream'][0].streamId = { name };
  }
};

const getListCovProject = async (host) => {
  const filePath = path.join(__dirname, '../assets/coverity/getProjects.xml');

  const data = fs.readFileSync(filePath);
  const parser = new xml2js.Parser();

  const xml = await new Promise((resolve, reject) => parser.parseString(data, (err, result) => {
    if (err) reject(err);
    else resolve(result);
  }));
  authenXML(xml, coverityAccount[host].username, coverityAccount[host].password);
  const xmlObject = new xml2js.Builder().buildObject(xml);

  try {
    // eslint-disable-next-line no-shadow
    const { data } = await axios.coverity.post(`${host}:443/ws/v9/configurationservice`, xmlObject, { headers: { 'Content-Type': 'text/xml;charset=UTF-8' } });
    const json = await new Promise((resolve, reject) => parser.parseString(data, (err, result) => {
      if (err) reject(err);
      else resolve(result);
    }));

    const resonpseResult = json['S:Envelope']['S:Body'][0]['ns2:getProjectsResponse'][0].return.map((project) => ({
      id: project.projectKey[0],
      name: project.id[0].name[0],
      streams: project.streams ? project.streams.map((stream) => stream.id[0].name[0]) : [],
      users: project.roleAssignments ? project.roleAssignments.map((role) => role.username) : [],
    }));
    return resonpseResult;
  } catch (error) {
    if (error && error.response) {
      return {
        status: error.response.status,
        errors: error.response.data,
      };
    }
    throw error.message;
  }
};

const getListCovUser = (users) => {
  const listUsers = [];
  for (let i = 0; i < users.length; i += 1) {
    if (users[i] !== undefined && users[i] !== null && !listUsers.includes(users[i][0])) {
      listUsers.push(users[i][0]);
    }
  }
  return listUsers;
};

const addUserToRoles = (listRoles, username) => {
  listRoles.forEach((element) => {
    element.username.push(username);
  });
};

const getAllCovProject = async (host) => {
  const coverityData = [];
  const coverityProjectList = await getListCovProject(host);

  for (let i = 0; i < coverityProjectList.length; i += 1) {
    const users = getListCovUser(coverityProjectList[i].users);
    const coverityProject = {
      server: host,
      projectData: {
        projectName: coverityProjectList[i].name,
        projectLink: `${host}/reports.htm#/p${coverityProjectList[i].id}`,
        projectStreams: coverityProjectList[i].streams,
        projectCode: coverityProjectList[i].name.split('.')[2] === undefined ? coverityProjectList[i].name.split('.')[0] : coverityProjectList[i].name.split('.')[2],
        department: {
          department1: coverityProjectList[i].name.split('.')[0],
          department2: coverityProjectList[i].name.split('.')[1] === undefined ? coverityProjectList[i].name.split('.')[0] : coverityProjectList[i].name.split('.')[1],
        },
        users,
      },
    };
    coverityData.push(coverityProject);
  }
  return coverityData;
};

const collectCoverityProject = async (host) => {
  const coverityData = await getAllCovProject(host);
  const projects = [];
  const data = coverityData;
  while (data.length > 0) {
    const project = {
      projectName: data[0].projectData.projectName,
      projectCode: data[0].projectData.projectCode,
      department: data[0].projectData.department,
      projectLink: '',
      projectRank: '',
      status: '',
      startDate: '',
      endDate: '',
      description: '',
      useDevOpsService: false,
      pmAccount: '',
      resourceCreatedBy: '',
      isResourceCreated: false,
      resourceData: {
        jenkins: [],
        sonar: [],
        coverity: [],
        blackduck: [],
      },
    };
    for (let i = 0; i < coverityData.length; i += 1) {
      if (project.projectCode === coverityData[i].projectData.projectCode) {
        project.resourceData.coverity.push(coverityData[i]);
        project.isResourceCreated = true;
        data.splice(i, 1);
      }
    }
    projects.push(project);
    data.splice(0, 1);
  }
  return projects;
};

const handleUpdateUser = async (body) => {
  const { username, userSpec, host } = body;
  const filePath = path.join(__dirname, '../assets/coverity/updateUser.xml');
  fs.readFile(filePath, { encoding: 'utf-8' }, async (err, data) => {
    let xml;
    xml2js.parseString(data, async (_err, result) => {
      authenXML(
        result,
        coverityAccount[host].username,
        coverityAccount[host].password,
      );
      updateUserXML(result, username, userSpec);
      xml = new xml2js.Builder().buildObject(result);
    });
    try {
      const response = await axios.coverity.post(`${host}:443/ws/v9/configurationservice`, xml, { headers: { 'Content-Type': 'text/xml;charset=UTF-8' } });
      return {
        status: response.status,
        data: response.data,
      };
    } catch (error) {
      if (error && error.response) {
        return {
          status: error.response.status,
          data: error.response.data,
        };
      }
      throw error.message;
    }
  });
};

module.exports = {
  getListCovUser,
  getListCovProject,
  authenXML,
  createStreamXML,
  createUserXML,
  createProjectXML,
  createRoleXML,
  deleteProjectXML,
  addUserToRoles,
  getAllCovProject,
  collectCoverityProject,
  handleUpdateUser,
  getUserXML,
  getStreamsXML,
  getProjectsXML,
  deleteStreamXML,
};
