const fs = require('fs');
const Action = require('../models/action.model');
const Log = require('../models/log.model');
const Project = require('../models/project.model');
const User = require('../models/user.model');

const createLogfile = async ({
  resourceType, resources, createdBy, actionType,
}) => {
  const dataLog = {
    createdBy,
    action: `${resourceType}_${actionType}_resources`,
  };

  if (resourceType === 'jenkins') {
    const host = resources.jenkinsFolderLink.split('/job/')[0];
    dataLog.host = host;
    dataLog.projectName = [resources.jenkinsFolderLink.split('/job/').pop()];
  }

  if (resourceType === 'sonars') {
    const project = resources.sonarProjectData.find((item) => item.link);

    dataLog.host = project && project.link.split('/dashboard')[0];
    dataLog.projectName = [project && project.link.split('?id=').pop()];
  }

  if (resourceType === 'coverity') {
    dataLog.host = resources.resourceUrl;
    dataLog.projectName = resources.coverityProjectData && resources.coverityProjectData.map(
      (item) => item.projectName,
    );
  }

  if (resourceType === 'blackduck') {
    dataLog.host = resources.resourceUrl;
    dataLog.projectName = resources.blackduckProjectData && resources.blackduckProjectData.map(
      (item) => item.projectName,
    );
  }

  const stream = fs.createWriteStream('public/logs/action.log', { flags: 'a' });
  stream.once('open', () => {
    const createBy = `${dataLog.createdBy ? ` Resouces ${actionType} by: ${dataLog.createdBy}` : ''}`;
    const action = `${dataLog.action ? `, with action: ${dataLog.action}` : ''}`;
    const projectName = `${dataLog.projectName ? `, with projectName: [${dataLog.projectName}]` : ''}`;
    const host = `${dataLog.host ? `, on the host: ${dataLog.host}` : ''}`;
    const lineLog = `[${(new Date()).toString()}] ${createBy} ${action} ${projectName} ${host}`;
    stream.write(`${lineLog}\r\n`);
  });

  const doc = new Log(dataLog);
  await doc.save();
};

const formatResourceData = (resourceType, resourceData, resources) => {
  const data = resourceData;
  if (resourceType === 'jenkins') {
    if (!data.jenkins.find((elem) => elem.jobURL === resources.jenkinsFolderLink)) {
      data.jenkins.push({
        displayName: resources.jenkinsFolderLink.split('/job/').pop(),
        jobURL: resources.jenkinsFolderLink,
      });
    }
  }

  if (resourceType === 'sonars') {
    const listProjectLink = data.sonar.map((item) => item.jobURL);
    const resourceNotInProject = resources.sonarProjectData.filter(
      (item) => item.link && !listProjectLink.includes(item.link),
    );
    const newProject = resourceNotInProject.map((item) => ({
      displayName: item.link.split('?id=').pop(),
      jobURL: item.link,
    }));
    data.sonar = [...data.sonar, ...newProject];
  }

  if (resourceType === 'coverity') {
    if (resources.coverityProjectData) {
      const listProject = data.coverity.map(
        (item) => ({
          displayName: item.displayName,
          jobURL: item.jobURL,
        }),
      );
      const resourceNotInProject = resources.coverityProjectData.filter((item) => {
        const projectName = item.projectName ? item.projectName : item;
        return (listProject.findIndex(
          (elem) => (
            elem.jobURL.includes(resources.resourceUrl) && elem.displayName === projectName),
        ) === -1);
      });
      const newProjects = resourceNotInProject.map(
        (item) => ({ displayName: item.projectName, jobURL: resources.resourceUrl }),
      );
      data.coverity = [...data.coverity, ...newProjects];
    }
  }

  if (resourceType === 'blackduck') {
    const listProject = data.blackduck.map(
      (item) => ({ displayName: item.displayName, jobURL: item.jobURL }),
    );

    const resourceNotInProject = resources.blackduckProjectData
      && resources.blackduckProjectData.filter((item) => {
        const projectName = item.projectName ? item.projectName : item;
        return (listProject.findIndex(
          (elem) => (elem.jobURL.includes(resources.resourceUrl)
            && elem.displayName === projectName),
        ) === -1);
      });
    const newProjects = resourceNotInProject.map(
      (item) => ({ displayName: item.projectName, jobURL: resources.resourceUrl }),
    );
    data.blackduck = [...data.blackduck, ...newProjects];
  }

  return data;
};

const saveDataToProducts = async (
  type,
  {
    fsu,
    bu,
    projectCode,
    projectName,
    resources,
    pmAccount,
    createdBy,
  },
) => {
  const timestamp = new Date();
  try {
    const resourceType = type;
    const record = await Project.findOne({ projectCode });
    const user = await User.findOne({ name: pmAccount && pmAccount.toLowerCase() });
    if (record) {
      const { resourceData } = record;
      const newResourceData = formatResourceData(resourceType, resourceData, resources);
      let projectPermission = [];
      if (Array.isArray(record.projectPermission)) {
        projectPermission = record.projectPermission;
      }
      if (!projectPermission.find((item) => (item.userName.toLowerCase() === pmAccount))) {
        projectPermission.push({
          userName: record.pmAccount.toLowerCase(),
          roleCode: user && ['admin', 'sub_admin', 'project_admin'].includes(user.roleCode) ? user.roleCode : 'project_admin',
          roleName: user && ['admin', 'sub_admin', 'project_admin'].includes(user.roleCode) ? user.roleName : 'Project Admin',
          permission: {
            jenkins: { view: true, edit: true },
            blackduck: { view: true, edit: true },
            coverity: { view: true, edit: true },
            sonar: { view: true, edit: true },
          },
          edit: true,
        });
      }
      if (pmAccount && pmAccount.length > 0) {
        if (user && ['user, project_user'].includes(user.roleCode)) {
          await User.updateOne({ name: user.name }, { roleCode: 'project_admin', roleName: 'Project Admin' });
        } else if (!user) {
          const userModel = new User({
            name: pmAccount,
            mail: `${pmAccount}@fsoft.com.vn`,
            token: '',
            roleName: 'Project Admin',
            roleCode: 'project_admin',
            // projectCodes: [`${item.projectCode}`]
          });
          await userModel.save();
        }
      }

      await Project.updateOne({ projectCode }, {
        $set: {
          resourceData: newResourceData,
          timestamp: timestamp.toISOString(),
          projectPermission,
        },
      });
    } else {
      const projectCt = {
        projectCode,
        projectName,
        pmAccount: pmAccount && pmAccount.toLowerCase(),
        department: {
          department1: fsu,
          department2: bu,
        },
        projectLink: '',
        projectRank: '',
        status: '',
        startDate: '',
        endDate: '',
        description: '',
        useDevOpsService: false,
        resourceCreatedBy: createdBy,
        isResourceCreated: false,
        projectPermission: {
          userName: pmAccount && pmAccount.toLowerCase(),
          roleCode: user && ['admin', 'sub_admin', 'project_admin'].includes(user.roleCode) ? user.roleCode : 'project_admin',
          roleName: user && ['admin', 'sub_admin', 'project_admin'].includes(user.roleCode) ? user.roleName : 'Project Admin',
          permission: {
            jenkins: { view: true, edit: true },
            blackduck: { view: true, edit: true },
            coverity: { view: true, edit: true },
            sonar: { view: true, edit: true },
          },
          edit: true,
        },
      };

      if (pmAccount && pmAccount.length > 0) {
        if (user && ['user, project_user'].includes(user.roleCode)) {
          await User.updateOne({ name: user.name }, { roleCode: 'project_admin', roleName: 'Project Admin' });
        } else if (!user) {
          const userModel = new User({
            name: pmAccount.toLowerCase(),
            mail: `${pmAccount.toLowerCase()}@fsoft.com.vn`,
            token: '',
            roleName: 'Project Admin',
            roleCode: 'project_admin',
            // projectCodes: [`${item.projectCode}`]
          });
          await userModel.save();
        }
      }

      const project = new Project(projectCt);
      const resourceData = {
        blackduck: [], coverity: [], jenkins: [], sonar: [],
      };

      project.resourceData = formatResourceData(resourceType, resourceData, resources);
      project.timestamp = timestamp.toISOString();

      await project.save();
    }
  } catch (error) {
    throw error.message;
  }
};

const saveLog = async (resourceType, {
  fsu, bu, projectCode, projectName, resources, pmAccount, createdBy,
}) => {
  try {
    const record = await Action.findOne({ projectCode });
    if (record) {
      const { note } = record;
      note[resourceType].push(resources);
      createLogfile({
        resourceType,
        resources,
        createdBy,
        actionType: 'updated',
      });
      await Action.updateOne({ projectCode }, { $set: { note } });
    } else {
      const doc = new Action({
        fsu, bu, projectCode, projectName, pmAccount, createdBy,
      });
      doc.note = {
        blackduck: [], coverity: [], jenkins: [], sonars: [],
      };
      doc.note[resourceType].push(resources);
      createLogfile({
        resourceType,
        resources,
        createdBy,
        actionType: 'created',
      });
      await doc.save();
    }

    await saveDataToProducts(resourceType, {
      fsu, bu, projectCode, projectName, resources, pmAccount, createdBy,
    });
  } catch (error) {
    throw error.message;
  }
};

const saveUserActionLog = async (action, { resources, createdBy }) => {
  try {
    const doc = new Log({ createdBy, resources, action });
    await doc.save();
  } catch (error) {
    throw error.message;
  }
};

module.exports = { saveLog, saveUserActionLog };
