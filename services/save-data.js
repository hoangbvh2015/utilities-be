const Project = require('../models/project.model');
const Server = require('../models/server.model');
const UnUseResource = require('../models/unUseResource.model');

const checkResourceCreated = (resourceData) => {
  let isCreated = false;
  if (resourceData.jenkins.length > 0) {
    isCreated = true;
  }

  if (resourceData.sonar.length > 0) {
    isCreated = true;
  }

  if (resourceData.coverity.length > 0) {
    isCreated = true;
  }

  if (resourceData.blackduck.length > 0) {
    isCreated = true;
  }
  return isCreated;
};

const saveData = async (resourceType, {
  department,
  projectCode,
  projectName,
  pmAccount,
  projectLink,
  projectRank,
  status,
  startDate,
  endDate,
  description,
  useDevOpsService,
  resourceCreatedBy,
  isResourceCreated,
  resourceData,
}) => {
  try {
    const record = await Project.findOne({ projectCode });
    // console.log('RAW DATA: ' + record)
    if (record) {
      if (resourceType === 'akawork') {
        await Project.updateOne({ projectCode }, {
          $set: {
            department,
            projectCode,
            projectName,
            projectLink,
            projectRank,
            status,
            startDate,
            endDate,
            description,
            useDevOpsService,
          },
        });
      } else {
        let haveResource = checkResourceCreated(record.resourceData);
        if (!haveResource) {
          haveResource = checkResourceCreated(resourceData);
        }
        for (let i = 0; i < resourceData[resourceType].length; i += 1) {
          const projectExist = record.resourceData[resourceType].find(
            (project) => {
              const pjKey = resourceData[resourceType][i].projectData.projectKey;
              const pjServer = resourceData[resourceType][i].server;
              return project.projectData.projectKey === pjKey && project.server === pjServer;
            },
          );
          if (!projectExist) {
            record.resourceData[resourceType].push(resourceData[resourceType][i]);
          }
        }
        await Project.updateOne({ projectCode }, {
          $set: {
            resourceData: record.resourceData,
            isResourceCreated: haveResource,
          },
        });
      }
      return {
        status: 'OK',
        message: `Data update successful for ${projectCode}`,
      };
    }
    const project = new Project({
      department,
      projectCode,
      projectName,
      pmAccount,
      projectLink,
      projectRank,
      status,
      startDate,
      endDate,
      description,
      useDevOpsService,
      resourceCreatedBy,
      isResourceCreated,
      resourceData,
    });

    await project.save();
    return {
      status: 'OK',
      message: `Data save for ${projectCode}`,
    };
  } catch (error) {
    return {
      status: 'FALSE',
      message: error.message,
      project: {
        department,
        projectCode,
        projectName,
        pmAccount,
        projectLink,
        projectRank,
        status,
        startDate,
        endDate,
        description,
        useDevOpsService,
        resourceCreatedBy,
        isResourceCreated,
        resourceData,
      },
    };
  }
};

const createOrUpdateProject = async (project) => {
  try {
    await Project.updateMany(
      {
        projectCode: project.projectCode,
      },
      project,
      {
        upsert: true,
        setDefaultsOnInsert: true,
      },
    );
  } catch (error) {
    const message = `${error.message} at ${project.projectCode}`;
    throw message;
  }
};

const removeJobUrlJenkins = async (projectCode, jobURL) => {
  try {
    const project = await Project.findOne({ projectCode });
    project.resourceData.jenkins = project.resourceData.jenkins.filter(
      (jenkin) => jenkin.jobURL !== jobURL,
    );
    await Project.updateOne(
      { projectCode },
      {
        $set: { 'resourceData.jenkins': project.resourceData.jenkins },
      },
    );

    return project;
  } catch (error) {
    const message = `${error.message} at ${projectCode}`;
    throw message;
  }
};

const removeProjectCoverity = async (projectCode, coverityProjectName, host, jobURL) => {
  try {
    const project = await Project.findOne({ projectCode });
    const unUseResource = await UnUseResource.findOne({ jobURL });

    project.resourceData.coverity = project.resourceData.coverity.filter((item) => (
      item.jobURL !== jobURL
    ));

    await Project.updateOne(
      {
        projectCode,
      },
      {
        $set: { 'resourceData.coverity': project.resourceData.coverity },
      },
    );

    await UnUseResource.updateOne({ jobURL }, {
      $set: {
        proejctAssigned: [],
      },
    });

    if (unUseResource && unUseResource.proejctAssigned.length > 0) {
      return {
        project,
        message: 'Project have moved to UnuseResource table!',
      };
    }

    return { project };
  } catch (error) {
    const message = `${error.message} when delete ${coverityProjectName} at ${host} for ${projectCode}`;
    throw message;
  }
};

const getServerData = async () => {
  try {
    const servers = await Server.find();
    return servers;
  } catch (error) {
    throw error.message;
  }
};

const createServerData = async (
  serverName,
  serverUrl,
  application,
  authenType,
  token,
  username,
  password,
) => {
  try {
    const server = await Server.find({ serverName, username });
    if (server.length > 0) {
      return {
        statusCode: 200,
        statusMessage: 'Server Name or UserName existed, Please check!',
      };
    }
    const serverData = new Server({
      serverName,
      serverUrl,
      application,
      authenType,
      token,
      username,
      password,
    });
    await serverData.save();

    return {
      statusCode: 200,
      statusMessage: 'Server added',
    };
  } catch (error) {
    throw error.message;
  }
};

const updateServerData = async (
  oldServerName,
  serverName,
  serverUrl,
  application,
  authenType,
  token,
  username,
  password,
) => {
  const oldServer = await Server.findOne({
    serverName: oldServerName,
  });
  if (serverName !== oldServerName) {
    const updateServer = await Server.findOne({
      serverName,
    });
    if (updateServer) {
      return {
        statusCode: 500,
        statusMessage: 'Server existed',
      };
    }
  } else if (oldServer) {
    await Server.updateOne(
      {
        serverName: oldServerName,
      },
      {
        $set: {
          serverName,
          serverUrl,
          application,
          authenType,
          token,
          username,
          password,
        },
      },
    );
    return {
      statusCode: 200,
      statusMessage: 'Server updated',
    };
  }
  return {
    statusCode: 404,
    statusMessage: 'Server not found',
  };
};

const removeServerData = async (
  serverName,
  serverUrl,
) => {
  try {
    const server = await Server.findOne({ serverName, serverUrl });
    if (server) {
      server.remove();
      return {
        statusCode: 200,
        statusMessage: 'Server removed',
      };
    }
    return {
      statusCode: 404,
      statusMessage: 'Server not found',
    };
  } catch (error) {
    throw error.message;
  }
};

module.exports = {
  saveData,
  createOrUpdateProject,
  removeJobUrlJenkins,
  removeProjectCoverity,
  createServerData,
  updateServerData,
  removeServerData,
  getServerData,
};
