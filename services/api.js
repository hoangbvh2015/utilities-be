const axios = require('axios');

const blackduck = axios.create({
  validateStatus: () => true,
});

const coverity = axios.create({
  validateStatus: () => true,
});

module.exports = { blackduck, coverity };
