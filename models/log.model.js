const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  createdBy: {
    type: String,
    required: true,
  },
  action: {
    type: String,
    required: true,
  },
  resources: {
    type: Object,
  },
  host: {
    type: String,
  },
  projectName: {
    type: Array,
  },
}, { timestamps: true });

const model = mongoose.model('logs', modelSchema);

module.exports = model;
