const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  createdBy: {
    type: String,
  },
  pmAccount: {
    type: String,
    required: true,
  },
  projectCode: {
    type: String,
    required: true,
  },
  fsu: {
    type: String,
    required: true,
  },
  bu: {
    type: String,
    required: true,
  },
  projectName: {
    type: String,
    required: true,
  },
  note: {
    type: Object,
  },
}, { timestamps: true });

const model = mongoose.model('actions', modelSchema);

module.exports = model;
