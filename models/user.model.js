const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  mail: {
    type: String,
    required: true,
  },
  token: {
    type: String,
  },
  roleName: {
    type: String,
    required: true,
  },
  roleCode: {
    type: String,
    required: true,
  },
}, { timestamps: true });

const model = mongoose.model('users', modelSchema);

module.exports = model;
