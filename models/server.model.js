const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  serverName: {
    type: String,
    required: true,
  },
  serverUrl: {
    type: String,
  },
  application: {
    type: String,
    required: true,
  },
  authenType: {
    type: String,
    required: true,
  },
  token: {
    type: String,
    required: false,
  },
  username: {
    type: String,
    required: false,
  },
  password: {
    type: String,
    required: false,
  },
}, { timestamps: true });

const model = mongoose.model('servers', modelSchema);

module.exports = model;
