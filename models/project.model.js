const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  createdBy: {
    type: String,
  },
  pmAccount: {
    type: String,
    required: false,
  },
  projectCode: {
    type: String,
    // required: true,
    unique: true,
  },
  department: {
    type: Object,
    required: false,
    department1: {
      type: String,
      required: false,
    },
    department2: {
      type: String,
      required: false,
    },
    department3: {
      type: String,
      required: false,
    },
  },
  projectName: {
    type: String,
    required: true,
  },
  projectLink: {
    type: String,
    required: false,
  },
  projectRank: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    required: false,
  },
  startDate: {
    type: String,
    required: false,
  },
  endDate: {
    type: String,
    required: false,
  },
  description: {
    type: String,
    required: false,
  },
  useDevOpsService: {
    type: Boolean,
    required: false,
  },
  resourceCreatedBy: {
    type: String,
    required: false,
  },
  isResourceCreated: {
    type: Boolean,
    required: false,
  },
  projectPermission: {
    type: Object,
  },
  resourceData: {
    type: Object,
  },
  timestamp: {
    type: String,
  },
}, { timestamps: true });

const model = mongoose.model('projects', modelSchema);

module.exports = model;
