const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  projectCode: {
    type: String,
  },
  projectKey: {
    type: String,
  },
  projectManager: {
    type: String,
  },
  startDate: {
    type: String,
  },
  endDate: {
    type: String,
  },
});

const model = mongoose.model('project_codes', modelSchema);

module.exports = model;
