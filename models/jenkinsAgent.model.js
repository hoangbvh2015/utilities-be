const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  host: {
    type: String,
    required: true,
  },
  agentLink: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: false,
  },
  description: {
    type: String,
    required: false,
  },
  label: {
    type: String,
    required: false,
  },
  executors: {
    type: Number,
    required: false,
  },
  rootRemoteDir: {
    type: String,
    required: false,
  },
  status: {
    type: Boolean,
    required: false,
  },
  primaryOwner: {
    type: String,
    required: false,
  },
  secondaryOwners: {
    type: Array,
    required: false,
  },
}, { timestamps: true });

const model = mongoose.model('jenkins_agent', modelSchema);

module.exports = model;
