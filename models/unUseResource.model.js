const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  id: {
    type: String,
  },
  displayName: {
    type: String,
  },
  hostName: {
    type: String,
  },
  active: {
    type: Boolean,
  },
  jobURL: {
    type: String,
  },
  toolId: {
    type: String,
  },
  hostId: {
    type: String,
  },
  name: {
    type: String,
  },
  resourceType: {
    type: String,
  },
  proejctAssigned: {
    type: Object,
  },
}, { timestamps: true });

const model = mongoose.model('unuse_resource', modelSchema);

module.exports = model;
