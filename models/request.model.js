const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  ticketCode: {
    type: String,
  },
  ticketSubject: {
    type: String,
  },
  ticketId: {
    type: String,
  },
  requestStatus: {
    type: String,
  },
  projectCode: {
    type: String,
  },
  pmAccount: {
    type: String,
  },
  fsu: {
    type: String,
  },
  bu: {
    type: String,
  },
  description: {
    type: String,
  },
  linkTicket: {
    type: String,
  },
  isResourceCreated: {
    type: String,
  },
  projectLink: {
    type: String,
  },
  projectRank: {
    type: String,
  },
  projectName: {
    type: String,
  },
  requester: {
    type: String,
  },
  approver: {
    type: String,
  },
  createTime: {
    type: String,
  },
  startDate: {
    type: String,
  },
  endDate: {
    type: String,
  },
  resourceData: {
    type: Object,
  },
  customerCode: {
    type: String,
  },
}, { timestamps: true });

const model = mongoose.model('requests', modelSchema);

module.exports = model;
