const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelSchema = new Schema({
  roleName: {
    type: String,
    required: true,
  },
  roleCode: {
    type: String,
    required: true,
  },
  menu: [
    {
      menuLink: {
        type: String,
        required: true,
      },
      permissionCd: {
        type: Number,
        required: true,
      },
    },
  ],
}, { timestamps: true });

const model = mongoose.model('user_roles', modelSchema);

module.exports = model;
