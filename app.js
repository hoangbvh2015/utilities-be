const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const cron = require('node-cron');

const app = express();

const indexRouter = require('./routes/index');
const blackduckRouter = require('./routes/blackduckRouter');
const jenkinsRouter = require('./routes/jenkinsRouter');
const sonarRouter = require('./routes/sonarRouter');
const coverityRouter = require('./routes/coverityRouter');
const actionRouter = require('./routes/actionRouter');
const resourcesRouter = require('./routes/resourcesRouter');
const healthCheckRouter = require('./routes/healthCheckRouter');
const userRouter = require('./routes/userRouter');
const projectUserRouter = require('./routes/projectUserRouter');
const exportRouter = require('./routes/exportRouter');
const gitRouter = require('./routes/gitRouter');
const requestRouter = require('./routes/requestRouter');
const serverRouter = require('./routes/serverRouter');

const collectProjectData = require('./scripts/collectProjectData');
const collectJenkinsAgentData = require('./scripts/collectAgentJenkins');
const collectRequestTicket = require('./scripts/collectRequestTicket');
const collectProjectCode = require('./scripts/collectProjectCode');

app.use(cors());
app.use(express.json({ limit: '50mb' }));
// app.use(express.urlencoded({ limit: '50mb' }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', healthCheckRouter);
app.use('/utilities-api', indexRouter);
app.use('/utilities-api/api/blackduck', blackduckRouter);
app.use('/utilities-api/api/jenkins', jenkinsRouter);
app.use('/utilities-api/api/sonar', sonarRouter);
app.use('/utilities-api/api/coverity', coverityRouter);
app.use('/utilities-api/api/action', actionRouter);
app.use('/utilities-api/api/resources', resourcesRouter);
app.use('/utilities-api/api/ldapUser', userRouter);
app.use('/utilities-api/api/projectUser', projectUserRouter);
app.use('/utilities-api/api/export', exportRouter);
app.use('/utilities-api/api/git', gitRouter);
app.use('/utilities-api/api/request', requestRouter);
app.use('/utilities-api/api/server', serverRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  if (req.app.get('env') === 'development') {
    next(createError(404));
    // res.status(404).send('Page Not Found')
  } else {
    res.status(404).send('Page Not Found');
  }
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

cron.schedule('0 */2 * * *', async () => {
  await collectProjectData();
});

cron.schedule('* * * * *', async () => {
  await collectRequestTicket();
});

cron.schedule('0 1 * * *', async () => {
  await collectJenkinsAgentData();
});

cron.schedule('0 2 * * *', async () => {
  await collectProjectCode();
});

module.exports = app;
