#!/usr/bin/env bash

FSOFT_REPO="https://git3.fsoft.com.vn/GROUP/DevOps/akaWork/resource-management/akawork-rm-be.git"
AKAWORK_REPO="https://git.akawork.io/akawork-Tools/resource-management/akawork-rm-be.git"
BRANCH="develop"

pull_original() {
    git remote set-url origin ${AKAWORK_REPO}
    if [ "$(git config --get remote.origin.url)" == ${AKAWORK_REPO} ];
    then
        echo "[+] Update repository link to ${AKAWORK_REPO} successful!"
    fi
    git checkout ${BRANCH}
    git pull origin ${BRANCH}
    echo "[+] Original branch already up to date!"
}

upload_to_mirror() {
    git remote set-url origin ${FSOFT_REPO}
    if [ "$(git config --get remote.origin.url)" == ${FSOFT_REPO} ];
    then
        echo "[+] Update repository link to ${FSOFT_REPO} successful!"
    fi

    git push origin -f ${BRANCH}
}

back_to_original() {
    git remote set-url origin ${AKAWORK_REPO}
    if [ "$(git config --get remote.origin.url)" == ${AKAWORK_REPO} ];
    then
        echo "[+] Update repository link to ${AKAWORK_REPO} successful!"
    fi
}

function main {
    pull_original
    upload_to_mirror
    back_to_original
}

main
