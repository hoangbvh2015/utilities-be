/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
const mongoose = require('mongoose');
const Project = require('../models/project.model');
const Action = require('../models/action.model');
const User = require('../models/user.model');

const formatData = (record, project) => {
  const projectData = project;
  if (record.note.jenkins.length > 0) {
    const listProjectLink = projectData.resourceData.jenkins.map((item) => item.jobURL);
    const resourceNotInProject = record.note.jenkins.filter(
      (item) => item.jenkinsFolderLink && !listProjectLink.includes(item.jenkinsFolderLink),
    );
    const newProjects = resourceNotInProject.map((item) => ({
      displayName: item.jenkinsFolderLink.split('/job/').pop(),
      jobURL: item.jenkinsFolderLink,
    }));
    projectData.resourceData.jenkins = [...projectData.resourceData.jenkins, ...newProjects];
  }

  if (record.note.sonars.length > 0) {
    const listProjectLink = projectData.resourceData.sonar.map((item) => item.jobURL);
    const listSonarProjectData = record.note.sonars.reduce((accumulator, currentValue) => {
      const listproject = currentValue.sonarProjectData && currentValue.sonarProjectData.filter(
        (item) => item.link,
      ).map((item) => ({
        displayName: item.link.split('?id=').pop(),
        jobURL: item.link,
      }));

      return listproject ? [...accumulator, ...listproject] : accumulator;
    }, []);
    const resourceNotInProject = listSonarProjectData.filter(
      (item) => item.jobURL && !listProjectLink.includes(item.jobURL),
    );
    projectData.resourceData.sonar = [...projectData.resourceData.sonar, ...resourceNotInProject];
  }

  if (record.note.coverity.length > 0) {
    const listProject = projectData.resourceData.coverity.map(
      (item) => ({ displayName: item.displayName, jobURL: item.jobURL }),
    );

    const listCoverityProjectData = record.note.coverity.reduce(
      (accumulator, currentValue) => {
        if (currentValue.coverityProjectData) {
          currentValue.coverityProjectData.forEach((item) => {
            const projectName = item.projectName ? item.projectName : item;
            const checkExist = accumulator.find(
              (elem) => (
                (elem.displayName === projectName)
                && (elem.resourceUrl === currentValue.resourceUrl)
              ),
            );
            if (!checkExist) { // check dupicate
              accumulator.push({
                displayName: item.projectName ? item.projectName : item,
                jobURL: currentValue.resourceUrl,
                resourceUrl: currentValue.resourceUrl,
              });
            }
          });
        }
        return accumulator;
      }, [],
    );

    const resourceNotInProject = listCoverityProjectData.filter((item) => (
      listProject.findIndex(
        (elem) => (
          elem.jobURL.includes(item.resourceUrl)
          && elem.displayName === item.displayName),
      ) === -1
    ));
    const newProjects = resourceNotInProject.map(
      (item) => ({ displayName: item.displayName, jobURL: item.jobURL }),
    );
    projectData.resourceData.coverity = [...projectData.resourceData.coverity, ...newProjects];
  }

  if (record.note.blackduck.length > 0) {
    const listProject = project.resourceData.blackduck.map(
      (item) => ({ displayName: item.displayName, jobURL: item.jobURL }),
    );
    const listBlackduckProjectData = record.note.blackduck.reduce((accumulator, currentValue) => {
      if (currentValue.blackduckProjectData) {
        currentValue.blackduckProjectData.map((item) => {
          const projectName = item.projectName ? item.projectName : item;
          const checkExist = accumulator.find(
            (elem) => (
              (elem.displayName === projectName)
              && (elem.resourceUrl === currentValue.resourceUrl)
            ),
          );
          if (!checkExist) { // check dupicate
            accumulator.push({
              displayName: item.projectName ? item.projectName : item,
              jobURL: currentValue.resourceUrl,
              resourceUrl: currentValue.resourceUrl,
            });
          }
          return accumulator;
        });
      }
      return accumulator;
    }, []);

    const resourceNotInProject = listBlackduckProjectData.filter((item) => (
      listProject.findIndex(
        (elem) => (
          elem.jobURL.includes(item.resourceUrl)
          && elem.displayName === item.displayName),
      ) === -1
    ));
    const newProjects = resourceNotInProject.map(
      (item) => ({ displayName: item.displayName, jobURL: item.jobURL }),
    );
    projectData.resourceData.blackduck = [...projectData.resourceData.blackduck, ...newProjects];
  }

  return project;
};

const updateProject = async (record, project) => {
  const projectUpdate = formatData(record, project);
  const pmAccount = project && project.pmAccount ? project.pmAccount.toLowerCase() : null;
  let projectPermission = [];
  if (Array.isArray(project.projectPermission)) {
    projectPermission = project.projectPermission;
  }

  const user = await User.findOne({ name: pmAccount });
  if (!projectPermission.find((item) => (item.userName.toLowerCase() === pmAccount))) {
    projectPermission.push({
      userName: record.pmAccount,
      roleCode: user && ['admin', 'sub_admin', 'project_admin'].includes(user.roleCode) ? user.roleCode : 'project_admin',
      roleName: user && ['admin', 'sub_admin', 'project_admin'].includes(user.roleCode) ? user.roleName : 'Project Admin',
      permission: {
        jenkins: { view: true, edit: true },
        blackduck: { view: true, edit: true },
        coverity: { view: true, edit: true },
        sonar: { view: true, edit: true },
      },
      edit: true,
    });
  }
  if (pmAccount && pmAccount.length > 0) {
    if (!user) {
      const userModel = new User({
        name: pmAccount,
        mail: `${pmAccount}@fsoft.com.vn`,
        token: '',
        roleName: 'Project Admin',
        roleCode: 'project_admin',
        // projectCodes: [`${item.projectCode}`]
      });
      await userModel.save();
    }
  }
  await Project.updateOne({ projectCode: project.projectCode }, projectUpdate, projectPermission);
};

const addNewProject = async (record) => {
  const resourceData = {
    blackduck: [], coverity: [], jenkins: [], sonar: [],
  };
  let user = await User.findOne({ name: record.pmAccount });

  const projectFormat = {
    projectCode: record.projectCode,
    projectName: record.projectName,
    pmAccount: record.pmAccount,
    department: {
      department1: record.fsu,
      department2: record.bu,
    },
    projectLink: '',
    projectRank: '',
    status: '',
    startDate: '',
    endDate: '',
    description: '',
    useDevOpsService: false,
    resourceCreatedBy: record.createdBy,
    isResourceCreated: false,
    resourceData,
    projectPermission: [{
      userName: record.pmAccount,
      roleCode: user && ['admin', 'sub_admin', 'project_admin'].includes(user.roleCode) ? user.roleCode : 'project_admin',
      roleName: user && ['admin', 'sub_admin', 'project_admin'].includes(user.roleCode) ? user.roleName : 'Project Admin',
      permission: {
        jenkins: { view: true, edit: true },
        blackduck: { view: true, edit: true },
        coverity: { view: true, edit: true },
        sonar: { view: true, edit: true },
      },
      edit: true,
    }],
  };

  const projectNew = formatData(record, projectFormat);

  const project = new Project(projectNew);
  await project.save();

  const pmAccount = project && project.pmAccount ? project.pmAccount.toLowerCase() : null;
  if (pmAccount && pmAccount.length > 0) {
    user = await User.findOne({ name: pmAccount });
    if (!user) {
      const userModel = new User({
        name: pmAccount,
        mail: `${pmAccount}@fsoft.com.vn`,
        token: '',
        roleName: 'Project Admin',
        roleCode: 'project_admin',
      });
      await userModel.save();
    }
  }
};

const runMirgateDB = async () => {
  const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/resources_management_logs';
  await mongoose.connect(mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }, (e) => { throw e; });
  try {
    const allRecord = await Action.find({});
    for (const record of allRecord) {
      const project = await Project.findOne({ projectCode: record.projectCode });
      if (project) {
        await updateProject(record, project);
      } else {
        await addNewProject(record);
      }
    }
  } catch (error) {
    throw error;
  }
  await mongoose.connection.close();
};

runMirgateDB();
