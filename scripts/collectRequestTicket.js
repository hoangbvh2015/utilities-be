/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
const axios = require('axios');
const { ezServiceAccount, servers } = require('../enviroment');
const Request = require('../models/request.model');

// const ezServer = 'https://devops.fsoft.com.vn';
// const ezServer = 'https://test-ez-devops.fsoft.com.vn';
const ezServer = servers.ez;
let tokenLoginEz = null;

const listLocation = {
  fhn: {
    location: [
      'fs_fhl_fville1',
      'fs_fhl_fville2',
      'fs_fhn_fpt_caugiay',
      'fs_fhn_keangnam',
      'fs_fhn_phamvanbach',
      'fs_fhn_flc',
      'fs_fhn_vpi',
      'fs_fqn',
      'fs_tamky',
      'fs_fct',
      'fs_other',
    ],
    host: {
      jenkins: 'https://jenkins.fsoft.com.vn',
      sonar: 'https://sonar.fsoft.com.vn',
      blackduck: 'https://blackduck.fsoft.com.vn',
      coverity: 'https://coverity.fsoft.com.vn',
      coverity1: 'https://coverity1.fsoft.com.vn',
    },
  },
  fdn: {
    location: [
      'fs_fdn_massda',
      'fs_fdn_fcomplex',
    ],
    host: {
      jenkins: 'https://jenkins-dn.fsoft.com.vn',
      sonar: 'https://sonar-dn.fsoft.com.vn',
      blackduck: 'https://blackduck-dn.fsoft.com.vn',
      coverity: 'https://coverity-dn.fsoft.com.vn',
    },
  },
  fhm: {
    location: [
      'fs_fhm_ftown1',
      'fs_fhm_ftown2',
      'fs_fhm_ftown3',
      'fs_fhm_sacom',
    ],
    host: {
      jenkins: 'https://jenkins.fsoft.com.vn',
      sonar: 'https://sonar.fsoft.com.vn',
      blackduck: 'https://blackduck.fsoft.com.vn',
      coverity: 'https://coverity-hcm.fsoft.com.vn',
    },
  },
};

const getHost = (locationCode, resource) => {
  if (listLocation.fdn.location.includes(locationCode)) {
    return listLocation.fdn.host[resource];
  }

  if (listLocation.fhm.location.includes(locationCode)) {
    return listLocation.fhm.host[resource];
  }

  return listLocation.fhn.host[resource];
};

const getAgent = async (token, ticketCode, locationCode) => {
  const inforRq = {
    objectTypeKey: 'otk_dsc_services_01_list_08',
    actionKey: '',
    filter: ` and fk_ticket_code = ${ticketCode}`,
    orderBy: '',
    fields: 'id,fk_ticket_code,fk_otk_dsc_services_01_list_08_agent_ip_address,fk_otk_dsc_services_01_list_08_agent_os,fk_otk_dsc_services_01_list_08_project_users,fk_otk_dsc_services_01_list_08_link_agent',
    pageIndex: 1,
    pageSize: 20,
    locale: 'vn-VN',
    viewType: 'list',
  };

  try {
    const { data } = await axios.post(`${ezServer}/services/screen/listObjectHtml`, {
      ...inforRq,
    }, { headers: { 'access-token': token } });
    let listAgent = [];
    if (Array.isArray(data)) {
      listAgent = data.reduce((acc, current) => {
        const agentData = {};
        if (Array.isArray(current.fields)) {
          let listProjectUser = [];
          const host = getHost(locationCode, 'jenkins');
          const ipAddress = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_08_agent_ip_address');
          const agentOs = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_08_agent_os');
          const linkAgent = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_08_link_agent');
          const projectUserObject = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_08_project_users');

          if (projectUserObject && projectUserObject.htmlValue) {
            const [, ...listUser] = projectUserObject.htmlValue.split('<span style="">');
            listProjectUser = listUser.map((item) => item.split('</span>')[0]);
          }

          agentData.ipAddress = ipAddress && ipAddress.value;
          agentData.agentOs = agentOs && agentOs.value;
          agentData.linkJobs = linkAgent && linkAgent.value;
          agentData.projectUsers = listProjectUser;
          agentData.host = host;
          acc.push(agentData);
        }
        return acc;
      }, []);
    }
    return { listAgent, status: 200 };
  } catch (error) {
    if (error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
        field: 'getAgent',
      };
    }
    return {
      status: 400,
      message: error.message,
      field: 'getAgent',
    };
  }
};

const getCoverity = async (token, ticketCode, locationCode) => {
  const inforRq = {
    objectTypeKey: 'otk_dsc_services_01_list_09',
    actionKey: '',
    filter: ` and fk_ticket_code = ${ticketCode}`,
    orderBy: '',
    fields: 'id,fk_ticket_code,fk_otk_dsc_services_01_list_09_project_name,fk_otk_dsc_services_01_list_09_project_stream,fk_otk_dsc_services_01_list_09_link_jobs',
    pageIndex: 1,
    pageSize: 20,
    locale: 'vn-VN',
    viewType: 'list',
  };

  try {
    const { data } = await axios.post(`${ezServer}/services/screen/listObjectHtml`, {
      ...inforRq,
    }, { headers: { 'access-token': token } });
    let listCoverity = [];
    if (Array.isArray(data)) {
      listCoverity = data.reduce((acc, current) => {
        const coverityData = {};
        if (Array.isArray(current.fields)) {
          const host = getHost(locationCode, 'coverity');
          const projectName = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_09_project_name');
          const projectStream = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_09_project_stream');
          const listStream = projectStream && projectStream.value && projectStream.value.match(/[a-zA-Z0-9]+/g);
          const linkJobs = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_09_link_jobs');

          coverityData.projectName = projectName && projectName.value;
          coverityData.streams = listStream;
          coverityData.linkJobs = linkJobs && linkJobs.value;
          coverityData.host = host;
          acc.push(coverityData);
        }
        return acc;
      }, []);
    }

    return { listCoverity, status: 200 };
  } catch (error) {
    if (error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
        field: 'getCoverity',
      };
    }
    return {
      status: 400,
      message: error.message,
      field: 'getCoverity',
    };
  }
};

const getSonar = async (token, ticketCode, locationCode) => {
  const inforRq = {
    objectTypeKey: 'otk_dsc_services_01_list_11',
    actionKey: '',
    filter: ` and fk_ticket_code = ${ticketCode}`,
    orderBy: '',
    fields: 'id,fk_ticket_code,fk_otk_dsc_services_01_list_11_project_name,fk_otk_dsc_services_01_list_11_link_jobs,fk_otk_dsc_services_01_list_11_project_users',
    pageIndex: 1,
    pageSize: 20,
    locale: 'vn-VN',
    viewType: 'list',
  };

  try {
    const { data } = await axios.post(`${ezServer}/services/screen/listObjectHtml`, {
      ...inforRq,
    }, { headers: { 'access-token': token } });
    let listSonar = [];

    if (Array.isArray(data)) {
      listSonar = data.reduce((acc, current) => {
        const sonarData = {};
        if (Array.isArray(current.fields)) {
          let listProjectUser = [];
          const host = getHost(locationCode, 'sonar');
          const projectName = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_11_project_name');
          const linkJobs = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_11_link_jobs');
          const projectUserObject = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_11_project_users');

          if (projectUserObject && projectUserObject.htmlValue) {
            const [, ...listUser] = projectUserObject.htmlValue.split('<span style="">');
            listProjectUser = listUser.map((item) => item.split('</span>')[0]);
          }

          sonarData.projectName = projectName && projectName.value;
          sonarData.projectUsers = listProjectUser;
          sonarData.linkJobs = linkJobs && linkJobs.value;
          sonarData.host = host;
          acc.push(sonarData);
        }
        return acc;
      }, []);
    }
    return { listSonar, status: 200 };
  } catch (error) {
    if (error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
        field: 'getSonar',
      };
    }
    return {
      status: 400,
      message: error.message,
      field: 'getSonar',
    };
  }
};

const getBlackduck = async (token, ticketCode, locationCode) => {
  const inforRq = {
    objectTypeKey: 'otk_dsc_services_01_list_12',
    actionKey: '',
    filter: ` and fk_ticket_code = ${ticketCode}`,
    orderBy: '',
    fields: 'id,fk_ticket_code,fk_otk_dsc_services_01_list_12_project_name,fk_otk_dsc_services_01_list_12_link_jobs',
    pageIndex: 1,
    pageSize: 20,
    locale: 'vn-VN',
    viewType: 'list',
  };

  try {
    const { data } = await axios.post(`${ezServer}/services/screen/listObjectHtml`, {
      ...inforRq,
    }, { headers: { 'access-token': token } });

    let listBlackduck = [];

    if (Array.isArray(data)) {
      listBlackduck = data.reduce((acc, current) => {
        const blackduckData = {};
        if (Array.isArray(current.fields)) {
          const host = getHost(locationCode, 'blackduck');
          const projectName = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_12_project_name');
          const linkJobs = current.fields.find((item) => item.key === 'fk_otk_dsc_services_01_list_12_link_jobs');

          blackduckData.projectName = projectName && projectName.value;
          blackduckData.linkJobs = linkJobs && linkJobs.value;
          blackduckData.host = host;
          acc.push(blackduckData);
        }
        return acc;
      }, []);
    }

    return { listBlackduck, status: 200 };
  } catch (error) {
    if (error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
        field: 'getBlackduck',
      };
    }
    return {
      status: 400,
      message: error.message,
      field: 'getBlackduck',
    };
  }
};

const getResourceType = (str) => {
  const regex = /(blackduck)|(coverity)|(sonar)|(jenkins)/gi;
  const strRegex = str && str.match(regex);
  let resourceType = '';
  if (strRegex) {
    resourceType = strRegex.map((item) => item.toLowerCase());
  }
  return resourceType;
};

const formatDataProject = async (token, ticketCode, data) => {
  const subject = data.fields.find((item) => item.key === 'fk_request_subject');
  const projectCode = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_project_code');
  const pmAccount = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_reporter');
  const fsuBu = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_fsu_bu');
  const fsuBuValue = fsuBu.htmlValue.split('</div></div>')[0].split('>').pop().split(' ');
  const description = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_description');
  const endDate = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_end_date');
  const startDate = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_start_date');
  const projectRank = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_project_rank');
  const locationCode = data.fields.find((item) => item.key === 'fk_location_code');
  const serviceApplice = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_service_apply');
  const requester = data.fields.find((item) => item.key === 'fk_requester');
  const createTicketTime = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_created_time');
  const customerCode = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_customer_code');
  const ticketStatus = data.fields.find((item) => item.key === 'fk_status');
  const ticketId = data.fields.find((item) => item.key === 'id');

  const dataFormat = {
    ticketCode,
    ticketId: ticketId && ticketId.value,
    ticketStatus: ticketStatus && ticketStatus.value,
    ticketSubject: subject && subject.value,
    requestStatus: ticketStatus && ticketStatus.value,
    projectCode: projectCode && projectCode.value,
    pmAccount: pmAccount && pmAccount.value,
    fsu: fsuBuValue && fsuBuValue[0],
    bu: fsuBuValue && fsuBuValue[1],
    description: description && description.value,
    linkTicket: ezServer + data.screens[0].link,
    projectRank: projectRank && projectRank.value,
    requester: requester.value,
    approver: '',
    createTime: createTicketTime.value,
    endDate: endDate.value,
    startDate: startDate.value,
    resourceData: {},
    customerCode: customerCode.value,
  };

  const listError = [];

  dataFormat.projectName = `${dataFormat.fsu}.${dataFormat.bu}.${dataFormat.projectCode}`;
  const resourceType = getResourceType(serviceApplice.htmlValue);

  if (resourceType.includes('jenkins')) {
    const agentData = await getAgent(token, ticketCode, locationCode.value);
    dataFormat.resourceData.jenkins = agentData.listAgent;

    if (agentData.status !== 200) {
      listError.push({
        type: 'jenkins',
        projectCode: dataFormat.projectCode,
        ...agentData,
      });
    }
  }

  if (resourceType.includes('sonar')) {
    const sonarData = await getSonar(token, ticketCode, locationCode.value);
    dataFormat.resourceData.sonar = sonarData.listSonar;
    if (sonarData.status !== 200) {
      listError.push({
        type: 'jenkins',
        projectCode: dataFormat.projectCode,
        ...sonarData,
      });
    }
  }

  if (dataFormat.projectRank !== 'C' || dataFormat.projectRank !== 'D') {
    if (resourceType.includes('coverity')) {
      const coverityData = await getCoverity(token, ticketCode, locationCode.value);
      dataFormat.resourceData.coverity = coverityData.listCoverity;
      if (coverityData.status !== 200) {
        listError.push({
          type: 'jenkins',
          projectCode: dataFormat.projectCode,
          ...coverityData,
        });
      }
    }

    if (resourceType.includes('blackduck')) {
      const blackduckData = await getBlackduck(token, ticketCode, locationCode.value);
      dataFormat.resourceData.blackduck = blackduckData.listBlackduck;
      if (blackduckData.status !== 200) {
        listError.push({
          type: 'jenkins',
          projectCode: dataFormat.projectCode,
          ...blackduckData,
        });
      }
    }
  }
  return { dataFormat, listError };
};

const getProjectInformation = async (token, ticketCode) => {
  const inforRq = {
    webPanelKey: 'wpk_dsc_services_01_form_view_content',
    objectTypeKey: 'otk_dsc_services_01',
    objectKey: null,
    viewType: 'view',
    filter: ` and fk_ticket_code = ${ticketCode}`,
  };

  try {
    const { data } = await axios.post(`${ezServer}/services/screen/objectHtml`, {
      ...inforRq,
    }, { headers: { 'access-token': token } });

    const typeIssue = data.fields.find((item) => item.key === 'fk_otk_dsc_services_01_issue_type');
    if (typeIssue && typeIssue.value === 'dsc_issue_type_request_cicd') {
      const { dataFormat, listError } = await formatDataProject(token, ticketCode, data);
      return { dataFormat, listError, status: 200 };
    }
    return null;
  } catch (error) {
    if (error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
        ticketCode,
        field: 'getProjectInformation',
      };
    }
    return {
      status: 400,
      message: error.message,
      ticketCode,
      field: 'getProjectInformation',
    };
  }
};

const collectRequestTicket = async (token) => {
  const inforRq = {
    objectTypeKey: 'otk_ticket',
    actionKey: '',
    filter: '',
    orderBy: '',
    fields: 'id,fk_company,fk_requester,fk_ticket_code,fk_service_code,fk_request_subject,fk_status,fk_otk_ticket_created_time,fk_url_view,fk_otk_ticket_created_from',
    pageIndex: 1,
    pageSize: 1,
    locale: 'vn-VN',
    viewType: 'list',
  };
  try {
    // get total item
    const getOneitem = await axios.post(`${ezServer}/services/screen/listObjectHtml`, {
      ...inforRq,
    }, { headers: { 'access-token': token['access-token'] } });

    const totalItem = getOneitem.data[0].totalRows;

    // get items
    const inforRqAllData = {
      objectTypeKey: 'otk_ticket',
      actionKey: '',
      filter: '',
      orderBy: '',
      fields: 'id,fk_company,fk_requester,fk_ticket_code,fk_service_code,fk_request_subject,fk_status,fk_otk_ticket_created_time,fk_url_view,fk_otk_ticket_created_from',
      pageIndex: 1,
      pageSize: totalItem,
      locale: 'vn-VN',
      viewType: 'list',
    };

    const { data } = await axios.post(`${ezServer}/services/screen/listObjectHtml`, {
      ...inforRqAllData,
    }, { headers: { 'access-token': token['access-token'] } });

    let listCollectError = [];

    // eslint-disable-next-line no-restricted-syntax
    for (const item of data) {
      const tickketCode = item.fields.find((ele) => ele.key === 'fk_ticket_code');
      if (tickketCode) {
        const projectInfo = await getProjectInformation(token['access-token'], tickketCode.value);
        if (projectInfo) {
          if (projectInfo.status === 200 && projectInfo.dataFormat) {
            const record = await Request.findOne({ ticketCode: projectInfo.dataFormat.ticketCode });
            if (!record) {
              const newRequest = new Request(projectInfo.dataFormat);
              newRequest.save();
            } else {
              await Request.updateOne({ ticketCode: projectInfo.dataFormat.ticketCode }, {
                $set: {
                  ticketId: projectInfo.dataFormat.ticketId,
                  ticketSubject: projectInfo.dataFormat.ticketSubject,
                  ticketStatus: projectInfo.dataFormat.ticketStatus,
                  requestStatus: projectInfo.dataFormat.requestStatus,
                  pmAccount: projectInfo.dataFormat.pmAccount,
                  fsu: projectInfo.dataFormat.fsu,
                  bu: projectInfo.dataFormat.bu,
                  description: projectInfo.dataFormat.description,
                  projectRank: projectInfo.dataFormat.projectRank,
                  endDate: projectInfo.dataFormat.endDate,
                  startDate: projectInfo.dataFormat.startDate,
                  projectName: projectInfo.dataFormat.projectName,
                },
              });
            }
            listCollectError = [...listCollectError, ...projectInfo.listError];
          } else {
            listCollectError.push({ ...projectInfo });
          }
        }
      }
    }
    return { status: 200, message: 'done', listCollectError };
  } catch (error) {
    if (error.response) {
      return {
        status: error.response.status,
        message: error.response.data,
      };
    }
    return {
      status: 400,
      message: error.message,
      fields: 'collectRequestTicket',
    };
  }
};

const main = async () => {
  try {
    const { data: token } = await axios.post(`${servers.ezAuthen}/slo/authen/login`, {
      username: ezServiceAccount[ezServer].username,
      password: ezServiceAccount[ezServer].password,
    });
    tokenLoginEz = token;
    const collectResponse = collectRequestTicket(token);
    return collectResponse;
  } catch (error) {
    if (tokenLoginEz) {
      const collectResponse = collectRequestTicket(tokenLoginEz);
      return collectResponse;
    }
    return {
      status: 400,
      message: error.message,
      fields: 'main',
    };
  }
};

module.exports = main;
