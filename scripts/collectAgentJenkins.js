/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
const mongoose = require('mongoose');
const JenkinsAgent = require('../models/jenkinsAgent.model');
const { jenkinsKey } = require('../enviroment.js');
const {
  getListAgent,
  getAgentUsersPermission,
  getAgentInfo,
} = require('../helper/jenkins.js');

const getAllListAgent = async () => {
  const listAgent = [];
  for (const [key] of Object.entries(jenkinsKey)) {
    listAgent.push(...await getListAgent(key));
  }

  return listAgent;
};

const collectAgent = async () => {
  if (process.env.EXEC === 'TRUE') {
    const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/resources_management_logs';
    await mongoose.connect(mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }, (e) => {
      throw e;
    });
  }

  const listAgent = await getAllListAgent();
  for (const item of listAgent) {
    const agentLink = item.agentLink ? item.agentLink.split('/computer/')[0] : '';
    const userPermission = await getAgentUsersPermission(agentLink, item.agentName);
    const { agentInfo } = await getAgentInfo(agentLink, item.agentName);

    const agentData = {
      host: agentLink,
      agentLink: item.agentLink,
      name: agentInfo && agentInfo.name,
      description: agentInfo && agentInfo.description,
      label: agentInfo && agentInfo.label,
      executors: agentInfo && agentInfo.executors,
      rootRemoteDir: agentInfo && agentInfo.rootRemoteDir,
      status: item && item.status,
      ...userPermission,
    };
    try {
      const record = await JenkinsAgent.findOne({ agentLink: item.agentLink });

      if (record) {
        await JenkinsAgent.updateOne({ agentLink: item.agentLink }, { $set: agentData });
      } else {
        const jenKinsModal = new JenkinsAgent(agentData);
        await jenKinsModal.save();
      }
    } catch (error) {
      throw error;
    }
  }

  if (process.env.EXEC === 'TRUE') {
    await mongoose.connection.close();
  }
};

if (process.env.EXEC === 'TRUE') {
  collectAgent();
}

module.exports = collectAgent;
