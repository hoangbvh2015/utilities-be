/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
const { getAkaAuthenToken, getAllProjectsJira } = require('../helper/akawork');
const ProjectCode = require('../models/projectCode.model');
const { servers } = require('../enviroment');

const akaworkApiHost = servers.akaworkApi;

const getAllProjectCode = async () => {
  const token = await getAkaAuthenToken(akaworkApiHost);

  const rawData = await getAllProjectsJira(akaworkApiHost, token);
  if (rawData) {
    for (const item of rawData) {
      const findExist = await ProjectCode.findOne({ projectCode: item.projectCode });
      if (!findExist) {
        const newProjectCode = new ProjectCode({
          endDate: item.endDate,
          projectCode: item.projectCode,
          projectKey: item.projectKey,
          projectManager: item.projectManager,
          startDate: item.startDate,
          status: item.status,
        });
        await newProjectCode.save();
      }
    }
  }
};

module.exports = getAllProjectCode;
