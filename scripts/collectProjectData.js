/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
const _ = require('lodash');
const fs = require('fs');
const mongoose = require('mongoose');
const {
  getExternalComponents,
  getAkaAuthenToken,
  getExternalProjects,
} = require('../helper/akawork');
const { createOrUpdateProject } = require('../services/save-data');
const Project = require('../models/project.model');
const User = require('../models/user.model');
const UnUseResource = require('../models/unUseResource.model');
const { servers } = require('../enviroment');

const { getAllFolderJenKinsInfo } = require('../helper/jenkins');

const akaworkEndpoint = servers.akaworkApi;

const getAllProjectRawData = async (token) => {
  const startTime = (new Date()).getTime();
  let islastPage = false;
  let pageIndex = 0;
  let data = [];

  while (!islastPage) {
    const rawData = await getExternalProjects(akaworkEndpoint, token, `size=500&page=${pageIndex}`);
    data = [...data, ...rawData.content];
    islastPage = rawData.last;
    pageIndex = rawData.number + 1;
  }
  const endTime = (new Date()).getTime();

  return { data, time: endTime - startTime };
};

const getAllComponent = async (token, tool) => {
  const startTime = (new Date()).getTime();
  let islastPage = false;
  let pageIndex = 0;
  let data = [];

  while (!islastPage) {
    const rawData = await getExternalComponents(akaworkEndpoint, token, `size=500&page=${pageIndex}&tool=${tool}`);
    data = [...data, ...rawData.content];

    islastPage = rawData.last;
    pageIndex = rawData.page + 1;
  }
  const endTime = (new Date()).getTime();

  return { data, time: endTime - startTime };
};

const formatProjectData = (projects, rawData) => {
  const unusedSonarResource = _.clone(rawData.rawSonarData);
  const unusedJenkinsResource = _.clone(rawData.rawJenkinsData);
  const unusedCoverityResource = _.clone(rawData.rawCoverityData);
  const unusedBlackduckResource = _.clone(rawData.rawBlackduckData);

  const formatedProjectData = projects.map((project) => {
    const [fsu, bu] = project.name.split('.');
    const sonarIds = project.components.filter(({ tool }) => tool === 'sonar').map(({ id }) => id);
    const jenkinsIds = project.components.filter(({ tool }) => tool === 'jenkins').map(({ id }) => id);
    const coverityIds = project.components.filter(({ tool }) => tool === 'coverity').map(({ id }) => id);
    const blackduckIds = project.components.filter(({ tool }) => tool === 'blackduck').map(({ id }) => id);

    const sonarResourceOfProject = _.remove(
      unusedSonarResource,
      ({ id }) => sonarIds.includes(id),
    );
    const jenkinsResourceOfProject = _.remove(
      unusedJenkinsResource,
      ({ id }) => jenkinsIds.includes(id),
    );
    const coverityResourceOfProject = _.remove(
      unusedCoverityResource,
      ({ id }) => coverityIds.includes(id),
    );
    const blackduckResourceOfProject = _.remove(
      unusedBlackduckResource,
      ({ id }) => blackduckIds.includes(id),
    );

    return {
      department: { department1: fsu, department2: bu },
      projectCode: project.projectCode,
      projectName: project.name,
      pmAccount: project.projectManager,
      projectLink: '',
      projectRank: project.projectRank,
      status: project.projectStatus,
      startDate: project.startDate,
      endDate: project.endDate,
      description: project.projectStatus,
      useDevOpsService: true,
      isResourceCreated: false,
      resourceData: {
        jenkins: jenkinsResourceOfProject,
        sonar: sonarResourceOfProject,
        coverity: coverityResourceOfProject,
        blackduck: blackduckResourceOfProject,
      },
    };
  });

  const rootUnusedJenkinsResource = unusedJenkinsResource.reduce((acc, current) => {
    const jobSplit = current.jobURL.split('/job/');
    if (jobSplit.length === 2 || current.jobURL.includes('/DEPLOY/')) {
      acc.push(current);
    }
    return acc;
  }, []);

  return {
    projects: formatedProjectData,
    sonars: unusedSonarResource,
    jenkins: rootUnusedJenkinsResource,
    coverity: unusedCoverityResource,
    blackduck: unusedBlackduckResource,
  };
};

const getProjectResourceData = async (project) => {
  const record = await Project.findOne({ projectCode: project.projectCode });
  if (record && record.resourceData) {
    const currentResource = record.resourceData;
    for (const [tool] of Object.entries(project.resourceData)) {
      const currentToolData = currentResource[tool] ? currentResource[tool] : [];
      project.resourceData[tool].forEach((resource) => {
        const index = currentToolData.findIndex((elem) => (
          elem.jobURL === resource.jobURL
        ));
        if (index !== -1) {
          currentToolData[index] = resource;
        } else {
          currentToolData.push(resource);
        }
      });
      currentResource[tool] = currentToolData;
    }
    return currentResource;
  }
  return project.resourceData;
};

const saveProjectData = async (projects) => {
  if (process.env.EXEC === 'TRUE') {
    const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/resources_management_logs';
    await mongoose.connect(mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }, (e) => {
      throw e;
    });
  }

  for (const project of projects) {
    project.resourceData = await getProjectResourceData(project);
    await createOrUpdateProject(project);
  }

  if (process.env.EXEC === 'TRUE') {
    await mongoose.connection.close();
  }
};

const saveLogs = ({
  projects, sonars, jenkins, coverity, blackduck,
}, total, time) => {
  fs.writeFile('public/logs/projects.json', JSON.stringify(projects), (err) => {
    if (err) throw err;
  });
  fs.writeFile('public/logs/unuse-sonar-resource.json', JSON.stringify(sonars), (err) => {
    if (err) throw err;
  });
  fs.writeFile('public/logs/unuse-jenkins-resource.json', JSON.stringify(jenkins), (err) => {
    if (err) throw err;
  });
  fs.writeFile('public/logs/unuse-coverity-resource.json', JSON.stringify(coverity), (err) => {
    if (err) throw err;
  });
  fs.writeFile('public/logs/unuse-blackduck-resource.json', JSON.stringify(blackduck), (err) => {
    if (err) throw err;
  });

  fs.writeFile(
    'public/logs/summary.json',
    JSON.stringify({
      time,
      updatedAt: (new Date()).toString(),
      projectCount: projects.length,
      sonar: { unusedCount: sonars.length, total: total.sonar, unusedList: 'logs/unuse-sonar-resource.json' },
      jenkins: { unusedCount: jenkins.length, total: total.jenkins, unusedList: 'logs/unuse-jenkins-resource.json' },
      coverity: { unusedCount: coverity.length, total: total.coverity, unusedList: 'logs/unuse-coverity-resource.json' },
      blackduck: { unusedCount: blackduck.length, total: total.blackduck, unusedList: 'logs/unuse-blackduck-resource.json' },
    }),
    (err) => {
      if (err) throw err;
    },
  );
};

const saveUnUseResourceToTable = async (resourceDate, type) => {
  for (const item of resourceDate) {
    const record = await UnUseResource.findOne({ jobURL: item.jobURL });

    if (record && record.proejctAssigned.length === 0) {
      UnUseResource.updateOne({ jobURL: item.jobURL }, {
        $set: {
          displayName: item.displayName,
          hostName: item.hostName,
          active: item.active,
          toolId: item.toolId,
          hostId: item.hostId,
          name: item.name,
        },
      });
    } else if (!record) {
      delete item.id;
      const newItem = UnUseResource({ ...item, resourceType: type, proejctAssigned: [] });
      await newItem.save();
    }
  }
};

const saveUnUseResource = async ({
  sonars, jenkins, coverity, blackduck,
}) => {
  if (sonars && sonars.length > 0) {
    await saveUnUseResourceToTable(sonars, 'sonar');
  }

  if (jenkins && jenkins.length > 0) {
    await saveUnUseResourceToTable(jenkins, 'jenkins');
  }

  if (coverity && coverity.length > 0) {
    await saveUnUseResourceToTable(coverity, 'coverity');
  }

  if (blackduck && blackduck.length > 0) {
    await saveUnUseResourceToTable(blackduck, 'blackduck');
  }
};

const clearDateNotExist = async ({
  sonars, jenkins, coverity, blackduck,
}) => {
  const listUnuseReSource = [...sonars, ...jenkins, ...coverity, ...blackduck];
  const listProject = await UnUseResource.find();

  for (const item of listProject) {
    if (!listUnuseReSource.find((ele) => (ele.jobURL === item.jobURL))) {
      await UnUseResource.deleteOne({ jobURL: item.jobURL });

      // if (item.proejctAssigned.length > 0) {
      //   const project = await Project.findOne({ projectCode: item.proejctAssigned[0] });

      //   if (project) {
      //     const resource = project.resourceData[item.resourceType];
      //     project.resourceData[item.resourceType] = resource.filter((ele) => (
      //       ele.jobURL !== item.jobURL
      //     ));
      //     await Project.updateOne({ projectCode: item.projectCode }, {
      //       $set: {
      //         resourceData: project.resourceData,
      //       },
      //     });
      //   }
      // }
    }
  }
};

const main = async () => {
  const startTime = (new Date()).getTime();
  const token = await getAkaAuthenToken(akaworkEndpoint);
  const { data: rawProjectData, time: getProjectTime } = await getAllProjectRawData(token);
  const { data: rawSonarData, time: getSonarTime } = await getAllComponent(token, 'sonar');
  const { data: rawJenkinsData, time: getJenkinsTime } = await getAllComponent(token, 'jenkins');
  const { data: rawCoverityData, time: getCoverityTime } = await getAllComponent(token, 'coverity');
  const { data: rawBlackduckData, time: getBlackduckTime } = await getAllComponent(token, 'blackduck');
  const total = {
    sonar: rawSonarData.length,
    jenkins: rawJenkinsData.length,
    coverity: rawCoverityData.length,
    blackduck: rawBlackduckData.length,
  };

  // PROCESS DATA
  const formatResult = formatProjectData(rawProjectData, {
    rawSonarData, rawJenkinsData, rawCoverityData, rawBlackduckData,
  });

  // get jenkins folder
  const listProjects = [];
  for (const item of formatResult.projects) {
    const pmAccount = item.pmAccount ? item.pmAccount.toLowerCase() : null;
    const user = await User.findOne({ name: pmAccount });
    if (!user && pmAccount && pmAccount.length > 0) {
      const userModel = new User({
        name: pmAccount,
        mail: `${pmAccount}@fsoft.com.vn`,
        token: '',
        roleName: 'Project Admin',
        roleCode: 'project_admin',
        // projectCodes: [`${item.projectCode}`]
      });

      await userModel.save();
    }

    // add pm accountD
    const projectPermission = Array.isArray(item.projectPermission) ? item.projectPermission : [];
    const userProject = projectPermission.find((ele) => (
      ele.userName === pmAccount
    ));

    if (!userProject) {
      projectPermission.push({
        userName: pmAccount,
        roleCode: user ? user.roleCode : 'project_admin',
        roleName: user ? user.roleName : 'Project Admin',
        permission: {
          jenkins: { view: true, edit: true },
          blackduck: { view: true, edit: true },
          coverity: { view: true, edit: true },
          sonar: { view: true, edit: true },
        },
        edit: true,
      });
      item.projectPermission = projectPermission;
      await Project.updateOne({ projectCode: item.projectCode }, { $set: { projectPermission } });
    }

    // update jenkins
    if (item.resourceData.jenkins.length > 0) {
      const newJenkins = [];
      for (const elem of item.resourceData.jenkins) {
        const {
          listJob,
          primaryOwner,
          secondaryOwner,
        } = await getAllFolderJenKinsInfo(elem.jobURL);
        newJenkins.push({
          ...elem,
          listJob,
          primaryOwner,
          secondaryOwner,
        });
      }
      item.resourceData.jenkins = newJenkins;
    }

    listProjects.push(item);
  }

  await saveProjectData(listProjects);
  await clearDateNotExist(formatResult);
  await saveUnUseResource(formatResult);

  // SAVE FILE TO FILE
  const endTime = (new Date()).getTime();
  saveLogs(
    formatResult,
    total,
    {
      totalCrawlTime: endTime - startTime,
      getProjectTime,
      getSonarTime,
      getJenkinsTime,
      getCoverityTime,
      getBlackduckTime,
    },
  );
};

if (process.env.EXEC === 'TRUE') {
  main();
}

module.exports = main;
