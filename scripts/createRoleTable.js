/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
const mongoose = require('mongoose');
const UserRoles = require('../models/userRoles.model');
const User = require('../models/user.model');
const Project = require('../models/project.model');

const menus = ['/pipeline-generator', '/project-management', '/report', '/role-management', '/migration'];
const roles = [
  {
    roleCode: 'admin',
    roleName: 'Administrator',
  },
  {
    roleCode: 'sub_admin',
    roleName: 'Sub-Admin',
  },
  {
    roleCode: 'user',
    roleName: 'User',
  },
  {
    roleCode: 'project_user',
    roleName: 'Project User',
  },
  {
    roleCode: 'project_admin',
    roleName: 'Project Admin',
  },
];

const createListMenu = (role) => {
  const listMenu = menus.map((item) => {
    let permissionCd = 0;
    if (role.roleCode === 'user') {
      if (item === '/pipeline-generator') {
        permissionCd = 2;
      }
    }

    if (role.roleCode === 'sub_admin') {
      if (item !== '/role-management') {
        permissionCd = 2;
      }
    }

    if (role.roleCode === 'project_user') {
      if (item !== '/role-management') {
        permissionCd = 2;
      }
    }

    if (role.roleCode === 'project_admin') {
      if (item !== '/role-management') {
        permissionCd = 2;
      }
    }

    if (role.roleCode === 'admin') {
      permissionCd = 2;
    }
    return {
      menuLink: item,
      permissionCd,
    };
  });

  return listMenu;
};

const createListRole = () => {
  const listRole = roles.map((item) => ({
    ...item,
    menu: createListMenu(item),
  }));

  return listRole;
};

const convertUser = async () => {
  const users = await User.find();

  for (const item of users) {
    if (item.roleName === 'admin') {
      await User.updateOne(
        {
          mail: item.mail,
        },
        {
          $set: {
            name: item.name.toLowerCase(),
            roleCode: 'admin',
            roleName: 'Administrator',
          },
        },
      );
    }

    if (item.roleName === 'config') {
      await User.updateOne(
        { mail: item.mail },
        {
          $set: {
            name: item.name.toLowerCase(),
            roleCode: 'sub_admin',
            roleName: 'Sub-Admin',
          },
        },
      );
    }

    if (item.roleName === 'user') {
      await User.updateOne(
        { mail: item.mail },
        {
          $set: {
            name: item.name.toLowerCase(),
            roleCode: 'user',
            roleName: 'User',
          },
        },
      );
    }
  }
};

const addPmAccount = async () => {
  const listProject = await Project.find();
  if (listProject) {
    for (const item of listProject) {
      // add project to user
      const pmAccount = item.pmAccount ? item.pmAccount.toLowerCase() : null;

      const user = await User.findOne({ name: pmAccount });
      if (!user && pmAccount && pmAccount.length > 0) {
        const userModel = new User({
          name: pmAccount,
          mail: `${pmAccount}@fsoft.com.vn`,
          token: '',
          roleName: 'Project Admin',
          roleCode: 'project_admin',
          // projectCodes: [`${item.projectCode}`]
        });

        await userModel.save();
      }

      // add pm accountD
      const projectPermission = Array.isArray(item.projectPermission) ? item.projectPermission : [];
      const userProject = projectPermission.find((ele) => (
        ele.userName === pmAccount
      ));

      if (!userProject) {
        projectPermission.push({
          userName: pmAccount,
          roleCode: user ? user.roleCode : 'project_admin',
          roleName: user ? user.roleName : 'Project Admin',
          permission: {
            jenkins: { view: true, edit: true },
            blackduck: { view: true, edit: true },
            coverity: { view: true, edit: true },
            sonar: { view: true, edit: true },
          },
          edit: true,
        });
        item.projectPermission = projectPermission;
        await Project.updateOne({ projectCode: item.projectCode }, { $set: { projectPermission } });
      }
    }
  }
};

const convertUserRole = async () => {
  const rolesList = await UserRoles.find();

  for (const item of rolesList) {
    if (item.roleName === 'admin' || item.roleName === 'Admin') {
      await UserRoles.updateOne(
        {
          roleName: item.roleName,
        },
        {
          $set: {
            roleCode: 'admin',
            roleName: 'Administrator',
          },
        },
      );
    }

    if (item.roleName === 'config') {
      await UserRoles.updateOne(
        { roleName: item.roleName },
        {
          $set: {
            roleCode: 'sub_admin',
            roleName: 'Sub Admin',
          },
        },
      );
    }

    if (item.roleName === 'user') {
      await UserRoles.updateOne(
        { roleName: item.roleName },
        {
          $set: {
            roleCode: 'user',
            roleName: 'User',
          },
        },
      );
    }
  }
};

const createRoles = async () => {
  if (process.env.EXEC === 'TRUE') {
    const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/resources_management_logs';
    await mongoose.connect(
      mongoURL,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
      (e) => { throw e; },
    );
  }

  let record = await User.findOne({ mail: 'TungTT22@fsoft.com.vn' });

  if (!record) {
    const userModel = new User({
      name: 'tungtt22',
      mail: 'TungTT22@fsoft.com.vn',
      token: '',
      roleName: 'Administrator',
      roleCode: 'admin',
    });
    await userModel.save();
  }

  await convertUser();
  await convertUserRole();
  await addPmAccount();

  const listRole = createListRole();
  for (const item of listRole) {
    record = await UserRoles.findOne({ roleCode: item.roleCode });
    if (!record) {
      const roleModel = new UserRoles(item);
      await roleModel.save();
    } else {
      await UserRoles.updateOne({ roleCode: item.roleCode }, { $set: { menu: item.menu } });
    }
  }

  if (process.env.EXEC === 'TRUE') {
    await mongoose.connection.close();
  }
};

if (process.env.EXEC === 'TRUE') {
  createRoles();
}

module.exports = createRoles;
