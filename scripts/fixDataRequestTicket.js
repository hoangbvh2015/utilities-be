/* eslint-disable no-console */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
const mongoose = require('mongoose');
const Request = require('../models/request.model');

const editData = async () => {
  const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/resources_management_logs';
  await mongoose.connect(mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }, (e) => {
    throw e;
  });

  console.log('start format data');
  const listRequest = await Request.find();

  if (listRequest && listRequest.length > 0) {
    for (const item of listRequest) {
      if (item.linkTicket.includes('https://test-ez-devops.fsoft.com.vn')) {
        await Request.deleteOne({ ticketCode: item.ticketCode });
      }
    }
  }

  console.log('start format done');
  await mongoose.connection.close();
};

editData();
