module.exports = {
  apps: [
    {
      name: 'akawork-utilities-be',
      script: 'bin/www',
      watch: true,
      env: {
        PORT: 3000,
        NODE_ENV: 'development',
        MONGO_URL: 'mongodb://localhost:27017/resources_management_logs',
        JWT_KEY: 'DEV_SECRET_KEY',
      },
      env_production: {
        PORT: 3000,
        NODE_ENV: 'production',
        MONGO_URL: 'mongodb://localhost:27017/resources_management_logs',
        JWT_KEY: 'PRODUCTION_SECRET_KEY',
      },
      env_staging: {
        PORT: 3000,
        NODE_ENV: 'staging',
        MONGO_URL: 'mongodb://resources_management_user:123456@10.17.69.235:27017/resources_management_logs',
        JWT_KEY: 'STAGGING_SECRET_KEY',
      },
    },
  ],
};
