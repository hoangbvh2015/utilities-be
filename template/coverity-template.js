const rolesCov = [
  {
    roleAssignmentType: ['user'],
    roleId: [{ name: ['committer'] }],
    type: ['project'],
    username: [],
  },
  {
    roleAssignmentType: ['user'],
    roleId: [{ name: ['developer'] }],
    type: ['project'],
    username: [],
  },
  {
    roleAssignmentType: ['user'],
    roleId: [{ name: ['observer'] }],
    type: ['project'],
    username: [],
  },
  {
    roleAssignmentType: ['user'],
    roleId: [{ name: ['projectOwner'] }],
    type: ['project'],
    username: [],
  },
  {
    roleAssignmentType: ['user'],
    roleId: [{ name: ['streamAdmin'] }],
    type: ['project'],
    username: [],
  },
  {
    roleAssignmentType: ['user'],
    roleId: [{ name: ['streamOwner'] }],
    type: ['project'],
    username: [],
  },
];

const userNoAccess = {
  groupId: [
    { displayName: [] },
    { name: [] },
  ],
  roleAssignmentType: ['group'],
  roleId: [{ name: ['noAccess'] }],
  type: ['project'],
};

const otherRoles = [
  {
    roleAssignmentType: ['user'],
    roleId: [{ name: ['Project Member'] }],
    type: ['project'],
    username: [],
  },
];

const stream = {
  autoDeleteOnExpiry: ['false'],
  componentMapId: [{ name: ['Default'] }],
  description: ['created by coverity api'],
  enableDesktopAnalysis: ['true'],
  language: ['MIXED'],
  name: ['api.stream.1'],
  outdated: ['false'],
  ownerAssignmentOption: ['default_component_owner'],
  triageStoreId: [{ name: ['Default Triage Store'] }],
};

const user = {
  disabled: ['false'],
  email: ['example@fsoft.com.vn'],
  familyName: ['temp'],
  givenName: ['temp'],
  groups: ['Users'],
  local: ['true'],
  locale: ['en_US'],
  password: ['1234567'],
  username: ['test-api'],
};

module.exports = {
  rolesCov,
  stream,
  user,
  userNoAccess,
  otherRoles,
};
