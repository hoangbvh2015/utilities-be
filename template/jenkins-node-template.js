const nodeTemplate = {
  windows: {
    nodeDescription: 'window agent',
    numExecutors: '2',
    remoteFS: 'C:/Jenkins',
    labelString: 'label',
    mode: 'EXCLUSIVE',
    '': [
      'hudson.slaves.JNLPLauncher',
      'hudson.slaves.RetentionStrategy$Always',
    ],
    launcher: {
      'stapler-class': 'hudson.slaves.JNLPLauncher',
      $class: 'hudson.slaves.JNLPLauncher',
      workDirSettings: {
        disabled: false,
        workDirPath: '',
        internalDir: 'remoting',
        failIfWorkDirIsMissing: false,
      },
      webSocket: false,
      tunnel: '',
      vmargs: '',
    },
    retentionStrategy: {
      'stapler-class': 'hudson.slaves.RetentionStrategy$Always',
      $class: 'hudson.slaves.RetentionStrategy$Always',
    },
    nodeProperties: {
      'stapler-class-bag': 'true',
    },
    type: 'hudson.slaves.DumbSlave',
  },
  linux: {
    nodeDescription: 'linux agent',
    numExecutors: '2',
    remoteFS: '/home/jenkins',
    labelString: 'label',
    mode: 'EXCLUSIVE',
    '': [
      'hudson.slaves.JNLPLauncher',
      'hudson.slaves.RetentionStrategy$Always',
    ],
    launcher: {
      'stapler-class': 'hudson.slaves.JNLPLauncher',
      $class: 'hudson.slaves.JNLPLauncher',
      workDirSettings: {
        disabled: false,
        workDirPath: '',
        internalDir: 'remoting',
        failIfWorkDirIsMissing: false,
      },
      webSocket: false,
      tunnel: '',
      vmargs: '',
    },
    retentionStrategy: {
      'stapler-class': 'hudson.slaves.RetentionStrategy$Always',
      $class: 'hudson.slaves.RetentionStrategy$Always',
    },
    nodeProperties: {
      'stapler-class-bag': 'true',
    },
    type: 'hudson.slaves.DumbSlave',
  },
  macos: {
    nodeDescription: 'MacOS agent',
    numExecutors: '2',
    remoteFS: '/home/jenkins',
    labelString: 'label',
    mode: 'EXCLUSIVE',
    '': [
      'hudson.slaves.JNLPLauncher',
      'hudson.slaves.RetentionStrategy$Always',
    ],
    launcher: {
      'stapler-class': 'hudson.slaves.JNLPLauncher',
      $class: 'hudson.slaves.JNLPLauncher',
      workDirSettings: {
        disabled: false,
        workDirPath: '',
        internalDir: 'remoting',
        failIfWorkDirIsMissing: false,
      },
      webSocket: false,
      tunnel: '',
      vmargs: '',
    },
    retentionStrategy: {
      'stapler-class': 'hudson.slaves.RetentionStrategy$Always',
      $class: 'hudson.slaves.RetentionStrategy$Always',
    },
    nodeProperties: {
      'stapler-class-bag': 'true',
    },
    type: 'hudson.slaves.DumbSlave',
  },
};

module.exports = nodeTemplate;
