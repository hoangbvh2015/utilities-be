const pipelineTemplate = (pipelineScript) => `<?xml version='1.1' encoding='UTF-8'?>
  <flow-definition plugin="workflow-job@2.40">
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties>
    <hudson.plugins.jira.JiraProjectProperty plugin="jira@3.0.9"/>
    <com.sonyericsson.jenkins.plugins.bfa.model.ScannerJobProperty plugin="build-failure-analyzer@2.0.0">
      <doNotScan>false</doNotScan>
    </com.sonyericsson.jenkins.plugins.bfa.model.ScannerJobProperty>
    <com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty plugin="gitlab-plugin@1.5.13">
      <gitLabConnection>GIT3-FSOFT</gitLabConnection>
    </com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty>
    <com.synopsys.arc.jenkins.plugins.ownership.jobs.JobOwnerJobProperty plugin="ownership@0.12.1">
      <ownership>
        <ownershipEnabled>false</ownershipEnabled>
        <primaryOwnerId></primaryOwnerId>
        <coownersIds class="sorted-set"/>
      </ownership>
    </com.synopsys.arc.jenkins.plugins.ownership.jobs.JobOwnerJobProperty>
    <com.sonyericsson.rebuild.RebuildSettings plugin="rebuild@1.31">
      <autoRebuild>false</autoRebuild>
      <rebuildDisabled>false</rebuildDisabled>
    </com.sonyericsson.rebuild.RebuildSettings>
    <com.synopsys.arc.jenkinsci.plugins.jobrestrictions.jobs.JobRestrictionProperty plugin="job-restrictions@0.8"/>
  </properties>
  <definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps@2.74">
    <script>${pipelineScript}</script>
  <sandbox>true</sandbox>
  </definition><triggers/>
  <disabled>false</disabled>
  </flow-definition>`;

module.exports = {
  pipelineTemplate,
};
